#!/bin/bash

source caEnvironment.sh

export KEYS_PASSWORD="testPassword"

echo "Prepare CA directories"
mkdir certificates

LOCALHOST_DEV_KEY=certificates/localhost.key
LOCALHOST_DEV_KEY_NOPASS=certificates/localhost_nopass.key
LOCALHOST_DEV_REQ=certificates/localhost.csr
LOCALHOST_DEV_CERT=certificates/localhost.crt
LOCALHOST_DEV_CONF=localhost.conf


echo "Generate server certificate for localhost.dev.pmsoft.eu"
openssl genrsa -aes256 -out $LOCALHOST_DEV_KEY -passout env:KEYS_PASSWORD 2048
openssl req -new -config $LOCALHOST_DEV_CONF -key $LOCALHOST_DEV_KEY -out $LOCALHOST_DEV_REQ -passin env:KEYS_PASSWORD -batch

echo "Sign certificate with CA"
openssl ca -cert $CA_CERT -config $CA_CONF -in $LOCALHOST_DEV_REQ -out $LOCALHOST_DEV_CERT -extensions server_ext -passin env:KEYS_PASSWORD -batch

openssl rsa -in $LOCALHOST_DEV_KEY -out $LOCALHOST_DEV_KEY_NOPASS -passin env:KEYS_PASSWORD


CLIENT_DEV_KEY=certificates/client.key
CLIENT_DEV_KEY_NOPASS=certificates/client_nopass.key
CLIENT_DEV_KEY_PKCS8=certificates/client_nopass.PKCS8
CLIENT_DEV_REQ=certificates/client.csr
CLIENT_DEV_CERT=certificates/client.crt
CLIENT_DEV_CONF=client.conf


echo "Generate test client certificate"
openssl genrsa -aes256 -out $CLIENT_DEV_KEY -passout env:KEYS_PASSWORD 2048
openssl req -new -config $CLIENT_DEV_CONF -key $CLIENT_DEV_KEY -out $CLIENT_DEV_REQ -passin env:KEYS_PASSWORD -batch

echo "Sign certificate with CA"
openssl ca -cert $CA_CERT -config $CA_CONF -in $CLIENT_DEV_REQ -out $CLIENT_DEV_CERT -extensions client_ext -passin env:KEYS_PASSWORD -batch

openssl rsa -in $CLIENT_DEV_KEY -out $CLIENT_DEV_KEY_NOPASS -passin env:KEYS_PASSWORD
openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt -in $CLIENT_DEV_KEY_NOPASS -out $CLIENT_DEV_KEY_PKCS8
