#!/bin/bash

source caEnvironment.sh

export KEYS_PASSWORD="testPassword"

echo "Prepare CA directories"
mkdir $CA_HOME
cd $CA_HOME
mkdir certs db private generated
chmod 700 private
touch db/index
openssl rand -hex 16 > db/serial
echo 1001 > db/crlnumber
cd ..

echo "Generate CA root certificate"
openssl req -new -config $CA_CONF -out $CA_REQ -keyout $CA_KEY -passout env:KEYS_PASSWORD
openssl ca -selfsign -config $CA_CONF -in $CA_REQ -out $CA_CERT -extensions ca_ext -passin env:KEYS_PASSWORD  -batch
openssl ca -gencrl -config $CA_CONF -cert $CA_CERT -out $CA_CRL -passin env:KEYS_PASSWORD  -batch

echo "Generate OCSP key"
openssl req -new -newkey rsa:4056 -subj "/C=NT/O=EIIPII/OU=Etcd/CN=OCSP Root Responder" -keyout $CA_OCSP_KEY -out $CA_OCSP_REQ -passout env:KEYS_PASSWORD
openssl ca -config $CA_CONF -cert $CA_CERT -in $CA_OCSP_REQ -out $CA_OCSP_CERT -extensions ocsp_ext -days 30 -passin env:KEYS_PASSWORD
