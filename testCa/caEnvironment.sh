#!/bin/bash


CA_HOME=devCa

CA_CONF=dev_ca.conf
CA_REQ=$CA_HOME/generated/dev-ca.csr
CA_KEY=$CA_HOME/private/dev-root-ca.key
CA_CERT=$CA_HOME/generated/dev-ca.crt
CA_CRL=$CA_HOME/generated/dev-ca.crl
CA_OCSP_REQ=$CA_HOME/generated/dev-ocsp.csr
CA_OCSP_KEY=$CA_HOME/private/dev-ocsp.key
CA_OCSP_CERT=$CA_HOME/generated/dev-ca-ocsp.crt
