/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import sbt._


object Dependencies {

  val json4sVersion = "3.4.2"

  val loggingDependencies = Seq(
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
    "ch.qos.logback" % "logback-classic" % "1.1.3",
    "org.slf4j" % "log4j-over-slf4j" % "1.7.21"
  )

  val testDependencies = Seq(
    "org.typelevel" %% "scalaz-scalatest" % "1.1.2" % Test,
    "org.scalatest" %% "scalatest" % "3.0.0" % Test,
    "org.mockito" % "mockito-all" % "1.10.19" % Test,
    "org.scalacheck" %% "scalacheck" % "1.13.4" % Test
  )

  val  whiskVersion = "0.9.2"

  val dockerTest = Seq(
    "com.whisk" %% "docker-testkit-scalatest" % whiskVersion % Test
      exclude("org.slf4j", "slf4j-api"),
    "com.whisk" %% "docker-testkit-impl-spotify" % whiskVersion % Test
  )

  val json4s = "org.json4s" %% "json4s-native" % json4sVersion
  val json4sJackson = "org.json4s" %% "json4s-jackson" % json4sVersion


//  val nettyVersion = "4.1.3.Final"
//  val nettyVersion = "4.0.42.Final"

  val async= Seq(
    "org.asynchttpclient" % "async-http-client" % "2.0.31"
//    "io.netty" % "netty-codec-http" % nettyVersion,
//    "io.netty" % "netty-handler" % nettyVersion
  )
}