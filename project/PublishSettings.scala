
import com.typesafe.sbt.SbtPgp.autoImport._
import sbt.Keys._
import sbt._

object PublishSettings {

  val sonatypeSettings = Seq(
    useGpg := true,
    usePgpKeyHex("25AB1E10DB5E4DC393CD84133915B2FA12CBEB4C"),
    publishMavenStyle := true,
    publishArtifact in Test := false,
    pomIncludeRepository := {
      _ => false
    },
    publishTo <<= version {
      (v: String) =>
        val nexus = "https://oss.sonatype.org/"
        if (v.trim.endsWith("SNAPSHOT"))
          Some("snapshots" at nexus + "content/repositories/snapshots")
        else
          Some("releases" at nexus + "service/local/staging/deploy/maven2")
    },
    useGpg := true,
    useGpgAgent := true,
    pomExtra := (
      <url>https://bitbucket.org/eiipii/etcdhttpclient</url>
        <licenses>
          <license>
            <name>Apache License, Version 2.0</name>
            <distribution>repo</distribution>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
          </license>
        </licenses>
        <scm>
          <url>https://bitbucket.org/eiipii/etcdhttpclient</url>
          <developerConnection>scm:git:git@bitbucket.org:eiipii/etcdhttpclient.git</developerConnection>
          <connection>scm:git:git@bitbucket.org:eiipii/etcdhttpclient.git</connection>
        </scm>
        <developers>
          <developer>
            <id>paweld2</id>
            <name>Paweł Cesar Sanjuan Szklarz</name>
            <url>http://eiipii.com</url>
          </developer>
          <developer>
            <id>antonio_maciej_matmoros_ochman</id>
            <name>Antonio Maciej Matamoros Ochman</name>
            <url>http://eiipii.com</url>
          </developer>
        </developers>
      )
  )
}