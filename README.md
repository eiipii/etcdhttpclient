# EtcdHttpClient #

EtcdClient is an asynchronous etcd client library (etcd api v2) with key-value methods, support for authentication and methods for retrieving statistics and managing members of etcd clusters. EtcdClient is built on top of Scala Futures and Promises, AsyncHttpClient, Netty and JSON4S.

### Documentation ###

* [User guide](http://etcdscalaclient.eiipii.com/)
* [ScalaDoc](http://etcdscalaclient.eiipii.com/api/#package)
* [Etcd documentation](https://coreos.com/etcd/docs/latest/)

### Bug with jffi library during test execution ##

While executing the tests, you might run into an error, where the **libjffi-1.2.so** dependency cannot be found.

If so, do the following workaround:

```aidl
git clone https://github.com/jnr/jffi.git
cd jffi
ant jar
cp build/jni/libjffi-1.2.so /usr/lib64/
```

Substitute ```/usr/lib64/``` for any location where the library could be placed.
You will see a few locations in the stacktrace of the error.
Also, make sure you do not overwrite an existing file.