/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd

/**
 * An easy-to-configure, asynchronous etcd client with key-value methods, support for authentication and methods for retrieving statistics from etcd clusters.
 *
 * EtcdClient is built on top of [[http://docs.scala-lang.org/overviews/core/futures.html Scala Futures and Promises]], [[https://github.com/AsyncHttpClient/async-http-client/ AsyncHttpClient]], [[http://netty.io/ Netty]] and [[https://github.com/json4s/json4s JSON4S]].
 *
 * It is implemented according to version 2 of the Etcd API.
 */

package object client {

}
