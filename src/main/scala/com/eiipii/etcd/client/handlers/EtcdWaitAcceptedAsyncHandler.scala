/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.handlers

import com.eiipii.etcd.client.model.EtcdJsonFormat._
import com.eiipii.etcd.client.model._
import org.asynchttpclient.AsyncHandler.State
import org.asynchttpclient._
import org.json4s.native.JsonMethods

import scala.concurrent.Promise

class EtcdWaitAcceptedAsyncHandler(
    val promise: Promise[EtcdWaitForKeyResult],
    val waitCallback: EtcdWaitCallback
) extends AbstractEtcdHandlerWithCompleteEtcdHeader[EtcdWaitForKeyResult] {

  override def createResponse(status: Int, header: EtcdHeaders, json: String): Option[EtcdWaitForKeyResult] =
    Some(EtcdWaitForKeyAccepted(header))

  override def onHeadersReceived(headers: HttpResponseHeaders): State = {
    super.onHeadersReceived(headers)
    //We finish the method promise to inform that the wait is accepted
    val waitOk = for {
      headers <- headsOp
      status <- statusCodeOp
      accepted <- createResponse(status, headers, "noBody") if status < 300
    } yield accepted
    waitOk match {
      case Some(ok) =>
        promise.success(ok)
        State.CONTINUE
      case None =>
        //bad header, fail all the operations
        failPromise()
        State.ABORT
    }
  }

  override def onStatusReceived(responseStatus: HttpResponseStatus): State = {
    val code: Int = responseStatus.getStatusCode
    if (code == 200) {
      statusCodeOp = Some(code)
      State.CONTINUE
    } else {
      //fast fail and abort request
      failPromise()
      State.ABORT
    }
  }

  private def buildGetResult(): Option[EtcdGetKeyResponse] = for {
    headers <- headsOp
    body <- JsonMethods.parse(bytes.toString("UTF-8")).extractOpt[EtcdGetKeyBody]
  } yield EtcdGetKeyResponse(headers, body)
  //On complete we need to finish only the callback logic,
  // promise should be already finished with EtcdWaitAccepted(headers)
  override def onCompleted(): Unit = {
    val result: Option[EtcdGetKeyResponse] = statusCodeOp match {
      case Some(ok) if ok < 300 => buildGetResult()
      case None => None //will result on a exception
    }
    result match {
      case Some(res) => waitCallback.onValueChange(res)
      case None => {
        val exception: IncorrectEtcdRequestFormatException = IncorrectEtcdRequestFormatException(
          "EtcdWaitAcceptedAsyncHandler",
          statusCodeOp,
          Some(bytes.toString("UTF-8")),
          headsOp
        )
        promise.tryFailure(exception)
        waitCallback.onError(exception)
      }
    }
  }

  override def failPromise(): Unit = {
    val error = IncorrectEtcdRequestFormatException(
      handlerName,
      statusCodeOp,
      Some(bytes.toString("UTF-8")),
      headsOp
    )
    promise.tryFailure(error)
    waitCallback.onError(error)
  }

}