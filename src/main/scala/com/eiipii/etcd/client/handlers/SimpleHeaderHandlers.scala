package com.eiipii.etcd.client.handlers

import com.eiipii.etcd.client.model.EtcdJsonFormat._
import com.eiipii.etcd.client.model._
import org.json4s.native.JsonMethods

import scala.concurrent.Promise

class EtcdAddMemberAsyncHandler(val promise: Promise[EtcdAddMemberResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdAddMemberResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdAddMemberResult] = for {
    body <- JsonMethods.parse(json).extractOpt[EtcdMember]
  } yield EtcdAddMemberResponse(xEtcdClusterId, body)
}

class EtcdAddUserAsyncHandler(val promise: Promise[EtcdAddUpdateUserResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdAddUpdateUserResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdAddUpdateUserResult] = for {
    body <- JsonMethods.parse(json).extractOpt[EtcdAddUpdateUserResponseBody]
  } yield EtcdAddUpdateUserResponse(xEtcdClusterId, body)

}

class EtcdCreateRoleAsyncHandler(val promise: Promise[EtcdAddUpdateRoleResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdAddUpdateRoleResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdAddUpdateRoleResult] = for {
    body <- JsonMethods.parse(json).extractOpt[EtcdRole]
  } yield EtcdAddUpdateRoleResponse(xEtcdClusterId, body)
}

class EtcdGetRolesAsyncHandler(val promise: Promise[EtcdGetRolesResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdGetRolesResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdGetRolesResult] = for {
    users <- JsonMethods.parse(json).extractOpt[EtcdGetRolesBody]
  } yield EtcdGetRolesResponse(xEtcdClusterId, users)

}

class EtcdGetRoleDetailsAsyncHandler(val promise: Promise[EtcdGetRoleDetailsResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdGetRoleDetailsResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdGetRoleDetailsResult] = for {
    body <- JsonMethods.parse(json).extractOpt[EtcdRole]
  } yield EtcdGetRoleDetailsResponse(xEtcdClusterId, body)
}

class EtcdGetMembersAsyncHandler(val promise: Promise[EtcdGetMembersResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdGetMembersResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdGetMembersResult] = for {
    body <- JsonMethods.parse(json).extractOpt[EtcdGetMembersBody]
  } yield EtcdGetMembersResponse(xEtcdClusterId, body)
}

class EtcdGetUserDetailsAsyncHandler(val promise: Promise[EtcdGetUserDetailsResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdGetUserDetailsResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdGetUserDetailsResult] = for {
    body <- JsonMethods.parse(json).extractOpt[EtcdUser]
  } yield EtcdGetUserDetailsResponse(xEtcdClusterId, body)

}

class EtcdDefaultResponseAsyncHandler(val promise: Promise[EtcdConfirmationResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdConfirmationResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdConfirmationResult] =
    Some(EtcdConfirmationResponse(xEtcdClusterId))

}

class EtcdGetAuthenticationStatusAsyncHandler(val promise: Promise[EtcdGetAuthenticationStatusResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdGetAuthenticationStatusResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdGetAuthenticationStatusResult] = for {
    body <- JsonMethods.parse(json).extractOpt[EtcdEnableAuthentication]
  } yield EtcdGetAuthenticationStatusResponse(xEtcdClusterId, body)

}

class EtcdGetUsersAsyncHandler(val promise: Promise[EtcdGetUsersResult]) extends AbstractEtcdHandlerWithSimpleClusterIdHeader[EtcdGetUsersResult] {

  override def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[EtcdGetUsersResult] = for {
    users <- JsonMethods.parse(json).extractOpt[EtcdGetUsersBody]
  } yield EtcdGetUsersResponse(xEtcdClusterId, users)

}
