/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.handlers

import com.eiipii.etcd.client.model.EtcdJsonFormat._
import com.eiipii.etcd.client.model._
import org.json4s.native.JsonMethods

import scala.concurrent.Promise

class EtcdSelfStatsResponseAsyncHandler(val promise: Promise[EtcdGetSelfStatsResult]) extends AbstractEtcdHandlerWithoutHeader[EtcdGetSelfStatsResult] {

  override def createResponse(status: Int, json: String): Option[EtcdGetSelfStatsResult] =
    JsonMethods.parse(json).extractOpt[EtcdGetSelfStatsResponse]
}

class EtcdStoreStatsResponseAsyncHandler(val promise: Promise[EtcdGetStoreStatsResult]) extends AbstractEtcdHandlerWithoutHeader[EtcdGetStoreStatsResult] {

  override def createResponse(status: Int, json: String): Option[EtcdGetStoreStatsResult] =
    JsonMethods.parse(json).extractOpt[EtcdGetStoreStatsResponse]
}

class EtcdVersionResponseAsyncHandler(val promise: Promise[EtcdGetVersionResult]) extends AbstractEtcdHandlerWithoutHeader[EtcdGetVersionResult] {

  override def createResponse(status: Int, json: String): Option[EtcdGetVersionResult] =
    JsonMethods.parse(json).extractOpt[EtcdGetVersionResponse]
}

class EtcdHealthResponseAsyncHandler(val promise: Promise[EtcdGetHealthResult]) extends AbstractEtcdHandlerWithoutHeader[EtcdGetHealthResult] {

  override def createResponse(status: Int, json: String): Option[EtcdGetHealthResult] =
    JsonMethods.parse(json).extractOpt[EtcdGetHealthResponse]

}

class EtcdLeaderStatsResultAsyncHandler(val promise: Promise[EtcdGetLeaderStatsResult]) extends AbstractEtcdHandlerWithoutHeader[EtcdGetLeaderStatsResult] {

  override def createResponse(status: Int, json: String): Option[EtcdGetLeaderStatsResult] =
    JsonMethods.parse(json).extractOpt[EtcdGetLeaderStatsResponse]
}
