/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.handlers

import java.io.ByteArrayOutputStream

import com.eiipii.etcd.client.model.EtcdJsonFormat._
import com.eiipii.etcd.client.model._
import org.asynchttpclient.AsyncHandler.State
import org.asynchttpclient._
import org.json4s.native.JsonMethods
import org.slf4j.{ Logger, LoggerFactory }

import scala.concurrent.Promise

abstract class AbstractEtcdHandler[T] extends AsyncHandler[Unit] {

  protected[this] var statusCodeOp: Option[Int] = None
  protected[this] val bytes: ByteArrayOutputStream = new ByteArrayOutputStream()

  def promise: Promise[T]

  private lazy val logger: Logger = LoggerFactory.getLogger(this.getClass)

  def handlerName: String = this.getClass.getSimpleName

  override def onCompleted(): Unit = {
    val result: Option[T] = statusCodeOp match {
      case Some(ok) if ok < 300 => buildResponse()
      case Some(error) => buildError()
      case None => None //will result on a exception
    }
    result match {
      case Some(res) => promise.success(res)
      case None => failPromise()
    }
  }

  override def onThrowable(t: Throwable): Unit = {
    promise.failure(t)
    logger.debug(t.getMessage(), t)
  }

  override def onBodyPartReceived(bodyPart: HttpResponseBodyPart): State = {
    bytes.write(bodyPart.getBodyPartBytes())
    State.CONTINUE
  }

  override def onStatusReceived(responseStatus: HttpResponseStatus): State = {
    val code: Int = responseStatus.getStatusCode
    statusCodeOp = Some(code)
    State.CONTINUE
  }

  protected def buildResponse(): Option[T]

  def failPromise(): Unit

  def buildError(): Option[T]

}

abstract class AbstractEtcdHandlerWithoutHeader[T >: EtcdStandardError] extends AbstractEtcdHandler[T] {

  def createResponse(status: Int, json: String): Option[T]

  override def failPromise(): Unit = promise.failure(
    IncorrectEtcdRequestFormatException(
      handlerName,
      statusCodeOp,
      Some(bytes.toString("UTF-8")),
      None
    )
  )

  override def onHeadersReceived(headers: HttpResponseHeaders): State = {
    State.CONTINUE
  }

  def buildResponse(): Option[T] = for {
    status <- statusCodeOp
    t <- createResponse(status, bytes.toString("UTF-8"))
  } yield t

  override def buildError(): Option[T] = buildStandardError()

  protected def buildStandardError(): Option[EtcdStandardError] = for {
    status <- statusCodeOp if status >= 400
    body <- JsonMethods.parse(bytes.toString("UTF-8")).extractOpt[EtcdErrorMessage]
  } yield EtcdStandardError(EtcdResponseCode(status), None, body)

}

abstract class AbstractEtcdHandlerWithSimpleClusterIdHeader[T >: EtcdStandardError] extends AbstractEtcdHandler[T] {

  def createResponse(status: Int, xEtcdClusterId: EtcdClusterId, json: String): Option[T]

  override def failPromise(): Unit = promise.failure(
    IncorrectSimpleEtcdRequestFormatException(
      handlerName,
      statusCodeOp,
      Some(bytes.toString("UTF-8")),
      xEtcdClusterIdOp
    )
  )

  private[this] var xEtcdClusterIdOp: Option[EtcdClusterId] = None

  override def onHeadersReceived(headers: HttpResponseHeaders): State = {
    xEtcdClusterIdOp = Option(headers.getHeaders.get("X-Etcd-Cluster-Id")).map(EtcdClusterId)
    State.CONTINUE
  }

  def buildResponse(): Option[T] = for {
    status <- statusCodeOp
    xEtcdClusterId <- xEtcdClusterIdOp
    t <- createResponse(status, xEtcdClusterId, bytes.toString("UTF-8"))
  } yield t

  override final def buildError(): Option[T] = buildStandardError()

  protected def buildStandardError(): Option[EtcdStandardError] = for {
    status <- statusCodeOp if status >= 400
    body <- JsonMethods.parse(bytes.toString("UTF-8")).extractOpt[EtcdErrorMessage]
  } yield EtcdStandardError(EtcdResponseCode(status), xEtcdClusterIdOp, body)

}

abstract class AbstractEtcdHandlerWithCompleteEtcdHeader[T >: EtcdRequestError] extends AbstractEtcdHandler[T] {

  def createResponse(status: Int, header: EtcdHeaders, json: String): Option[T]

  override def failPromise(): Unit = promise.failure(
    IncorrectEtcdRequestFormatException(
      handlerName,
      statusCodeOp,
      Some(bytes.toString("UTF-8")),
      headsOp
    )
  )

  protected var headsOp: Option[EtcdHeaders] = None

  override def onHeadersReceived(headers: HttpResponseHeaders): State = {
    headsOp = for {
      clusterId <- Option(headers.getHeaders.get("X-Etcd-Cluster-Id")).map(EtcdClusterId)
      etcdIndex <- Option(headers.getHeaders.get("X-Etcd-Index")).map(_.toInt).map(EtcdIndex)
      raftIndex <- Option(headers.getHeaders.get("X-Raft-Index")).map(_.toInt).map(RaftIndex)
      raftTerm <- Option(headers.getHeaders.get("X-Raft-Term")).map(_.toInt).map(RaftTerm)
    } yield EtcdHeaders(clusterId, etcdIndex, raftIndex, raftTerm)
    State.CONTINUE
  }

  def buildResponse(): Option[T] = for {
    status <- statusCodeOp
    header <- headsOp
    t <- createResponse(status, header, bytes.toString("UTF-8"))
  } yield t

  override final def buildError(): Option[T] = buildErrorResult()

  def buildErrorResult(): Option[EtcdRequestError] = for {
    status <- statusCodeOp if status >= 400
    body <- JsonMethods.parse(bytes.toString("UTF-8")).extractOpt[EtcdErrorMessage]
  } yield EtcdRequestError(EtcdResponseCode(status), headsOp, body)

}

