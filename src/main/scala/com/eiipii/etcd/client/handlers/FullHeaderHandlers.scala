/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.handlers

import com.eiipii.etcd.client.model.EtcdJsonFormat._
import com.eiipii.etcd.client.model._
import org.json4s.native.JsonMethods

import scala.concurrent.Promise

class FullHeaderHandlers {

}

class EtcdPutResponseAsyncHandler(val promise: Promise[EtcdSetKeyResult]) extends AbstractEtcdHandlerWithCompleteEtcdHeader[EtcdSetKeyResult] {

  override def createResponse(status: Int, header: EtcdHeaders, json: String): Option[EtcdSetKeyResult] = for {
    body <- JsonMethods.parse(bytes.toString("UTF-8")).extractOpt[EtcdSetKeyBody]
  } yield EtcdSetKeyResponse(header, body)

}

class EtcdDeleteResponseAsyncHandler(val promise: Promise[EtcdDeleteKeyResult]) extends AbstractEtcdHandlerWithCompleteEtcdHeader[EtcdDeleteKeyResult] {

  override def createResponse(status: Int, header: EtcdHeaders, json: String): Option[EtcdDeleteKeyResult] = for {
    body <- JsonMethods.parse(bytes.toString("UTF-8")).extractOpt[EtcdDeleteKeyBody]
  } yield EtcdDeleteKeyResponse(header, body)

}

class EtcdCreateDirResponseAsyncHandler(val promise: Promise[EtcdCreateDirResult]) extends AbstractEtcdHandlerWithCompleteEtcdHeader[EtcdCreateDirResult] {

  override def createResponse(status: Int, header: EtcdHeaders, json: String): Option[EtcdCreateDirResult] = for {
    body <- JsonMethods.parse(json).extractOpt[EtcdCreateDirBody]
  } yield EtcdCreateDirResponse(header, body)

}

class EtcdListDirAsyncHandler(val promise: Promise[EtcdListDirResult]) extends AbstractEtcdHandlerWithCompleteEtcdHeader[EtcdListDirResult] {

  override def createResponse(status: Int, header: EtcdHeaders, json: String): Option[EtcdListDirResult] = for {
    body <- JsonMethods.parse(bytes.toString("UTF-8")).extractOpt[EtcdListDirBody]
  } yield EtcdListDirResponse(header, body)
}

class EtcdGetResponseAsyncHandler(val promise: Promise[EtcdGetKeyResult]) extends AbstractEtcdHandlerWithCompleteEtcdHeader[EtcdGetKeyResult] {

  override def createResponse(status: Int, header: EtcdHeaders, json: String): Option[EtcdGetKeyResult] = for {
    body <- JsonMethods.parse(bytes.toString("UTF-8")).extractOpt[EtcdGetKeyBody]
  } yield EtcdGetKeyResponse(header, body)

}

