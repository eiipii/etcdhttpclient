/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client

/**
 * ==Handlers==
 *
 * [[https://github.com/AsyncHttpClient/async-http-client/ AsyncHttpClient]] handlers used to handle requests and return etcd responses asynchronously.
 *
 * For a given HTTP request, etcd most often returns a response containing a JSON array in its body (although, sometimes it may return an empty body).
 * A response may also contain HTTP headers with the HTTP status code for the response and some information of the etcd cluster (mostly indexes captured as instances of [[com.eiipii.etcd.client.model.EtcdIndex EtcdIndex]]).
 * [[https://github.com/AsyncHttpClient/async-http-client/ AsyncHttpClient]] offers support for handling HTTP requests with customized handlers.
 * Each handler formats a type of response returned by etcd by parsing the JSON array returned in the response and retrieving the pertinent information in the response's headers.
 * The parsing is done using [[https://github.com/json4s/json4s#extracting-values JSON4S extract method]] and case classes defined in package [[com.eiipii.etcd.client.model model]].
 * The information retrieved from the response's headers is passed along with the body to appropriate case classes in the same package.
 *
 * Http requests to etcd may return and error response or a success response.
 * Error responses usually contain useful information on what went wrong, while success responses return the status of etcd after the request is performed.
 * Handlers appropriately adapted to deal with both situations.
 *
 */

package object handlers {

}
