/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client

/**
 * ==Model==
 *
 * Contains case classes for dealing with etcd HTTP responses and building correct HTTP requests to etcd.
 * Most of the case classes are self-explanatory, once the Responses and Errors section below is read.
 * Those that are not are discussed in more detail in this documentation.
 * They are mostly used by handlers or by the client to [[https://github.com/json4s/json4s#extracting-values extract]] information from JSON objects or [[https://github.com/json4s/json4s#serialization write]] information to JSON objects sent in requests.
 *
 * Also, package model contains an object for properly building instances of [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]], [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]], hidden nodes and [[com.eiipii.etcd.client.model.EtcdValue EtcdValue]].
 *
 * Finally, it contains [[https://github.com/json4s/json4s#serializing-non-supported-types JSON4S custom serializers]] for JSON parsing support.
 * All the custom serializers contained in this package are contained  in the [[com.eiipii.etcd.client.model.EtcdJsonFormat EtcdJsonFormat]] object, which in turn is used by handlers in package [[com.eiipii.etcd.client.handlers handlers]] to parse responses from etcd to HTTP requests.
 *
 *  ===Responses and Errors===
 *
 * All requests can return either a response or an error.
 * In order to deal with this two possible situations, the outcome of each request is represented by a Result sealed trait in this package.
 * These traits are extended by case classes representing an error or a response in the manner described bellow.
 * The naming convention for Result sealed traits is the following: Etcd<NameOfTheEtcdClientMethod>Result.
 * For example [[com.eiipii.etcd.client.model.EtcdGetKeyResult EtcdGetKeyResult]].
 *
 * There are two types of error that etcd could return.
 * For EtcdClient, they are represented in the [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]] and [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]].
 * One of these case classes extends each Result sealed trait representing the outcome of a request sent by the client.
 *
 * On the other hand, Responses are dealt with using Response case classes.
 * They contain all the necessary fields used to deserialize the information contained in the HTTP response received from etcd.
 * Response case classes also extend Result sealed traits. The naming convention is similar: Etcd<NameOfTheEtcdClientMethod>Response.
 * For example [[com.eiipii.etcd.client.model.EtcdGetKeyResponse EtcdGetKeyResponse]].
 *
 * Both Response and Error case classes contain the relevant information about etcd that can be found in headers and in the response body.
 *
 * A more involved case, which is delt with a callback, is [[com.eiipii.etcd.client.model.EtcdWaitForKeyResult EtcdWaitForKeyResult]].
 *
 *
 */
package object model {

}
