/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.model

import org.json4s.JsonAST._
import org.json4s.{ CustomSerializer, DefaultFormats }

/**
 * Object extending JSON4S DefaultFormats with custom Serializer]]s.
 *
 * All of them are defined in the [[com.eiipii.etcd.client.model model]] package.
 * They are the following:
 *
 *  - [[com.eiipii.etcd.client.model.EtcdValueSerializer EtcdValueSerializer]]]],
 *  - [[com.eiipii.etcd.client.model.EtcdKeySerializer EtcdKeySerializer]],
 *  - [[com.eiipii.etcd.client.model.EtcdClusterIdSerializer EtcdClusterIdSerializer]],
 *  - [[com.eiipii.etcd.client.model.EtcdIndexSerializer EtcdIndexSerializer]],
 *  - [[com.eiipii.etcd.client.model.EtcdTTLSerializer EtcdTTLSerializer]],
 *  - [[com.eiipii.etcd.client.model.RaftIndexSerializer RaftIndexSerializer]],
 *  - [[com.eiipii.etcd.client.model.RaftTermSerializer RaftTermSerializer]],
 *  - [[com.eiipii.etcd.client.model.EtcdMemberStatsSerialized EtcdMemberStatsSerialized]],
 *  - [[com.eiipii.etcd.client.model.EtcdErrorMessageSerializer EtcdErrorMessageSerializer]].
 *
 */
object EtcdJsonFormat {

  implicit lazy val format = DefaultFormats ++ Seq(
    EtcdValueSerializer,
    EtcdKeySerializer,
    EtcdClusterIdSerializer,
    EtcdIndexSerializer,
    EtcdTTLSerializer,
    RaftIndexSerializer,
    RaftTermSerializer,
    EtcdMemberStatsSerialized,
    EtcdErrorMessageSerializer,
    EtcdListDirNodeSerializer
  )

}
import org.json4s.JsonDSL._

case object EtcdListDirNodeSerializer extends CustomSerializer[EtcdListDirNode](
  implicit format => ({
    case rootNode: JValue =>
      (rootNode \ "dir") match {
        case JBool(true) =>
          EtcdListDirectory(
            true,
            (rootNode \ "key").extract[EtcdKey],
            (rootNode \ "nodes").extract[List[EtcdListDirNode]],
            (rootNode \ "modifiedIndex").extract[EtcdIndex],
            (rootNode \ "createdIndex").extract[EtcdIndex]
          )
        case _ => EtcdListValue(
          false,
          (rootNode \ "key").extract[EtcdKey],
          (rootNode \ "value").extract[EtcdValue],
          (rootNode \ "modifiedIndex").extract[EtcdIndex],
          (rootNode \ "createdIndex").extract[EtcdIndex]
        )
      }
  }, {
    case value: EtcdErrorMessage => value match {
      case EtcdError(cause, errorCode, index, message) =>
        ("cause", cause) ~
          ("errorCode", errorCode) ~
          ("index", index) ~
          ("message", message)
      case EtcdSimpleError(message) =>
        ("message", message)
    }
  })
)

case object EtcdErrorMessageSerializer extends CustomSerializer[EtcdErrorMessage](
  implicit format => ({
    case rootNode: JValue =>
      (rootNode \ "errorCode") match {
        case JInt(errorCode) =>
          EtcdError(
            (rootNode \ "cause").extract[String],
            errorCode.intValue(),
            (rootNode \ "index").extract[Int],
            (rootNode \ "message").extract[String]

          )
        case _ => EtcdSimpleError((rootNode \ "message").extract[String])
      }
  }, {
    case value: EtcdErrorMessage => value match {
      case EtcdError(cause, errorCode, index, message) =>
        ("cause", cause) ~
          ("errorCode", errorCode) ~
          ("index", index) ~
          ("message", message)
      case EtcdSimpleError(message) =>
        ("message", message)
    }
  })
)

case object EtcdMemberStatsSerialized extends CustomSerializer[EtcdGetLeaderStatsResponse](
  implicit format => ({
    case rootNode: JValue =>
      val members = (rootNode \ "followers") match {
        case JObject(members) =>
          members.map {
            case (id, json) =>
              val counts = (json \ "counts").extract[EtcdCounts]
              val latency = (json \ "latency").extract[EtcdLatency]
              EtcdMemberStats(
                id,
                counts,
                latency
              )
          }
        case _ => List[EtcdMemberStats]()
      }
      EtcdGetLeaderStatsResponse((rootNode \ "leader").extract[String], members)
  }, {
    case value: EtcdGetLeaderStatsResponse =>
      val followersJ: JObject = (JObject() /: value.followers) {
        case (jsonPart, member) =>
          jsonPart ~ (
            member.memberId,
            ("counts",
              ("fail", member.counts.fail) ~ ("success", member.counts.success)) ~ ("latency",
                ("average", member.latency.average) ~
                ("current", member.latency.current) ~
                ("maximum", member.latency.maximum) ~
                ("minimum", member.latency.minimum) ~
                ("standardDeviation", member.latency.standardDeviation))
          )
      }
      ("leader", value.leader) ~ ("followers", followersJ)
      JObject(List(
        "leader" -> JString(value.leader),
        "followers" -> followersJ
      ))
  })
)

case object EtcdValueSerializer extends CustomSerializer[EtcdValue](
  format => ({
    case JString(s) => EtcdValue(s)
  }, {
    case value: EtcdValue => JString(value.value)
  })
)

case object EtcdKeySerializer extends CustomSerializer[EtcdKey](
  format => ({
    case JString(s) => EtcdKey(s)
  }, {
    case key: EtcdKey => JString(key.key)
  })
)

case object EtcdClusterIdSerializer extends CustomSerializer[EtcdClusterId](
  format => ({
    case JString(s) => EtcdClusterId(s)
  }, {
    case v: EtcdClusterId => JString(v.id)
  })
)

case object EtcdIndexSerializer extends CustomSerializer[EtcdIndex](
  format => ({
    case JInt(s) => EtcdIndex(s.intValue())
  }, {
    case v: EtcdIndex => JInt(v.index)
  })
)

case object RaftIndexSerializer extends CustomSerializer[RaftIndex](
  format => ({
    case JInt(s) => RaftIndex(s.intValue())
  }, {
    case v: RaftIndex => JInt(v.rIndex)
  })
)

case object RaftTermSerializer extends CustomSerializer[RaftTerm](
  format => ({
    case JInt(s) => RaftTerm(s.intValue())
  }, {
    case v: RaftTerm => JInt(v.term)
  })
)

case object EtcdTTLSerializer extends CustomSerializer[EtcdTTL](
  format => ({
    case JInt(s) => EtcdTTL(s.intValue())
  }, {
    case v: EtcdTTL => JInt(v.ttl)
  })
)

