/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.model

import scala.concurrent.{ Future, Promise }

/**
 * Contains methods to define well-formed instances of [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]], [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]], hidden nodes and [[com.eiipii.etcd.client.model.EtcdValue EtcdValue]].
 *
 */
object EtcdModel {

  /**
   * Returns an instance of [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] from a list of node names.
   *
   * The key preserves the order of the list given as input.
   *
   * @param path A list of strings not containing "/", which are to be used as node names.
   */

  def fromPath(path: List[String]): EtcdKey = {
    if (path.exists(node => node.contains("/"))) {
      throw new IllegalArgumentException("node names cannot contain '/'")
    }
    //An empty string ("") is prepended to the list so as to get a "/" at the beginning of the key.
    EtcdKey((("") :: path).mkString("/"))
  }

  /**
   * Returns a list of all node names in order.
   *
   * @param key An [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] which is to be split in node names and placed in order into a list.
   */
  def toPath(key: EtcdKey): List[String] = {
    //The first element is dropped, since it is always and empty string ("").
    key.key.split("/").toList.drop(1)
  }

  /**
   * Creates a well-formed instance of [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]]  from a String that represents a path to an etcd node.
   *
   * @param key the String representation of the key
   */
  def key(key: String): EtcdKey = {
    if (key == null || key.isEmpty) {
      throw new IllegalArgumentException("key name can not be null.")
    }
    if (!key.startsWith("/")) {
      throw new IllegalArgumentException(s"key name must start with '/'. Provided value $key")
    }
    if (key.endsWith("/")) {
      throw new IllegalArgumentException(s"key name must not end with '/'. Provided value $key")
    }
    EtcdKey(key)
  }

  /**
   * Creates a well-formed instance of [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] from a String that represents a path to an etcd node and a String that represents the name of the node.
   *
   * @param dir the [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] which will be appended with a name of a node.
   * @param key the string representation of the name of the node.
   */
  def key(dir: EtcdDirectory, key: String): EtcdKey = EtcdModel.key(dir.dir + key)

  /**
   * Creates a well-formed instance of [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] from a String that represents a path.
   *
   * @param dir the string representation of the directory.
   */
  def directory(dir: String): EtcdDirectory = {
    if (dir == null) {
      throw new IllegalArgumentException("Directory name can not be null.")
    }
    if (!dir.startsWith("/")) {
      throw new IllegalArgumentException(s"Directory name must start with '/'. Provided value $dir")
    }
    if (!dir.endsWith("/")) {
      throw new IllegalArgumentException(s"Directory must end with '/'. Provided value $dir")
    }
    EtcdDirectory(dir)
  }
  /**
   * Creates a well-formed instance of [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] from a string that represents a path to a node and a string that represents the name of the node.
   *
   * For more information on hidden nodes see [[https://coreos.com/etcd/docs/latest/v2/api.html#creating-a-hidden-node Creating a hidden node]].
   *
   * @param dir the string representation of the directory.
   * @param node the string representation of the node's name.
   * @return A [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] composed of a path and the name of a node prepended with '_'.
   *
   */
  def hiddenNode(dir: String, node: String): EtcdKey = {
    if (dir == null || dir.isEmpty) {
      throw new IllegalArgumentException("Directory name can not be null.")
    }
    if (!dir.startsWith("/")) {
      throw new IllegalArgumentException(s"Directory name must start with '/'. Provided value $dir")
    }
    if (!dir.endsWith("/")) {
      throw new IllegalArgumentException(s"Directory must end with '/'. Provided value $dir")
    }
    if (node == null || node.isEmpty) {
      throw new IllegalArgumentException("node name cannot be null.")
    }
    if (node.endsWith("/") || node.startsWith("/")) {
      throw new IllegalArgumentException(s"Node name must not end or start with '/'. Provided value $node")
    }
    EtcdKey(dir + "_" + node)
  }

  /**
   * Creates a well-formed instance of [[com.eiipii.etcd.client.model.EtcdValue EtcdValue]]
   *
   * @param value the string representation of the value
   */
  def value(value: String): EtcdValue = EtcdValue(value)

  /**
   * Returns an instance of [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] from a [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]].
   *
   * An EtcdDirectory can be treated as an EtcdKey by dropping the closing '/'.
   *
   * @param dir the [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] that is to be treated as a key.
   */
  def keyOfDirectory(dir: EtcdDirectory) = EtcdKey(dir.dir.dropRight(1))

  /**
   * Returns an instance of [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] from a [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]].
   *
   * An EtcdDirectory can be treated as an EtcdKey by adding a closing '/'.
   *
   * @param key the [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] that is to be treated as a directory.
   */
  def directoryOfKey(key: EtcdKey) = EtcdDirectory(key.key + "/")
}
// Common elements
/**
 * A key name is a full path name.
 *
 * @param key the string representation of the key.
 */
case class EtcdKey(key: String) extends AnyVal

/**
 * A directory is a full path with a trailing '/'.
 *
 * @param dir the string representation of the directory.
 */
case class EtcdDirectory(dir: String) extends AnyVal

/**
 * A value is any data being stored under a key.
 *
 * @param value the string representation of the value.
 */

case class EtcdValue(value: String) extends AnyVal

/**
 * Etcd information returned in headers of responses to requests performed by key-value methods.
 */
case class EtcdHeaders(
  xEtcdClusterId: EtcdClusterId,
  xEtcdIndex: EtcdIndex,
  xRaftIndex: RaftIndex,
  xRaftTerm: RaftTerm
)

sealed trait EtcdErrorMessage {
  def message: String
}

case class EtcdError(
  cause: String,
  errorCode: Int,
  index: Int,
  message: String
) extends EtcdErrorMessage

case class EtcdSimpleError(message: String) extends EtcdErrorMessage

case class EtcdNodeEmpty(
  key: EtcdKey,
  modifiedIndex: EtcdIndex,
  createdIndex: EtcdIndex,
  ttl: Option[EtcdTTL]
)

case class EtcdNodeValue(
  key: EtcdKey,
  value: EtcdValue,
  modifiedIndex: EtcdIndex,
  createdIndex: EtcdIndex,
  ttl: Option[EtcdTTL],
  expiration: Option[String]
)

// Model for methods arguments

// Model for methods results.

/**
 * If HTTP request information do not match the etcd format,
 * then a IncorrectEtcdRequestFormatException is created
 */
case class IncorrectEtcdRequestFormatException(
    val handler: String,
    val responseCode: Option[Int],
    val responseBody: Option[String],
    val headers: Option[EtcdHeaders]
) extends Exception {

  lazy val clustedIdHeader: Option[EtcdClusterId] = headers.map(_.xEtcdClusterId)
  override def toString: String =
    s"Incorrect Etcd response. Handler[${handler}], statusCode[${responseCode}], body:[${responseBody}] clustedIdHeader:[${clustedIdHeader}] headers:[${headers}]"
}

/**
 * A simple version of IncorrectEtcdRequestFormatException when only ClusterId is provided on headers.
 */
case class IncorrectSimpleEtcdRequestFormatException(
    val handler: String,
    val responseCode: Option[Int],
    val responseBody: Option[String],
    val clustedIdHeader: Option[EtcdClusterId]
) extends Exception {

  override def toString: String =
    s"Incorrect Etcd response. Handler[${handler}], statusCode[${responseCode}], body:[${responseBody}] clustedIdHeader:[${clustedIdHeader}]"
}

/**
 * HTTP response code to a request.
 */
case class EtcdResponseCode(val code: Int) extends AnyVal

case class EtcdClusterId(val id: String) extends AnyVal

/**
 * Index associated to the last operation performed in etcd.
 *
 * See [[https://coreos.com/etcd/docs/latest/v2/api.html#response-headers Response Headers]].
 */
case class EtcdIndex(val index: Int) extends AnyVal

/**
 * Life span in seconds of a key or directory.
 *
 * @param ttl Integer representing the time to live in seconds.
 */
case class EtcdTTL(val ttl: Int) extends AnyVal

/**
 * An integer that will increase whenever an etcd master election happens in the cluster.
 *
 * See [[https://coreos.com/etcd/docs/latest/v2/api.html#response-headers Response Headers]].
 */
case class RaftTerm(val term: Int) extends AnyVal

/**
 * See [[https://coreos.com/etcd/docs/latest/v2/api.html#response-headers Response Headers]].
 */
case class RaftIndex(val rIndex: Int) extends AnyVal

/**
 * Default response for operations which return only X-Etcd Cluster ID.
 */
sealed trait EtcdConfirmationResult {
  def accepted: Boolean
}

/**
 * Empty response to confirm that operation was successful.
 *
 * @param xEtcdClusterId cluster id on headers
 */
case class EtcdConfirmationResponse(
    xEtcdClusterId: EtcdClusterId
) extends EtcdConfirmationResult {
  override def accepted: Boolean = true
}

/**
 * Key-Value methods
 * Put operation result.
 */
sealed trait EtcdSetKeyResult {
  def accepted: Boolean
}

case class EtcdSetKeyResponse(
    headers: EtcdHeaders,
    body: EtcdSetKeyBody
) extends EtcdSetKeyResult {
  override def accepted: Boolean = true
}

case class EtcdSetKeyBody(
  action: String,
  node: EtcdNodeValue,
  prevNode: Option[EtcdNodeValue]
)

sealed trait EtcdGetKeyResult {
  def accepted: Boolean
}

case class EtcdGetKeyResponse(
    headers: EtcdHeaders,
    body: EtcdGetKeyBody
) extends EtcdGetKeyResult {
  override def accepted: Boolean = true
}

case class EtcdGetKeyBody(
  action: String,
  node: EtcdNodeValue
)

/**
 * Result of a wait operation.
 *
 * For implementation details, see [[com.eiipii.etcd.client.model.EtcdWaitForKeyAccepted EtcdWaitForKeyAccepted]], [[com.eiipii.etcd.client.model.FutureBasedEtcdWaitCallback FutureBasedEtcdWaitCallback]] and [[com.eiipii.etcd.client.model.EtcdWaitCallback EtcdWaitCallback]].
 */
sealed trait EtcdWaitForKeyResult {
  def accepted: Boolean
}

/**
 * Response returned once the wait is accepted.
 *
 * In the case there has not been any change in etcd since the watch has ben set, etcd returns a HTTP response with an empty body, but with headers confirming the wait has been set.
 * See [[https://coreos.com/etcd/docs/latest/v2/api.html#waiting-for-a-change Waiting for a change]].
 *
 * @param headers Headers returned as response to a request of an operation on the key space.
 */
case class EtcdWaitForKeyAccepted(headers: EtcdHeaders) extends EtcdWaitForKeyResult {
  override def accepted: Boolean = true
}

/**
 * Callback for a wait operation used by [[com.eiipii.etcd.client.handlers.EtcdWaitAcceptedAsyncHandler EtcdWaitAcceptedAsyncHandler]] to asynchonously handle etcd responses to a wait operation.
 */
trait EtcdWaitCallback {
  /**
   *
   *
   * @param response [[com.eiipii.etcd.client.model.EtcdGetKeyResponse EtcdGetKeyResponse]] from etcd sent when a change on a key or directory takes place.
   */
  def onValueChange(response: EtcdGetKeyResponse): Unit

  /**
   *
   * @param t Throwable from an error of an [[com.eiipii.etcd.client.EtcdClient.waitForKey EtcdClient.waitForKey]] operation.
   */
  def onError(t: Throwable): Unit
}

/**
 * Callback for a wait operation.
 *
 * Extends [[com.eiipii.etcd.client.model.EtcdWaitCallback EtcdWaitCallback]].
 * It should be used as input for [[com.eiipii.etcd.client.EtcdClient.waitForKey EtcdClient.waitForKey]].
 *
 * It contains methods for retrieving an [[com.eiipii.etcd.client.model.EtcdGetKeyResponse EtcdGetKeyResponse]] received once a change to etcd is registered.
 *
 */
class FutureBasedEtcdWaitCallback extends EtcdWaitCallback {
  private[this] val p = Promise[EtcdGetKeyResponse]

  /**
   *
   * @param response [[com.eiipii.etcd.client.model.EtcdGetKeyResponse EtcdGetKeyResponse]] from etcd sent when a change on a key or directory takes place.
   */
  override def onValueChange(response: EtcdGetKeyResponse): Unit = p.success(response)

  /**
   *
   * @param t Throwable from an error of an [[com.eiipii.etcd.client.EtcdClient.waitForKey EtcdClient.waitForKey]] operation.
   */
  override def onError(t: Throwable): Unit = p.failure(t)

  /**
   *
   * @return Future wrapped around the [[com.eiipii.etcd.client.model.EtcdGetKeyResponse EtcdGetKeyResponse]] to be returned once the callback is completed.
   */
  def future: Future[EtcdGetKeyResponse] = p.future

  /**
   *
   * @return true if the callback has been completed, false otherwise.
   */
  def isCompleted: Boolean = p.isCompleted
}

/**
 * Create directory operation result.
 */
sealed trait EtcdCreateDirResult {
  def accepted: Boolean
}

case class EtcdCreateDirResponse(
    headers: EtcdHeaders,
    body: EtcdCreateDirBody
) extends EtcdCreateDirResult {
  override def accepted: Boolean = true
}

case class EtcdCreateDirBody(
  action: String,
  node: EtcdCreateDirNode,
  prevNode: Option[EtcdCreateDirNode]
)

case class EtcdCreateDirNode(
  createdIndex: EtcdIndex,
  dir: Boolean,
  expiration: Option[String],
  key: EtcdKey,
  modifiedIndex: Option[EtcdIndex],
  ttl: Option[EtcdTTL]
)

/**
 * Delete operation result.
 */
sealed trait EtcdDeleteKeyResult {
  def accepted: Boolean
}

case class EtcdDeleteKeyResponse(
    headers: EtcdHeaders,
    body: EtcdDeleteKeyBody
) extends EtcdDeleteKeyResult {
  override def accepted: Boolean = true
}

case class EtcdDeleteKeyBody(
  action: String,
  node: EtcdNodeEmpty,
  prevNode: EtcdNodeValue
)

/**
 * List directory operation result.
 */
sealed trait EtcdListDirResult {
  def accepted: Boolean
}

case class EtcdListDirResponse(
    headers: EtcdHeaders,
    body: EtcdListDirBody
) extends EtcdListDirResult {
  override def accepted: Boolean = true
}

case class EtcdListDirBody(action: String, node: EtcdListDirectory)

sealed trait EtcdListDirNode {
  def dir: Boolean
  def key: EtcdKey
  def modifiedIndex: EtcdIndex
  def createdIndex: EtcdIndex
}

case class EtcdListDirectory(
  dir: Boolean = true,
  key: EtcdKey,
  nodes: List[EtcdListDirNode],
  modifiedIndex: EtcdIndex,
  createdIndex: EtcdIndex
) extends EtcdListDirNode

case class EtcdListValue(
  dir: Boolean = false,
  key: EtcdKey,
  value: EtcdValue,
  modifiedIndex: EtcdIndex,
  createdIndex: EtcdIndex
) extends EtcdListDirNode

/**
 * Version Request Result.
 */

sealed trait EtcdGetVersionResult {
  def accepted: Boolean
}

case class EtcdGetVersionResponse(
    etcdserver: String,
    etcdcluster: String
) extends EtcdGetVersionResult {
  override def accepted: Boolean = true
}

sealed trait EtcdGetHealthResult {
  def accepted: Boolean
}

case class EtcdGetHealthResponse(
    health: String
) extends EtcdGetHealthResult {
  override def accepted: Boolean = true
}

/**
 * Leader Statistics Result.
 */

sealed trait EtcdGetLeaderStatsResult {
  def accepted: Boolean
}

case class EtcdGetLeaderStatsResponse(
    leader: String,
    followers: List[EtcdMemberStats]
) extends EtcdGetLeaderStatsResult {
  override def accepted: Boolean = true
}

case class EtcdMemberStats(
  memberId: String,
  counts: EtcdCounts,
  latency: EtcdLatency
)

case class EtcdCounts(
  fail: Int,
  success: Int
)

case class EtcdLatency(
  average: Double,
  current: Double,
  maximum: Double,
  minimum: Double,
  standardDeviation: Double
)

/**
 * Self Statistics Result.
 */

sealed trait EtcdGetSelfStatsResult {
  def accepted: Boolean
}

case class EtcdGetSelfStatsResponse(
    id: String,
    leaderInfo: EtcdLeaderInfo,
    name: String,
    recvAppendRequestCnt: Int,
    recvBandwidthRate: Option[Double],
    recvPkgRate: Option[Double],
    sendAppendRequestCnt: Int,
    startTime: String,
    state: String
) extends EtcdGetSelfStatsResult {
  override def accepted: Boolean = true
}

case class EtcdLeaderInfo(
  leader: String,
  startTime: String,
  uptime: String
)

/**
 * Self Statistics Result.
 */

sealed trait EtcdGetStoreStatsResult {
  def accepted: Boolean
}

case class EtcdGetStoreStatsResponse(
    compareAndSwapFail: Int,
    compareAndSwapSuccess: Int,
    createFail: Int,
    createSuccess: Int,
    deleteFail: Int,
    deleteSuccess: Int,
    expireCount: Int,
    getsFail: Int,
    getsSuccess: Int,
    setsFail: Int,
    setsSuccess: Int,
    updateFail: Int,
    updateSuccess: Int,
    watchers: Int
) extends EtcdGetStoreStatsResult {
  override def accepted: Boolean = true
}

// Authentication

/**
 * Data used to get authorization when authentication is enabled.
 *
 * @param userLogin String representing a login.
 * @param password String representing a password.
 */
case class EtcdUserAuthenticationData(
  userLogin: String,
  password: String
)

/**
 * Data representing a user in etcd.
 *
 * @param user String representing the name of the user.
 * @param password String representing the password of the user.
 * @param roles List of instances of [[com.eiipii.etcd.client.model.EtcdRole EtcdRole]], which specify the permissions the user has.
 */
case class EtcdUser(
  user: String,
  password: Option[String],
  roles: List[EtcdRole] = List()
)

/**
 * Data specifying what write and read privileges are granted or revoked to the users in possession of it.
 *
 * @param role String representing the name of the role.
 * @param permissions List of permissions that the possession of the role grants.
 * @param grant Used for granting a permission when a role is updated.
 * @param revoke Used for revoking a permission when a role is updated.
 */
case class EtcdRole(
  role: String,
  permissions: Option[EtcdPermission] = None,
  grant: Option[EtcdPermission] = None,
  revoke: Option[EtcdPermission] = None
)

/**
 * Data specifying the read and write access to the key-value and authentication spaces.
 *
 * @param kv Instance of [[com.eiipii.etcd.client.model.EtcdKV EtcdKV]]
 */
case class EtcdPermission(
  kv: EtcdKV
)

/**
 * Data specifying the read and write access to the key-value and authentication spaces.
 *
 * @param read List of strings specifying read access to a certain domain of the key-value and authentication spaces.
 * @param write List of strings specifying write access to a certain domain of the key-value and authentication spaces.
 */
case class EtcdKV(
  read: List[String],
  write: List[String]
)

case class EtcdEnableAuthentication(
  enabled: Option[Boolean]
)

/**
 * Form that needs to be filled to use as input for [[com.eiipii.etcd.client.EtcdClient.addUpdateUser EtcdClient.addUpdateUser]] and [[com.eiipii.etcd.client.EtcdClient.deleteUser EtcdClient.deleteUser]] methods.
 *
 * @param user String identifying a user.
 * @param password Password that the user will use for authentication.
 * @param roles List of roles that a user has when it gets created.
 * @param grant List of roles to be granted when performing an update.
 * @param revoke List of roles to be revoked when performing an update.
 */
case class EtcdUserRequestForm(
  user: String,
  password: Option[String] = None,
  roles: List[String] = List(),
  grant: List[String] = List(),
  revoke: List[String] = List()
)

/**
 * Get Authentication Status
 */

sealed trait EtcdGetAuthenticationStatusResult {
  def accepted: Boolean
}

case class EtcdGetAuthenticationStatusResponse(
    xEtcdClusterId: EtcdClusterId,
    body: EtcdEnableAuthentication
) extends EtcdGetAuthenticationStatusResult {
  override def accepted: Boolean = true
}

/**
 * Add or Update User
 */

sealed trait EtcdAddUpdateUserResult {
  def accepted: Boolean
}

case class EtcdAddUpdateUserResponse(
    xEtcdClusterId: EtcdClusterId,
    body: EtcdAddUpdateUserResponseBody
) extends EtcdAddUpdateUserResult {
  override def accepted: Boolean = true
}

case class EtcdAddUpdateUserResponseBody(
  user: String,
  password: Option[String],
  roles: List[String]
)

/**
 * GetUsers
 */

sealed trait EtcdGetUsersResult {
  def accepted: Boolean
}

case class EtcdGetUsersResponse(
    xEtcdClusterId: EtcdClusterId,
    users: EtcdGetUsersBody
) extends EtcdGetUsersResult {
  override def accepted: Boolean = true
}

case class EtcdGetUsersBody(
  users: List[EtcdUser]
)

/**
 * GetUserDetails
 */

sealed trait EtcdGetUserDetailsResult {
  def accepted: Boolean
}

case class EtcdGetUserDetailsResponse(
    xEtcdClusterId: EtcdClusterId,
    body: EtcdUser
) extends EtcdGetUserDetailsResult {
  override def accepted: Boolean = true
}

/**
 * CreateUpdateRole
 */

sealed trait EtcdAddUpdateRoleResult {
  def accepted: Boolean
}

case class EtcdAddUpdateRoleResponse(
    xEtcdClusterId: EtcdClusterId,
    body: EtcdRole
) extends EtcdAddUpdateRoleResult {
  override def accepted: Boolean = true
}

/**
 * GetRoles
 */

sealed trait EtcdGetRolesResult {
  def accepted: Boolean
}

case class EtcdGetRolesResponse(
    xEtcdClusterId: EtcdClusterId,
    roles: EtcdGetRolesBody
) extends EtcdGetRolesResult {
  override def accepted: Boolean = true
}

case class EtcdGetRolesBody(
  roles: List[EtcdRole]
)

/**
 * GetRoleDetails
 */

sealed trait EtcdGetRoleDetailsResult {
  def accepted: Boolean
}

case class EtcdGetRoleDetailsResponse(
    xEtcdClusterId: EtcdClusterId,
    body: EtcdRole
) extends EtcdGetRoleDetailsResult {
  override def accepted: Boolean = true
}

// Members

/**
 * Case class representing the information that a member of a cluster may have of another member.
 *
 * @param id String identifying a member of an etcd cluster.
 * @param name String identifying a member of an etcd cluster by name.
 * @param peerURLs List of URLs (represented as strings) used for communication between peers in an etcd cluster.
 * @param clientURLs List of URLs (represented as strings) used for communication with clients.
 */
case class EtcdMember(
  id: String,
  name: String,
  peerURLs: List[String],
  clientURLs: List[String]
)

/**
 * GetMembers
 */

sealed trait EtcdGetMembersResult {
  def accepted: Boolean
}

case class EtcdGetMembersResponse(
    xEtcdClusterId: EtcdClusterId,
    body: EtcdGetMembersBody
) extends EtcdGetMembersResult {
  override def accepted: Boolean = true
}

case class EtcdGetMembersBody(
  members: List[EtcdMember]
)

/**
 * AddMember
 */

sealed trait EtcdAddMemberResult {
  def accepted: Boolean
}

case class EtcdAddMemberResponse(
    xEtcdClusterId: EtcdClusterId,
    body: EtcdMember
) extends EtcdAddMemberResult {
  override def accepted: Boolean = true
}

/**
 * Form used to update the peer URLs of a member of a cluster.
 *
 * @param peerURLs List of strings representing the peer URLs.
 */

case class EtcdMemberForm(
  peerURLs: List[String]
)

/**
 * Type of error returned by most operations on etcd that are not done on the key space.
 *
 * @param statusCode HTTP response code returned in the response's headers.
 * @param xEtcdClusterId Etcd cluster id information
 * @param error Message with information on the cause of the error.
 */
case class EtcdStandardError(
  statusCode: EtcdResponseCode,
  xEtcdClusterId: Option[EtcdClusterId],
  error: EtcdErrorMessage
) extends EtcdAddMemberResult
    with EtcdGetMembersResult
    with EtcdGetRoleDetailsResult
    with EtcdGetRolesResult
    with EtcdGetUserDetailsResult
    with EtcdGetUsersResult
    with EtcdGetAuthenticationStatusResult
    with EtcdAddUpdateUserResult
    with EtcdGetVersionResult
    with EtcdGetLeaderStatsResult
    with EtcdConfirmationResult
    with EtcdGetStoreStatsResult
    with EtcdGetSelfStatsResult
    with EtcdGetHealthResult
    with EtcdAddUpdateRoleResult {
  override def accepted: Boolean = false
}

/**
 * Type of error returned by most operations on etcd done on the key space.
 *
 * @param statusCode HTTP response code returned in the response's headers.
 * @param headers Headers of the HTTP response returned by etcd.
 * @param body Body of the HTTP error response containing information on the cause of the error.
 */
case class EtcdRequestError(
  statusCode: EtcdResponseCode,
  headers: Option[EtcdHeaders],
  body: EtcdErrorMessage
) extends EtcdListDirResult
    with EtcdSetKeyResult
    with EtcdGetKeyResult
    with EtcdWaitForKeyResult
    with EtcdCreateDirResult
    with EtcdDeleteKeyResult {
  override def accepted: Boolean = false
}
