/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.model

/**
 * Used for conditionally updating a key with the [[com.eiipii.etcd.client.EtcdClient.setKey EtcdClient.setKey]] method.
 *
 * Its subclasses pose a condition which has to be met in order to perform an update to a key in etcd.
 */
sealed trait CompareAndSwapCondition

/**
 * Used for conditionally deleting a key with the [[com.eiipii.etcd.client.EtcdClient.deleteKey EtcdClient.deleteKey]] method.
 *
 * Its subclasses pose a condition which has to be met in order to perform a delete operation of a key in etcd.
 */
sealed trait ConditionalDeleteCondition

/**
 * Used for conditionally updating a key using the [[com.eiipii.etcd.client.EtcdClient.setKey EtcdClient.setKey]] method.
 *
 * @param state Requires that the key to already exist, when set to true. When set to false, requires that key do not exist.
 */
case class KeyMustExist(state: Boolean) extends CompareAndSwapCondition

/**
 * Used for conditionally updating or deleting a key using the [[com.eiipii.etcd.client.EtcdClient.setKey EtcdClient.setKey]] or [[com.eiipii.etcd.client.EtcdClient.deleteKey EtcdClient.deleteKey]] methods.
 *
 * @param index Requires for the key to have this modified index associated to its last operation.
 */
case class KeyMustHaveIndex(index: EtcdIndex) extends CompareAndSwapCondition with ConditionalDeleteCondition

/**
 * Used for conditionally updating or deleting a key using the [[com.eiipii.etcd.client.EtcdClient.setKey EtcdClient.setKey]] or [[com.eiipii.etcd.client.EtcdClient.deleteKey EtcdClient.deleteKey]] methods.
 *
 * @param value Requires for the key to have this value at the time of the request.
 */
case class KeyMustHaveValue(value: EtcdValue) extends CompareAndSwapCondition with ConditionalDeleteCondition

