/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client

import com.eiipii.etcd.client.handlers._
import com.eiipii.etcd.client.model.EtcdJsonFormat._
import com.eiipii.etcd.client.model._
import io.netty.handler.ssl.SslContext
import org.asynchttpclient._
import org.json4s.native.Serialization.write

import scala.concurrent.{ Future, Promise }

/**
 * EtcdClient companion object.
 *
 */

object EtcdClient {

  def apply(config: EtcdConfiguration) = new EtcdClient(config)

}

/**
 * Asynchronously sends HTTP requests to etcd servers and handles outcome responses.
 *
 * ==EtcdClient==
 *
 * EtcdClient uses [[https://github.com/AsyncHttpClient/async-http-client/ AsyncHttpClient]] in order to build and send HTTP requests.
 * Its methods were developed to suit the etcd [[https://coreos.com/etcd/docs/latest/ documentation]].
 * All methods use [[http://docs.scala-lang.org/overviews/core/futures.html Scala Futures and Promises]] in order to provide an asynchronous implementation.
 *
 * Depending on the request, either an error will be thrown or a response will be returned.
 * In both cases, there is a case class extending a trait in package [[com.eiipii.etcd.client.model model]] with a name ending in Result, which will be returned containing either the response or information about the error.
 *
 * For example, when performing an [[com.eiipii.etcd.client.EtcdClient.setKey EtcdClient.setKey]] operation, an [[com.eiipii.etcd.client.model.EtcdSetKeyResult EtcdSetKeyResult]] is expected.
 * The outcome of the operation, either an error or a response, can be accessed considering both cases.
 * An easy way to retrieve a response could be with a `match` statement:
 *
 * {{{
 *        etcdcli.setKey(key, value) onSuccess  {
 *        case EtcdSetKeyResponse(headers, body) =>
 *         ???
 *        }
 * }}}
 *
 * ===Key-Value Methods===
 *
 * The implementation of key-value methods follows the specification given in the [[https://coreos.com/etcd/docs/latest/v2/api.html etcd Client API]].
 * Their purpose is to set, retrieve, modify or delete keys, values and directories in etcd.
 * It is also possible to wait (set a watch) on etcd keys, so as to monitor changes in the server in real time and conditionally set operations depending on the response obtained once a change in the data is registered.
 *
 * Key-Value methods take as inputs case classes defined in package [[model]].
 * The recommended way of getting input data ([[com.eiipii.etcd.client.model.EtcdKey EtcdKey]]s, [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]]s, hidden nodes and [[com.eiipii.etcd.client.model.EtcdValue EtcdValue]]s) for key-value methods is using the [[com.eiipii.etcd.client.model.EtcdModel EtcdModel]] object.
 *
 * ===Statistics===
 *
 * The implementation of methods for retrieving etcd cluster statistics also follows the specification given in the [[https://coreos.com/etcd/docs/latest/v2/api.html etcd Client API]] and [[https://coreos.com/etcd/docs/latest/v2/other_apis.html etcd Admin API]].
 *
 * ===Authentication Methods===
 *
 * The implementation of authentication methods follows the specification given in the [[https://coreos.com/etcd/docs/latest/v2/auth_api.html etcd authentication and security API]].
 * With the help of these methods, a system administrator can enable authentication and create users and roles with different levels of access to etcd.
 * etcd only allows authentication on authentication methods and key-value methods.
 *
 * ===Member Methods===
 *
 * The implementation for member methods follows the specification given in the [[https://coreos.com/etcd/docs/latest/v2/members_api.html etcd  Members API]].
 * They allow an administrator to configure a cluster, by changing the members of a cluster.
 *
 * @param etcdConfig [[com.eiipii.etcd.client.EtcdConfiguration EtcdConfiguration]] with information about an endpoint to which requests will be sent and optional authorization data.
 *
 */

class EtcdClient(etcdConfig: EtcdConfiguration) {

  /**
   * Create a new EtcdClient instance with authorization configuration.
   *
   * Be aware that each EtcdClient instance must be closed after it is no longer used.
   *
   * For an example of a set up of an EtcdClient with authentication see this [[http://etcdscalaclient.eiipii.com/Client/Authentication%20Methods.html#a-possible-work-flow example]] in our user's guide.
   *
   * @param auth Case class providing a login and a password used for authorization.
   */
  def withAuthorization(auth: EtcdUserAuthenticationData): EtcdClient = EtcdClient(etcdConfig.withAuthorization(auth))

  /**
   * Create a new EtcdClient instance with support for TLS.
   *
   * See the [[https://coreos.com/etcd/docs/latest/v2/security.html V2 Security Model]] documentation in order to learn about the possible use cases of etcd with TLS.
   *
   * The following is an example of a possible configuration with TLS taken from out tests:
   *
   * {{{
   *  val clientCert = this.getClass.getResourceAsStream("/ssl/client.crt")
   *  val keyFile = this.getClass.getResourceAsStream("/ssl/client_nopass.PKCS8")
   *  val caCert = this.getClass.getResourceAsStream("/ssl/dev-ca.crt")
   *  val sslContext = SslContextBuilder.forClient()
   *  .trustManager(caCert)
   *  .keyManager(clientCert, keyFile)
   *  .build()
   *
   *  val endpoint: String = s"https://localhost:4001"
   *  val etcdCli = new EtcdClient(EtcdDSL.config(endpoint))
   *  val secureCli: EtcdClient = etcdCli.withTLS(sslContext)
   * }}}
   *
   *  Make sure to fill in the details for properly configuring the TLS protocol according to your needs.
   * @param nettySslContext Look for io.netty.handler.ssl.SslContext in the [[http://netty.io/4.0/api/index.html Netty Javadoc]].
   */

  def withTLS(nettySslContext: SslContext): EtcdClient = EtcdClient(etcdConfig.withTLS(nettySslContext))
  /**
   * Close the internal resources.
   */
  def close(): Unit = asyncHttpClient.close()

  /*
  Uses DefaultAsyncHttpClientConfig and the parameter etcdConfig in order to configure asyncHttpClient used internally for http requests to etcd.
   */
  private lazy val constructAsyncHttpConfiguration: DefaultAsyncHttpClientConfig = {
    import Dsl._
    val baseBuilder = new DefaultAsyncHttpClientConfig.Builder()
      .setKeepAlive(true)
    val postAuth = etcdConfig.authorization match {
      case Some(x) => baseBuilder.setRealm(
        basicAuthRealm(x.userLogin, x.password).setUsePreemptiveAuth(true)
      )
      case None => baseBuilder
    }

    val configTLS = etcdConfig.optContext match {
      case Some(nettySslContext) =>
        postAuth.setSslContext(nettySslContext)
      case None =>
        postAuth
    }
    configTLS.build()
  }

  /**
   * Base path to keys node, where keys are stored in etcd.
   */
  private val baseUrl = etcdConfig.conn + "/v2/keys"

  /*
  DefaultAsyncHttpClient used internally for http requests to etcd.
   */
  private lazy val asyncHttpClient = new DefaultAsyncHttpClient(constructAsyncHttpConfiguration)

  // Private objects containing implicits for handling parameters and for asynchronous implementation of the EtcdClient.
  import EtcdClientParamatersCreation._
  import RequestToFutureImplicit._

  // Key-Value methods

  /**
   * Sets or updates a key in a specific etcd node; also, sets and unsets TTL.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#setting-the-value-of-a-key Setting the value of a key]]".
   * See also "[[https://coreos.com/etcd/docs/latest/v2/api.html#using-key-ttl Using key TTL]]", "[[https://coreos.com/etcd/docs/latest/v2/api.html#creating-a-hidden-node Creating a hidden node]]" and "[[https://coreos.com/etcd/docs/latest/v2/api.html#atomic-compare-and-swap Atomic Compare-and-Swap]]".
   *
   * The value of a key can be updated by choosing the same key, but a different value.
   *
   * Optionally one can choose a TTL (time to live), which is a lifespan in seconds for the key-value.
   * When it expires, the key is deleted.
   *
   * It is also possible to update the TTL of a key by specifying a new value for the ttlOp parameter with the same key.
   * A similar effect can be achieved using [[com.eiipii.etcd.client.EtcdClient.refreshTTL EtcdClient.refreshTTL]], except that this last operation does not notify watchers.
   *
   * The TTL can be unset by updating the key with the parameter ttlOp set to None and with condition KeyMustExist set to true.
   *
   * Additionally, etcd allows to conditionally compare and swap values.
   * This can be done by setting the conditionOp parameter to any of the case classes extending trait [[com.eiipii.etcd.client.model.CompareAndSwapCondition CompareAndSwapCondition]].
   *
   * @param key [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] that is to be set or updated.
   * @param value [[com.eiipii.etcd.client.model.EtcdValue EtcdValue]] to be associated to key.
   * @param ttlOp Life span in seconds of key-value.
   *              Defaults to None.
   * @param conditionOp One of case classes [[com.eiipii.etcd.client.model.KeyMustExist KeyMustExist]], [[com.eiipii.etcd.client.model.KeyMustHaveIndex KeyMustHaveIndex]] or [[com.eiipii.etcd.client.model.KeyMustHaveValue KeyMustHaveValue ]] extending [[com.eiipii.etcd.client.model.CompareAndSwapCondition CompareAndSwapCondition]].
   *                    Defaults to None.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdSetKeyResponse EtcdSetKeyResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdSetKeyResult EtcdSetKeyResult]].
   */
  def setKey(key: EtcdKey, value: EtcdValue, ttlOp: Option[EtcdTTL] = None, conditionOp: Option[CompareAndSwapCondition] = None): Future[EtcdSetKeyResult] = {
    val baseRequest = asyncHttpClient.preparePut(baseUrl + key.key).addFormParam("value", value.value)
    build(baseRequest)(Seq(addSwapCondition(conditionOp), addTTL(ttlOp)))
  }

  /**
   * Sets a watch, which returns a get response if there is a change in a key in etcd.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#waiting-for-a-change Waiting for a change]]".
   *
   * Use case of the waitForKey method:
   *
   *
   * Optionally, one can choose an etcd modified index for which the watch is being set.
   * An etcd modified index is the integer associated to the operation modifying the state of a key in etcd.
   * If an index `i` is chosen, such that there were subsequent changes in the state of the key after the operation associated to `i`, etcd returns an [[com.eiipii.etcd.client.model.EtcdGetKeyResponse EtcdGetKeyResponse]] with the state of the key after the first change registered with indexe greater than `i`.
   *
   * For an index `j` greater than the etcd index associated to the last operation performed, the watch will not return a response until a change with an index greater than `j` is made.
   *
   * Also, one can choose to wait for changes in any of the subnodes of a given node.
   * This can be achieved by setting the parameter recursive to true.
   * This parameter defaults to false.
   *
   * @param key                  [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] on watch.
   * @param callback             [[com.eiipii.etcd.client.model.EtcdWaitCallback EtcdWaitCallback]] which will return a [[com.eiipii.etcd.client.model.EtcdGetKeyResponse EtcdGetKeyResponse]] or a throwable once it is called upon.
   * @param modifiedIndexForWait [[com.eiipii.etcd.client.model.EtcdIndex EtcdIndex]] associated to an operation performed on a given key.
   * @param recursive            waits on changes of subnoded of the specified key, when it is set to true.
   *                             Defaults to false.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdWaitForKeyAccepted]] containing the headers of the response  or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]].
   */
  def waitForKey(key: EtcdKey, callback: EtcdWaitCallback, modifiedIndexForWait: Option[EtcdIndex] = None, recursive: Boolean = false): Future[EtcdWaitForKeyResult] = {
    val baseRequest = asyncHttpClient.prepareGet(baseUrl + key.key)
    (
      build(baseRequest)(Seq(addWait(), addWaitIndex(modifiedIndexForWait), addRecursive(recursive))),
      callback
    )
  }

  /**
   * Gets the current state of a key in etcd.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#get-the-value-of-a-key Get the value of a key]]".
   *
   * Note that there is no difference in a GET request for a key and a directory.
   * An GET HTTP request made to a node used as a directory looks the same as a request to a node used as a key.
   * However, there is a difference in terms of a response.
   * The getKey method is intended to handle responses only for requests to nodes used as keys.
   * If it is used to make a request to a directory, it will return an EtcdRequestError with the statusCode field set to 200.
   * This means that etcd accepts the request and returns a response.
   * However EtcdClient does not parse the response.
   * In order to get a response which extracts the JSON returned in the response body into a case class, use [[com.eiipii.etcd.client.EtcdClient.listDir EtcdClient.listDir]].
   *
   * @param key [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] whose status is queried.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdGetKeyResponse EtcdGetKeyResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdGetKeyResult EtcdGetKeyResult]]
   */
  def getKey(key: EtcdKey): Future[EtcdGetKeyResult] = {
    asyncHttpClient.prepareGet(baseUrl + key.key)
  }

  /**
   * Deletes a key from etcd.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#deleting-a-key Deleting a key]]".
   * See also "[[https://coreos.com/etcd/docs/latest/v2/api.html#atomic-compare-and-delete Atomic Compare-and-Delete]]".
   *
   * Optionally etcd allows to conditionally delete a key.
   * This can be done by setting the conditionOp parameter to any of the case classes extending trait [[com.eiipii.etcd.client.model.ConditionalDeleteCondition ConditionalDeleteCondition]].
   *
   * @param key [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] to be deleted.
   * @param deleteConditionOp One of case classes [[com.eiipii.etcd.client.model.KeyMustHaveIndex KeyMustHaveIndex]] or [[com.eiipii.etcd.client.model.KeyMustHaveValue KeyMustHaveValue ]] extending [[com.eiipii.etcd.client.model.ConditionalDeleteCondition ConditionalDeleteCondition]].
   *                    Defaults to None.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdDeleteKeyResponse EtcdDeleteKeyResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdDeleteKeyResult EtcdDeleteKeyResult]].
   */

  def deleteKey(key: EtcdKey, deleteConditionOp: Option[ConditionalDeleteCondition] = None): Future[EtcdDeleteKeyResult] = {
    val baseRequest = asyncHttpClient.prepareDelete(baseUrl + key.key)
    build(baseRequest)(Seq(addDeleteCondition(deleteConditionOp)))
  }

  /**
   * Creates an empty directory in the specified path.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#creating-directories Creating Directories]]".
   * See also "[[https://coreos.com/etcd/docs/latest/v2/api.html#using-a-directory-ttl Using a directory TTL]]".
   *
   * One can also choose to create a directory by setting a key in it using [[com.eiipii.etcd.client.EtcdClient.setKey EtcdClient.setKey]].
   *
   * Optionally one can choose a TTL (time to live), which is a lifespan in seconds for the directory.
   * When it expires, the directory is deleted.
   * To update the TTL, use method [[com.eiipii.etcd.client.EtcdClient.refreshDirTTL EtcdClient.refreshDirTTL]].
   *
   * @param dir [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] to be created.
   * @param ttlOp Life span in seconds of the directory.
   *              Defaults to None.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdCreateDirResponse EtcdCreateDirResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdCreateDirResult EtcdCreateDirResult]].
   */
  def createDir(dir: EtcdDirectory, ttlOp: Option[EtcdTTL] = None): Future[EtcdCreateDirResult] = {
    val baseRequest = asyncHttpClient.preparePut(baseUrl + dir.dir).addFormParam("dir", "true")
    build(baseRequest)(Seq(addTTL(ttlOp)))
  }

  /**
   * Deletes a directory from etcd.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#deleting-a-directory Deleting a Directory]]".
   *
   * An empty directory can be deleted form etcd by simply specifying [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] leading to the directory in etcd.
   * However, for a non-empty directory one must also set the recursive parameter (which defaults to false) to true.
   *
   * @param dir [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] to be deleted.
   * @param recursive enables deletion of non-empty directories when set to true.
   *                  Defaults to false.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdCreateDirResponse EtcdCreateDirResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdCreateDirResult EtcdCreateDirResult]].
   */
  def deleteDir(dir: EtcdDirectory, recursive: Boolean = false): Future[EtcdCreateDirResult] = {
    val baseRequest = asyncHttpClient.prepareDelete(baseUrl + dir.dir).addQueryParam("dir", "true")
    build(baseRequest)(Seq(addRecursive(recursive)))
  }

  /**
   * Lists the elements of a directory.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#listing-a-directory Listing a directory]]".
   * See also "[[https://coreos.com/etcd/docs/latest/v2/api.html#atomically-creating-in-order-keys Atomically Creating In-Order Keys]]" for information on the sorted parameter.
   *
   * When the parameter recursive is set to false (as it is by default), it lists the immediate children of the given node.
   * When it is set to true, it also recursively lists subnodes of the directory.
   *
   * When both parameters recursive and sorted are set to true, the list returned is ordered.
   *
   * @param dir [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] to be listed.
   * @param recursive recursively gets all subnodes of a directory when set to true.
   * @param sorted sorts listed key when set to true in combination with the recursive parameter.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdListDirResponse EtcdListDirResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdListDirResult EtcdListDirResult]].
   */
  def listDir(dir: EtcdDirectory, recursive: Boolean = false, sorted: Boolean = false): Future[EtcdListDirResult] = {
    val baseRequest = asyncHttpClient.prepareGet(baseUrl + dir.dir)
    build(baseRequest)(Seq(addRecursive(recursive), addSorted(sorted)))
  }

  /**
   * Updates the TTL of a key without notifying watchers.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#refreshing-key-ttl Refreshing key TTL]]".
   *
   * When a watch is set, and a key is updated using [[com.eiipii.etcd.client.EtcdClient.setKey EtcdClient.setKey]], watchers are notified, triggering any operation conditionally set to follow given a particular response.
   * This includes updates to its TTL.
   * etcd allows updating TTL without notifying watches set on a key, thus not causing such side effects.
   * EtcdClient implements this feature in the refreshTTL method.
   * When a TTL is refreshed, its value cannot be updated.
   *
   * @param key [[com.eiipii.etcd.client.model.EtcdKey EtcdKey]] that is to be updated.
   * @param ttlOp Life span in seconds of key-value .
   *              Defaults to None.
   * @param conditionOp One of case classes [[com.eiipii.etcd.client.model.KeyMustExist KeyMustExist]], [[com.eiipii.etcd.client.model.KeyMustHaveIndex KeyMustHaveIndex]] or [[com.eiipii.etcd.client.model.KeyMustHaveValue KeyMustHaveValue ]] extending [[com.eiipii.etcd.client.model.CompareAndSwapCondition CompareAndSwapCondition]].
   *                    Defaults to None.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdSetKeyResponse EtcdSetKeyResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdSetKeyResult EtcdSetKeyResult]].
   */
  def refreshTTL(key: EtcdKey, ttlOp: EtcdTTL, conditionOp: Option[CompareAndSwapCondition] = None): Future[EtcdSetKeyResult] = {
    val baseRequest = asyncHttpClient.preparePut(baseUrl + key.key).addFormParam("ttl", ttlOp.ttl.toString).addFormParam("refresh", "true")
    build(baseRequest)(Seq(addSwapCondition(conditionOp)))
  }

  /**
   * Refreshes or unsets the TTL of an existing directory.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#using-a-directory-ttl Using a directory TTL]]".
   *
   * Sets a lifespan in seconds for an existing directory.
   * Once it expires, the directory is deleted.
   * Setting ttlOp parameter to None allows to unset TTL.
   *
   * @param dir [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] whose TTL is to be updated.
   * @param ttlOp Life span in seconds of the directory.
   *              Defaults to None.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdCreateDirResponse EtcdCreateDirResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdCreateDirResult EtcdCreateDirResult]].
   */
  def refreshDirTTL(dir: EtcdDirectory, ttlOp: Option[EtcdTTL]): Future[EtcdCreateDirResult] = {
    val baseRequest = asyncHttpClient.preparePut(baseUrl + dir.dir).addFormParam("dir", "true").addFormParam("prevExist", "true")
    build(baseRequest)(Seq(addTTL(ttlOp)))
  }

  /**
   * Creates a key in a specified directory whose name is the current etcd index preceded with leading zeroes.
   *
   * ===Key-Value Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#atomically-creating-in-order-keys Atomically Creating In-Order Keys]]".
   *
   * etcd keeps a counter for each operation performed.
   * The etcd index keeps track of this counter, that is, it is the integer associated to the last operation performed in etcd.
   * Given a specified directory [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]], createKeyFromCurrentEtcdIndex creates a key in that directory named after the etcd index and associates the value inputed through parameter value to it.
   * This allows to create keys which are strictly ordered by those indexes, since etcd indexes increment monotonously.
   *
   * @param dir [[com.eiipii.etcd.client.model.EtcdDirectory EtcdDirectory]] where the new key will be placed.
   * @param value [[com.eiipii.etcd.client.model.EtcdValue EtcdValue]] to be associated to key.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdSetKeyResponse EtcdSetKeyResponse]] or [[com.eiipii.etcd.client.model.EtcdRequestError EtcdRequestError]] extending [[com.eiipii.etcd.client.model.EtcdSetKeyResult EtcdSetKeyResult]].
   */

  def createKeyFromCurrentEtcdIndex(dir: EtcdDirectory, value: EtcdValue): Future[EtcdSetKeyResult] = {
    asyncHttpClient.preparePost(baseUrl + dir.dir).addFormParam("value", value.value)
  }

  //Statistics

  /**
   * Gets statistics from the leader of a cluster, which holds information of the whole cluster.
   *
   * ===Statistics Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#leader-statistics Leader Statistics]]".
   *
   */
  def getLeaderStats: Future[EtcdGetLeaderStatsResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/stats/leader")
  }

  /**
   * Verifies the health of a member of an etcd cluster.
   *
   * ===Statistics Method===
   *
   * Implemented according to etcd Admin API: "[[https://coreos.com/etcd/docs/latest/v2/other_apis.html#checking-health-of-an-etcd-member-node Checking health of an etcd member node]]".
   *
   * @return A Future wrapped around [[com.eiipii.etcd.client.model.EtcdGetHealthResult EtcdGetHealthResult]] with an error or a response containing a boolean, which evaluates to true if the member is healthy, false otherwise.
   */
  def getHealth: Future[EtcdGetHealthResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/health")
  }

  /**
   * Gets statistics from a member of an etcd cluster.
   *
   * ===Statistics Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#self-statistics Self Statistics]]".
   */
  def getSelfStats: Future[EtcdGetSelfStatsResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/stats/self")
  }

  /**
   * Gets statistics about performed operations stored by a node of an etcd cluster.
   *
   * ===Statistics Method===
   *
   * Implemented according to etcd Client API: "[[https://coreos.com/etcd/docs/latest/v2/api.html#store-statistics Store Statistics]]".
   */
  def getStoreStats: Future[EtcdGetStoreStatsResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/stats/store")
  }

  /**
   * Gets the version of an instance of etcd.
   *
   * ===Statistics Method===
   *
   * Implemented according to etcd Admin API: "[[https://coreos.com/etcd/docs/latest/v2/other_apis.html#getting-the-etcd-version Getting the etcd version]]".
   */
  def getVersion: Future[EtcdGetVersionResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/version")
  }

  // Authentication methods

  /**
   * Checks if authentication is enabled.
   *
   * ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdGetAuthenticationStatusResponse EtcdGetAuthenticationStatusResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdGetAuthenticationStatusResult EtcdGetAuthenticationStatusResult]].
   */

  def getAuthenticationStatus: Future[EtcdGetAuthenticationStatusResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/auth/enable")
  }

  /**
   * Enables authentication.
   *
   * ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * In order to enable authentication, the root user must be created first using [[com.eiipii.etcd.client.EtcdClient.addUpdateUser EtcdClient.addUpdateUser]].
   * After authentication only the root user or users with root rights (for example, with the root role) can access and modify permission resources.
   *
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdConfirmationResponse EtcdConfirmationResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdConfirmationResult EtcdConfirmationResult]].
   */
  def enableAuthentication(): Future[EtcdConfirmationResult] =
    asyncHttpClient.preparePut(etcdConfig.conn + "/v2/auth/enable")

  /**
   * Disables authentication.
   *
   * ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdConfirmationResponse EtcdConfirmationResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdConfirmationResult EtcdConfirmationResult]].
   */
  def disableAuthentication(): Future[EtcdConfirmationResult] = {
    asyncHttpClient.prepareDelete(etcdConfig.conn + "/v2/auth/enable")
  }

  /**
   * Adds new users and modifies user details.
   *
   * ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * @param user [[com.eiipii.etcd.client.model.EtcdUserRequestForm EtcdUserRequestForm]] containing the details (login, password, granted and revoked permissions, and roles) to be updated for a given user.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdAddUpdateUserResponse EtcdAddUpdateUserResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdAddUpdateUserResult EtcdAddUpdateUserResult]].
   */
  def addUpdateUser(user: EtcdUserRequestForm): Future[EtcdAddUpdateUserResult] = {
    asyncHttpClient.preparePut(etcdConfig.conn + "/v2/auth/users/" + user.user).setBody(write(user))
  }

  /**
   * Deletes a user.
   *
   *  ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * In this case, it suffices to provide the user in the [[com.eiipii.etcd.client.model.EtcdUserRequestForm EtcdUserRequestForm]].
   *
   * @param user [[com.eiipii.etcd.client.model.EtcdUserRequestForm EtcdUserRequestForm]] containing the details of a given user.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdConfirmationResponse EtcdConfirmationResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdConfirmationResult EtcdConfirmationResult]].
   */
  def deleteUser(user: EtcdUserRequestForm): Future[EtcdConfirmationResult] = {
    asyncHttpClient.prepareDelete(etcdConfig.conn + "/v2/auth/users/" + user.user)
  }

  /**
   * Gets a list of existing users.
   *
   *  ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdGetUsersResponse EtcdGetUsersResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdGetUsersResult EtcdGetUsersResult]].
   */
  def getUsers(): Future[EtcdGetUsersResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/auth/users")
  }

  /**
   * Gets the details of an existing user including permissions granted to her and her roles
   *
   *  ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * In this case, it suffices to provide the user field in the [[com.eiipii.etcd.client.model.EtcdUserRequestForm EtcdUserRequestForm]].
   *
   * @param user [[com.eiipii.etcd.client.model.EtcdUserRequestForm EtcdUserRequestForm]] containing the details of a given user.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdGetUserDetailsResponse EtcdGetUserDetailsResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdGetUserDetailsResult EtcdGetUserDetailsResult]].
   */
  def getUserDetails(user: EtcdUserRequestForm): Future[EtcdGetUserDetailsResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/auth/users/" + user.user)
  }

  /**
   * Adds new roles and modifies role details.
   *
   * ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * @param role [[com.eiipii.etcd.client.model.EtcdRole EtcdRole]] with details of the role to be created or updated.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdAddUpdateRoleResponse EtcdAddUpdateRoleResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdAddUpdateRoleResult EtcdAddUpdateRoleResult]].
   */
  def addUpdateRole(role: EtcdRole): Future[EtcdAddUpdateRoleResult] = {
    asyncHttpClient.preparePut(etcdConfig.conn + "/v2/auth/roles/" + role.role).setBody(write(role))
  }

  /**
   * Deletes a role.
   *
   * ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * In this case, it suffices to provide the role field in the [[com.eiipii.etcd.client.model.EtcdRole EtcdRole]].
   *
   * @param role [[com.eiipii.etcd.client.model.EtcdRole EtcdRole]] with details of the role to be created or updated.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdConfirmationResponse EtcdConfirmationResponse] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdConfirmationResult EtcdConfirmationResult]].
   */
  def deleteRole(role: EtcdRole): Future[EtcdConfirmationResult] = {
    asyncHttpClient.prepareDelete(etcdConfig.conn + "/v2/auth/roles/" + role.role)
  }

  /**
   * Gets a list of existing roles.
   *
   * ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdGetRolesResponse EtcdGetRolesResponse] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]].
   *         extending [[com.eiipii.etcd.client.model.EtcdGetRolesResult EtcdGetRolesResult]].
   */
  def getRoles(): Future[EtcdGetRolesResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/auth/roles")
  }

  /**
   * Gets a list of details of an existing role.
   *
   * ===Authentication Method===
   *
   * Implemented according to etcd Auth and Security API: "[[https://coreos.com/etcd/docs/latest/v2/auth_api.html#api-endpoints API endpoints]]".
   *
   * In this case, it suffices to provide the role field in the [[com.eiipii.etcd.client.model.EtcdRole EtcdRole]].
   *
   * @param role [[com.eiipii.etcd.client.model.EtcdRole EtcdRole]] with details of the role to be created or updated.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdGetRoleDetailsResponse EtcdGetRoleDetailsResponse] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]]
   *         extending [[com.eiipii.etcd.client.model.EtcdGetRoleDetailsResult EtcdGetRoleDetailsResult]].
   */
  def getRoleDetails(role: EtcdRole): Future[EtcdGetRoleDetailsResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/auth/roles/" + role.role)
  }

  // Member methods

  /**
   * Gets a lsi of members of an etcd cluster.
   *
   * ===Member methods===
   *
   * Implemented according to etcd Members API: "[[https://coreos.com/etcd/docs/latest/v2/members_api.html#list-members List members]]".
   *
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdGetMembersResponse EtcdGetMembersResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]]
   *         extending [[com.eiipii.etcd.client.model.EtcdGetMembersResult EtcdGetMembersResult]].
   */
  def getMembers: Future[EtcdGetMembersResult] = {
    asyncHttpClient.prepareGet(etcdConfig.conn + "/v2/members")
  }

  /**
   * Adds a member to an etcd cluster.
   *
   * ===Member methods===
   *
   * Implemented according to etcd Members API: "[[https://coreos.com/etcd/docs/latest/v2/members_api.html#add-a-member Add a member]]".
   *
   * @param peerURLs [[com.eiipii.etcd.client.model.EtcdMemberForm EtcdMemberForm]] containing a list of strings representing the peer URLs of the new member.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdAddMemberResponse EtcdAddMemberResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]]
   *         extending [[com.eiipii.etcd.client.model.EtcdAddMemberResult EtcdAddMemberResult]].
   */
  def addMember(peerURLs: EtcdMemberForm): Future[EtcdAddMemberResult] = {
    asyncHttpClient.preparePost(etcdConfig.conn + "/v2/members").setBody(write(peerURLs)).setHeader("Content-Type", "application/json")
  }

  /**
   * Deletes a member form an etcd cluster.
   *
   * ===Member methods===
   *
   * Implemented according to etcd Members API: "[[https://coreos.com/etcd/docs/latest/v2/members_api.html#delete-a-member Delete a member]]".
   *
   * @param peerID A string identifying another member of the etcd cluster.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdConfirmationResponse EtcdConfirmationResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]]
   *         extending [[com.eiipii.etcd.client.model.EtcdConfirmationResult EtcdConfirmationResult]].
   */
  def deleteMember(peerID: String): Future[EtcdConfirmationResult] = {
    asyncHttpClient.prepareDelete(etcdConfig.conn + "/v2/members/" + peerID)
  }

  /**
   * Changes the peer urls of a member of an etcd cluster.
   *
   * ===Member methods===
   *
   * Implemented according to etcd Members API: "[[https://coreos.com/etcd/docs/latest/v2/members_api.html#change-the-peer-urls-of-a-member Change the peer urls of a member]]".
   *
   * @param peerID A string identifying another member of the etcd cluster.
   * @param peerURLs [[com.eiipii.etcd.client.model.EtcdMemberForm EtcdMemberForm]] containing a list of strings representing the new peer URLs that will replace the old ones.
   * @return A Future wrapped around either [[com.eiipii.etcd.client.model.EtcdConfirmationResponse EtcdConfirmationResponse]] or [[com.eiipii.etcd.client.model.EtcdStandardError EtcdStandardError]]
   *         extending [[com.eiipii.etcd.client.model.EtcdConfirmationResult EtcdConfirmationResult]].
   */
  def changePeerURLs(peerID: String, peerURLs: EtcdMemberForm): Future[EtcdConfirmationResult] = {
    asyncHttpClient.preparePut(etcdConfig.conn + "/v2/members/" + peerID).setBody(write(peerURLs)).setHeader("Content-Type", "application/json")
  }
}

/*
Provides implicit conversion from requests to futures for an asynchronous implementation of the client methods.
 */
private object RequestToFutureImplicit {

  implicit def wrapOnScalaFuture(request: BoundRequestBuilder): Future[EtcdSetKeyResult] = {
    val promise: Promise[EtcdSetKeyResult] = Promise[EtcdSetKeyResult]()
    request.execute(new EtcdPutResponseAsyncHandler(promise))
    promise.future
  }

  implicit def wrapDefaultOperation(request: BoundRequestBuilder): Future[EtcdConfirmationResult] = {
    val promise: Promise[EtcdConfirmationResult] = Promise[EtcdConfirmationResult]()
    request.execute(new EtcdDefaultResponseAsyncHandler(promise))
    promise.future
  }

  // Key-Value Operations

  implicit def wrapDirOnScalaFuture(request: BoundRequestBuilder): Future[EtcdCreateDirResult] = {
    val promise: Promise[EtcdCreateDirResult] = Promise[EtcdCreateDirResult]()
    request.execute(new EtcdCreateDirResponseAsyncHandler(promise))
    promise.future
  }

  implicit def wrapDeleteOperation(request: BoundRequestBuilder): Future[EtcdDeleteKeyResult] = {
    val promise: Promise[EtcdDeleteKeyResult] = Promise[EtcdDeleteKeyResult]()
    request.execute(new EtcdDeleteResponseAsyncHandler(promise))
    promise.future
  }

  implicit def wrapGetOperation(request: BoundRequestBuilder): Future[EtcdGetKeyResult] = {
    val promise: Promise[EtcdGetKeyResult] = Promise[EtcdGetKeyResult]()
    request.execute(new EtcdGetResponseAsyncHandler(promise))
    promise.future
  }

  implicit def wrapWaitOperation(context: (BoundRequestBuilder, EtcdWaitCallback)): Future[EtcdWaitForKeyResult] = {
    val promise: Promise[EtcdWaitForKeyResult] = Promise[EtcdWaitForKeyResult]()
    context._1.execute(new EtcdWaitAcceptedAsyncHandler(promise, context._2))
    promise.future
  }

  implicit def wrapListDirOperation(request: BoundRequestBuilder): Future[EtcdListDirResult] = {
    val promise: Promise[EtcdListDirResult] = Promise[EtcdListDirResult]()
    request.execute(new EtcdListDirAsyncHandler(promise))
    promise.future
  }

  // Statistics

  implicit def wrapLeaderStatsOperation(request: BoundRequestBuilder): Future[EtcdGetLeaderStatsResult] = {
    val promise: Promise[EtcdGetLeaderStatsResult] = Promise[EtcdGetLeaderStatsResult]()
    request.execute(new EtcdLeaderStatsResultAsyncHandler(promise))
    promise.future
  }

  implicit def wrapSelfStatsOperation(request: BoundRequestBuilder): Future[EtcdGetSelfStatsResult] = {
    val promise: Promise[EtcdGetSelfStatsResult] = Promise[EtcdGetSelfStatsResult]()
    request.execute(new EtcdSelfStatsResponseAsyncHandler(promise))
    promise.future
  }

  implicit def wrapStoreStatsOperation(request: BoundRequestBuilder): Future[EtcdGetStoreStatsResult] = {
    val promise: Promise[EtcdGetStoreStatsResult] = Promise[EtcdGetStoreStatsResult]()
    request.execute(new EtcdStoreStatsResponseAsyncHandler(promise))
    promise.future
  }

  implicit def wrapVersionOperation(request: BoundRequestBuilder): Future[EtcdGetVersionResult] = {
    val promise: Promise[EtcdGetVersionResult] = Promise[EtcdGetVersionResult]()
    request.execute(new EtcdVersionResponseAsyncHandler(promise))
    promise.future
  }

  implicit def wrapHealthOperation(request: BoundRequestBuilder): Future[EtcdGetHealthResult] = {
    val promise: Promise[EtcdGetHealthResult] = Promise[EtcdGetHealthResult]()
    request.execute(new EtcdHealthResponseAsyncHandler(promise))
    promise.future
  }

  // Authentication methods

  implicit def wrapAddUpdateUserOperation(request: BoundRequestBuilder): Future[EtcdAddUpdateUserResult] = {
    val promise: Promise[EtcdAddUpdateUserResult] = Promise[EtcdAddUpdateUserResult]()
    request.execute(new EtcdAddUserAsyncHandler(promise))
    promise.future
  }

  implicit def wrapGetUsersOperation(request: BoundRequestBuilder): Future[EtcdGetUsersResult] = {
    val promise: Promise[EtcdGetUsersResult] = Promise[EtcdGetUsersResult]()
    request.execute(new EtcdGetUsersAsyncHandler(promise))
    promise.future
  }

  implicit def wrapAddUpdateRoleOperation(request: BoundRequestBuilder): Future[EtcdAddUpdateRoleResult] = {
    val promise: Promise[EtcdAddUpdateRoleResult] = Promise[EtcdAddUpdateRoleResult]()
    request.execute(new EtcdCreateRoleAsyncHandler(promise))
    promise.future
  }

  implicit def wrapGetUserDetailsOperation(request: BoundRequestBuilder): Future[EtcdGetUserDetailsResult] = {
    val promise: Promise[EtcdGetUserDetailsResult] = Promise[EtcdGetUserDetailsResult]()
    request.execute(new EtcdGetUserDetailsAsyncHandler(promise))
    promise.future
  }

  implicit def wrapGetAuthenticationStatusOperation(request: BoundRequestBuilder): Future[EtcdGetAuthenticationStatusResult] = {
    val promise: Promise[EtcdGetAuthenticationStatusResult] = Promise[EtcdGetAuthenticationStatusResult]()
    request.execute(new EtcdGetAuthenticationStatusAsyncHandler(promise))
    promise.future
  }

  implicit def wrapGetRolesOperation(request: BoundRequestBuilder): Future[EtcdGetRolesResult] = {
    val promise: Promise[EtcdGetRolesResult] = Promise[EtcdGetRolesResult]()
    request.execute(new EtcdGetRolesAsyncHandler(promise))
    promise.future
  }

  implicit def wrapGetRoleDetailsOperation(request: BoundRequestBuilder): Future[EtcdGetRoleDetailsResult] = {
    val promise: Promise[EtcdGetRoleDetailsResult] = Promise[EtcdGetRoleDetailsResult]()
    request.execute(new EtcdGetRoleDetailsAsyncHandler(promise))
    promise.future
  }

  // Members methods

  implicit def wrapGetMembersOperation(request: BoundRequestBuilder): Future[EtcdGetMembersResult] = {
    val promise: Promise[EtcdGetMembersResult] = Promise[EtcdGetMembersResult]()
    request.execute(new EtcdGetMembersAsyncHandler(promise))
    promise.future
  }

  implicit def wrapAddMemberOperation(request: BoundRequestBuilder): Future[EtcdAddMemberResult] = {
    val promise: Promise[EtcdAddMemberResult] = Promise[EtcdAddMemberResult]()
    request.execute(new EtcdAddMemberAsyncHandler(promise))
    promise.future
  }
}

/*
* Optional parameters for requests are also dealt with using implicits.
* This is achieved using the methods defined in the n EtcdClientParamatersCreation private object.
*/

private object EtcdClientParamatersCreation {
  type EtcdApiParameterFunction = BoundRequestBuilder => BoundRequestBuilder

  def addSwapCondition(conditionOp: Option[CompareAndSwapCondition]): EtcdApiParameterFunction = builder => {
    conditionOp match {
      case Some(condition) => condition match {
        case KeyMustExist(state) => builder.addFormParam("prevExist", if (state) "true" else "false")
        case KeyMustHaveIndex(index) => builder.addFormParam("prevIndex", index.index.toString)
        case KeyMustHaveValue(value) => builder.addFormParam("prevValue", value.value)
      }
      case None => builder
    }
  }

  def addDeleteCondition(deleteConditionOp: Option[ConditionalDeleteCondition]): EtcdApiParameterFunction = builder => {
    deleteConditionOp match {
      case Some(KeyMustHaveIndex(index)) => builder.addQueryParam("prevIndex", index.index.toString)
      case Some(KeyMustHaveValue(value)) => builder.addQueryParam("prevValue", value.value)
      case None => builder
    }
  }

  def addTTL(ttlOp: Option[EtcdTTL]): EtcdApiParameterFunction = builder => {
    ttlOp match {
      case Some(ttl) => builder.addFormParam("ttl", ttl.ttl.toString)
      case None => builder
    }
  }

  def addWait(): EtcdApiParameterFunction = builder => {
    builder.addQueryParam("wait", "true")
  }

  //TODO Antonio. Add test
  def addWaitIndex(waitIndex: Option[EtcdIndex]): EtcdApiParameterFunction = builder => {
    waitIndex match {
      case Some(index) => builder.addQueryParam("waitIndex", waitIndex.get.index.toString)
      case None => builder
    }
  }

  def addSorted(sorted: Boolean): EtcdApiParameterFunction = builder => {
    if (sorted) {
      builder.addQueryParam("sorted", "true")
    } else {
      builder
    }
  }

  def addRecursive(recursive: Boolean): EtcdApiParameterFunction = builder => {
    if (recursive) {
      builder.addQueryParam("recursive", "true")
    } else {
      builder
    }
  }

  def build(baseRequest: BoundRequestBuilder)(paramsFunc: Seq[EtcdApiParameterFunction]): BoundRequestBuilder =
    (baseRequest /: paramsFunc) { (req, paramF) =>
      paramF(req)
    }
}