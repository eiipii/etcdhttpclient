/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client

import com.eiipii.etcd.client.model.EtcdUserAuthenticationData
import io.netty.handler.ssl.SslContext

/**
 * Domain specific language with methods for the configuration of an etcd client.
 */

object EtcdDSL {

  /**
   * Returns an instance of [[com.eiipii.etcd.client.EtcdConfiguration EtcdConfiguration]] using the parameter conn as input.
   *
   * @param conn String representing an HTTP endpoint composed of an IP address and a port.
   */
  def config(conn: String): EtcdConfiguration = EtcdConfiguration(conn)
}

/**
 * Configures the EtcdClient with information about an endpoint to which requests will be sent and optional authorization data.
 *
 * @param conn          String representing an HTTP endpoint composed of an IP address and a port.
 * @param authorization Optional [[com.eiipii.etcd.client.model.EtcdUserAuthenticationData EtcdUserAuthenticationData]] that will be used for authorization.
 */

case class EtcdConfiguration(conn: String, authorization: Option[EtcdUserAuthenticationData] = None, optContext: Option[SslContext] = None) {

  /**
   * Adds data for authorization when authentication is enabled.
   *
   * @param auth a login and a password used for authorization.
   * @return [[com.eiipii.etcd.client.EtcdConfiguration EtcdConfiguration]]
   */
  def withAuthorization(auth: EtcdUserAuthenticationData): EtcdConfiguration = this.copy(authorization = Some(auth))

  /**
   * Adds an SSL Context necessary for adding the necessary certificates and key to use TLS.
   *
   * @param nettySslContext Look for io.netty.handler.ssl.SslContext in the [[http://netty.io/4.0/api/index.html Netty Javadoc]].
   * @return [[com.eiipii.etcd.client.EtcdConfiguration EtcdConfiguration]]
   */
  def withTLS(nettySslContext: SslContext): EtcdConfiguration = this.copy(optContext = Some(nettySslContext))
}