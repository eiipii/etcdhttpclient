/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.utils.AbstractSecureSingleContainerEtcdClientTest
import io.netty.handler.ssl.SslContextBuilder
import org.scalatest.exceptions.TestFailedException

import scala.concurrent.Future

class AuthorizationWithTLSEnabledEtcdClientTest extends AbstractSecureSingleContainerEtcdClientTest {

  override def etcdClientPortShift: Int = 0

  override def testScopePrefix: String = "/TLS"

  it should "fail to perform operations without a certificate" in { etcdcli =>

    val key: EtcdKey = genUniqueRandomKey.sample.get
    val value: EtcdValue = genUniqueRandomValue.sample.get

    val response: Future[EtcdSetKeyResult] = etcdcli.setKey(key, value)
    val thrown = the[TestFailedException] thrownBy response.futureValue
    thrown.message.get should include("General SSLEngine problem")
  }

  it should "successfully perform operations once the client has the appropriate certificate" in { etcdcli =>
    val clientCert = this.getClass.getResourceAsStream("/ssl/client.crt")
    val keyFile = this.getClass.getResourceAsStream("/ssl/client_nopass.PKCS8")
    val caCert = this.getClass.getResourceAsStream("/ssl/dev-ca.crt")
    val sslContext = SslContextBuilder.forClient()
      .trustManager(caCert)
      //      .trustManager(InsecureTrustManagerFactory.INSTANCE)
      .keyManager(clientCert, keyFile)
      .build()

    val secureCli: EtcdClient = etcdcli.withTLS(sslContext)

    forAll((genUniqueRandomKey, "key"), (genRandomValue, "value")) { (key: EtcdKey, value: EtcdValue) =>

      val valueSet = secureCli.setKey(key, value)
      inside(valueSet.futureValue) {
        case EtcdSetKeyResponse(headers, body) =>
          body.action should be("set")
          body.node.value should be(value)
      }
    }
  }

}
