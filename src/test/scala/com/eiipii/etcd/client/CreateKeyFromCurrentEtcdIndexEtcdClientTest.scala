/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class CreateKeyFromCurrentEtcdIndexEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/keyFromEtcdIndex"

  it should "create a key using the value of the current Etcd Index which increases with each operation and list all such keys in order" in { etcdcli =>
    val dir: EtcdDirectory = genUniqueRandomDirectoryWithMaxDeep(3).sample.get
    val value1: EtcdValue = genRandomValue.sample.get
    val value2: EtcdValue = genRandomValue.sample.get
    val value3: EtcdValue = genRandomValue.sample.get

    etcdcli.createDir(dir).futureValue
    val createUniqueKeyReq = etcdcli.createKeyFromCurrentEtcdIndex(dir, value1).futureValue

    val firstSetIndex: EtcdIndex = createUniqueKeyReq match {
      case EtcdSetKeyResponse(headers, body) =>
        val firstKey = body.node.key
        //The created key name is 000000${modifiedIndex}
        firstKey.key should endWith(body.node.modifiedIndex.index.toString)
        body.node.modifiedIndex
      case error @ EtcdRequestError(statusCode, headers, body) =>
        fail(s"Error message on put operation $error")
    }

    val createAnotherUniqueKeyReq = etcdcli.createKeyFromCurrentEtcdIndex(dir, value2).futureValue
    inside(createAnotherUniqueKeyReq) {
      case EtcdSetKeyResponse(headers, body) =>
        val secondKey = body.node.key
        //The created key name is 000000${modifiedIndex}
        secondKey.key should endWith(body.node.modifiedIndex.index.toString)
        //And the index is bigger
        firstSetIndex.index should be < body.node.modifiedIndex.index
    }
    //The created key name is 000000${modifiedIndex}
    etcdcli.createKeyFromCurrentEtcdIndex(dir, value3).futureValue
    //and a request for an ordered list
    val listDirReq = etcdcli.listDir(dir, true, true).futureValue

    def flatValues(dir: EtcdListDirNode): List[EtcdListValue] = {
      dir match {
        case EtcdListDirectory(dir, key, nodes, modifiedIndex, createdIndex) => nodes.flatMap(flatValues _)
        case v @ EtcdListValue(dir, key, value, modifiedIndex, createdIndex) => List(v)
      }
    }

    inside(listDirReq) {
      case EtcdListDirResponse(headers, body) =>
        flatValues(body.node).map(_.value) should contain inOrder (value1, value2, value3)
        //and keys are listed in order
        EtcdModel.toPath(body.node.nodes.head.key).last.toInt should be < EtcdModel.toPath(body.node.nodes.tail.head.key).last.toInt
        EtcdModel.toPath(body.node.nodes.tail.head.key).last.toInt should be < EtcdModel.toPath(body.node.nodes.tail.tail.head.key).last.toInt
    }
  }
}

