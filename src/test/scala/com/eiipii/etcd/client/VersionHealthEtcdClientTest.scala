/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class VersionHealthEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/version"

  it should "get health and version information from a node of an etcd cluster" in { etcdcli =>

    val version = etcdcli.getVersion.futureValue
    version.accepted should be(true)
    inside(version) {
      case EtcdGetVersionResponse(etcdserver, etcdcluster) =>
        etcdcluster should startWith("2.3")
    }

    val result = etcdcli.getHealth.futureValue
    result.accepted should be(true)
    inside(result) {
      case EtcdGetHealthResponse(health) =>
        health should be("true")
    }
  }
}
