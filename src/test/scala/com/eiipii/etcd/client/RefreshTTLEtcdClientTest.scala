/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest
import org.scalatest.exceptions.TestFailedException
import org.scalatest.time.{ Milliseconds, Seconds, Span }

class RefreshTTLEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/refereshTTL"

  override implicit val pc = PatienceConfig(Span(2, Seconds), Span(150, Milliseconds))

  it should "refresh the value of the TTL field of a given key without notifying watchers" in { etcdcli =>
    val key = genUniqueRandomKey.sample.get
    val value = genRandomValue.sample.get
    val ttl = EtcdTTL(genRandomInteger.sample.get)
    val newTtl = EtcdTTL(genRandomInteger.sample.get)

    //given an existing key
    inside(etcdcli.setKey(key, value, Some(ttl)).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        val callback = new FutureBasedEtcdWaitCallback()
        val watchingReady = etcdcli.waitForKey(key, callback).futureValue
        inside(etcdcli.refreshTTL(key, newTtl).futureValue) {
          case EtcdSetKeyResponse(headers, body) =>
            body.action should be("set")
            body.node.ttl should be(Some(newTtl))
            //TODO do not check exceptions, check the timeout
            val thrown = the[TestFailedException] thrownBy callback.future.futureValue
            thrown.getMessage should startWith("A timeout occurred waiting for a future to complete.")
        }
    }

    // conditional tests
    inside(etcdcli.setKey(key, value, Some(ttl)).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        val callback = new FutureBasedEtcdWaitCallback()
        val watchingReady = etcdcli.waitForKey(key, callback).futureValue
        // refreshTTL request with condition KeyMustExist
        inside(etcdcli.refreshTTL(key, newTtl, Some(KeyMustExist(true))).futureValue) {
          case EtcdSetKeyResponse(headers, body) =>
            body.action should be("update")
            body.node.ttl should be(Some(newTtl))
            val thrown = the[TestFailedException] thrownBy callback.future.futureValue
            thrown.getMessage should startWith("A timeout occurred waiting for a future to complete.")
        }
    }

    //given an existing key
    inside(etcdcli.setKey(key, value, Some(ttl)).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        val modIndex = body.node.modifiedIndex
        val callback = new FutureBasedEtcdWaitCallback()
        val watchingReady = etcdcli.waitForKey(key, callback).futureValue
        // refreshTTL request with condition KeyMustHaveIndex
        inside(etcdcli.refreshTTL(key, newTtl, Some(KeyMustHaveIndex(modIndex))).futureValue) {
          case EtcdSetKeyResponse(headers, body) =>
            body.action should be("compareAndSwap")
            body.node.ttl should be(Some(newTtl))
            val thrown = the[TestFailedException] thrownBy callback.future.futureValue
            thrown.getMessage should startWith("A timeout occurred waiting for a future to complete.")
        }
    }

    //given an existing key
    inside(etcdcli.setKey(key, value, Some(ttl)).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        val callback = new FutureBasedEtcdWaitCallback()
        val watchingReady = etcdcli.waitForKey(key, callback).futureValue
        // refreshTTL request with condition KeyMustHaveValue
        inside(etcdcli.refreshTTL(key, newTtl, Some(KeyMustHaveValue(value))).futureValue) {
          case EtcdSetKeyResponse(headers, body) =>
            body.action should be("compareAndSwap")
            body.node.ttl should be(Some(newTtl))
            val thrown = the[TestFailedException] thrownBy callback.future.futureValue
            thrown.getMessage should startWith("A timeout occurred waiting for a future to complete.")
        }
    }
  }

}
