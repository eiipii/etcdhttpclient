/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client.model

import org.scalatest.{ FlatSpec, Matchers }

class IncorrectEtcdRequestFormatExceptionTest extends FlatSpec with Matchers {

  it should "provide complete message" in {

    val error = IncorrectEtcdRequestFormatException(
      "handler",
      Some(1),
      Some("body"),
      Some(EtcdHeaders(EtcdClusterId("headerClusterId"), EtcdIndex(2), RaftIndex(3), RaftTerm(4)))
    )

    error.toString should be("Incorrect Etcd response. Handler[handler], statusCode[Some(1)], body:[Some(body)] clustedIdHeader:[Some(EtcdClusterId(headerClusterId))] headers:[Some(EtcdHeaders(EtcdClusterId(headerClusterId),EtcdIndex(2),RaftIndex(3),RaftTerm(4)))]")
  }
}
