/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.model

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ FunSuite, Matchers }

import scala.concurrent.Future

class FutureBasedEtcdWaitCallbackTest extends FunSuite with ScalaFutures with Matchers {

  test("testOnError") {
    //Given
    val callback: FutureBasedEtcdWaitCallback = new FutureBasedEtcdWaitCallback()
    val eventualResponse: Future[EtcdGetKeyResponse] = callback.future

    //When
    callback.onError(new Exception("Netty passed exception"))

    //Then
    val thrown = the[Exception] thrownBy eventualResponse.futureValue
    thrown.getMessage should be("The future returned an exception of type: java.lang.Exception, with message: Netty passed exception.")
  }

}
