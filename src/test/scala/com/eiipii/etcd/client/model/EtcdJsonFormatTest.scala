/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client.model

import org.json4s.native.JsonMethods
import org.json4s.native.Serialization.write
import org.scalatest.{ FlatSpec, Matchers }

class EtcdJsonFormatTest extends FlatSpec with Matchers {

  import EtcdJsonFormat.format

  it should "deserialize leader json" in {
    val response = EtcdGetLeaderStatsResponse(
      "leader",
      List(
        EtcdMemberStats("id1", EtcdCounts(1, 2), EtcdLatency(0.1, 0.2, 0.3, 0.4, 0.5)),
        EtcdMemberStats("id2", EtcdCounts(10, 20), EtcdLatency(1.1, 1.2, 1.3, 1.4, 1.5))
      )
    )

    val json = write(response)
    json should equal("{\"leader\":\"leader\",\"followers\":{\"id1\":{\"counts\":{\"fail\":1,\"success\":2},\"latency\":{\"average\":0.1,\"current\":0.2,\"maximum\":0.3,\"minimum\":0.4,\"standardDeviation\":0.5}},\"id2\":{\"counts\":{\"fail\":10,\"success\":20},\"latency\":{\"average\":1.1,\"current\":1.2,\"maximum\":1.3,\"minimum\":1.4,\"standardDeviation\":1.5}}}}")

    val afterSerialization = JsonMethods.parse(json).extract[EtcdGetLeaderStatsResponse]
    afterSerialization should equal(response)
  }

  it should "deserialize a list dir json response" in {

    val json = "{\"action\":\"get\",\"node\":{\"key\":\"/keyFromEtcdIndex/d1/wovaftelzfkWbhzzxbjtuIktmups7wOHl5npLc3coy1ongromtAvaso/jvwLAq7ijzLa51mqMyoemZ4agc3DYba2tvylkoxWxm4r3xezlJnppR/aLfwship5ogSikzobpkpaegyrsqhistdmhzdonhjnabnikx38zmqffpir/otegjhiagj3nhetzuMalg3iwcpluc2xngmnszzvnJsySvrzbcluk2pu6/haRrpP/ezbWEy2qWmd9dmngvm0AxbucytSzkhe7dd5qkxz7mt/vt2U\",\"dir\":true,\"nodes\":[{\"key\":\"/keyFromEtcdIndex/d1/wovaftelzfkWbhzzxbjtuIktmups7wOHl5npLc3coy1ongromtAvaso/jvwLAq7ijzLa51mqMyoemZ4agc3DYba2tvylkoxWxm4r3xezlJnppR/aLfwship5ogSikzobpkpaegyrsqhistdmhzdonhjnabnikx38zmqffpir/otegjhiagj3nhetzuMalg3iwcpluc2xngmnszzvnJsySvrzbcluk2pu6/haRrpP/ezbWEy2qWmd9dmngvm0AxbucytSzkhe7dd5qkxz7mt/vt2U/00000000000000000015\",\"value\":\"zbwvt0\",\"modifiedIndex\":15,\"createdIndex\":15},{\"key\":\"/keyFromEtcdIndex/d1/wovaftelzfkWbhzzxbjtuIktmups7wOHl5npLc3coy1ongromtAvaso/jvwLAq7ijzLa51mqMyoemZ4agc3DYba2tvylkoxWxm4r3xezlJnppR/aLfwship5ogSikzobpkpaegyrsqhistdmhzdonhjnabnikx38zmqffpir/otegjhiagj3nhetzuMalg3iwcpluc2xngmnszzvnJsySvrzbcluk2pu6/haRrpP/ezbWEy2qWmd9dmngvm0AxbucytSzkhe7dd5qkxz7mt/vt2U/00000000000000000013\",\"value\":\"qenowselhqziitpJeeglhfrsjzuxwhujaoggxmfjh1jhPfiVa7dafe6emgpdjjjMKfgesglbgkadoOegzfejrmzyk\",\"modifiedIndex\":13,\"createdIndex\":13},{\"key\":\"/keyFromEtcdIndex/d1/wovaftelzfkWbhzzxbjtuIktmups7wOHl5npLc3coy1ongromtAvaso/jvwLAq7ijzLa51mqMyoemZ4agc3DYba2tvylkoxWxm4r3xezlJnppR/aLfwship5ogSikzobpkpaegyrsqhistdmhzdonhjnabnikx38zmqffpir/otegjhiagj3nhetzuMalg3iwcpluc2xngmnszzvnJsySvrzbcluk2pu6/haRrpP/ezbWEy2qWmd9dmngvm0AxbucytSzkhe7dd5qkxz7mt/vt2U/00000000000000000014\",\"value\":\"o\",\"modifiedIndex\":14,\"createdIndex\":14}],\"modifiedIndex\":12,\"createdIndex\":12}}"

    val listBody = JsonMethods.parse(json).extract[EtcdListDirBody]

    listBody.action should be("get")

  }

  it should "deserialize a complete leader info json" in {

    val json = "{\"leader\":\"8a03016493c7cf16\",\"followers\":{\"7acc72c74bf53218\":{\"latency\":{\"current\":0.002256,\"average\":0.004154931818181816,\"standardDeviation\":0.00633733684888695,\"minimum\":0.000513,\"maximum\":0.043356},\"counts\":{\"fail\":0,\"success\":44}},\"da55c7bc1a109e37\":{\"latency\":{\"current\":0.001096,\"average\":0.0038338888888888867,\"standardDeviation\":0.003619407221092811,\"minimum\":0.000802,\"maximum\":0.017382},\"counts\":{\"fail\":0,\"success\":45}}}}"

    val afterSerialization = JsonMethods.parse(json).extract[EtcdGetLeaderStatsResponse]

    val expected = EtcdGetLeaderStatsResponse(
      "8a03016493c7cf16",
      List(
        EtcdMemberStats(
          "7acc72c74bf53218",
          EtcdCounts(0, 44),
          EtcdLatency(0.004154931818181816, 0.002256, 0.043356, 0.000513, 0.00633733684888695)
        ),
        EtcdMemberStats(
          "da55c7bc1a109e37",
          EtcdCounts(0, 45),
          EtcdLatency(0.0038338888888888867, 0.001096, 0.017382, 8.02E-4, 0.003619407221092811)
        )
      )
    )
    afterSerialization should equal(expected)
  }
}
