/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class ConditionalDeleteClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/delete"

  it should "conditionally delete keys in etcd" in { etcdcli =>
    val key = genUniqueRandomKey.sample.get
    val value = genRandomValue.sample.get

    //given an existing key
    val deletedF = for {
      created <- etcdcli.setKey(key, value)
      deleted <- etcdcli.deleteKey(key, Some(KeyMustHaveValue(value)))
    } yield deleted

    inside(deletedF.futureValue) {
      case EtcdDeleteKeyResponse(headers, body) =>
        body.action should be("compareAndDelete")
    }

    //given an existing key
    inside(etcdcli.setKey(key, value).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        val modIndex = body.node.modifiedIndex
        //and a delete request with a KeyMustHaveIndexCondition
        inside(etcdcli.deleteKey(key, Some(KeyMustHaveIndex(modIndex))).futureValue) {
          case EtcdDeleteKeyResponse(headers, body) =>
            body.action should be("compareAndDelete")
        }
    }

  }
}
