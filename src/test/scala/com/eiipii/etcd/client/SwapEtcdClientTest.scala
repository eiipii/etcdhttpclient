/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class SwapEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/swap"

  it should "compare and swap values in etcd" in { etcdcli =>
    val key = genUniqueRandomKey.sample.get
    val value = genRandomValue.sample.get
    val newvalue = genRandomValue.sample.get
    val thirdValue = genRandomValue.sample.get
    val fourthValue = genRandomValue.sample.get
    //given a non existing key with condition KeyMustExist set to false
    inside(etcdcli.setKey(key, value, None, Some(KeyMustExist(false))).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        body.node.value should be(value)
    }
    //given an existing key with condition KeyMustExist set to true
    inside(etcdcli.setKey(key, newvalue, None, Some(KeyMustExist(true))).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        body.node.value should be(newvalue)
        val modIndex = body.node.modifiedIndex
        //given an existing key with condition MustHaveIndex() and the modified index of the previous put request
        inside(etcdcli.setKey(key, thirdValue, None, Some(KeyMustHaveIndex(modIndex))).futureValue) {
          case EtcdSetKeyResponse(headers, body) =>
            body.node.value should be(thirdValue)
            //given an existing key with condition MustHaveValue() and the current value of the key
            //Also, Ttl set.
            val randomInt = EtcdTTL(genRandomInteger.sample.get)
            inside(etcdcli.setKey(key, fourthValue, Some(randomInt), Some(KeyMustHaveValue(thirdValue))).futureValue) {
              case EtcdSetKeyResponse(headers, body) =>
                body.node.value should be(fourthValue)
                body.node.ttl should be(Some(randomInt))
            }
        }
    }
  }

}
