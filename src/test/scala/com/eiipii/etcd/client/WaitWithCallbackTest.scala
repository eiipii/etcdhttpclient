/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

import scala.concurrent.Future

class WaitWithCallbackTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/wait"

  it should "wait on a GET query and watch for changes" in { etcdcli =>
    //given an etcdclient
    //a key
    val key = genUniqueRandomKey.sample.get
    //and several different values
    val valueSeed = genRandomValue.sample.get
    val value1 = EtcdValue(s"1${valueSeed.value}")
    val value2 = EtcdValue(s"2${valueSeed.value}")
    //and the key has value 1
    val setValue1Req = etcdcli.setKey(key, value1).futureValue
    //and a modified index
    val modIndx = setValue1Req match {
      case EtcdSetKeyResponse(headers, body) =>
        body.node.modifiedIndex
      case error @ EtcdRequestError(statusCode, headers, body) =>
        fail(s"Error message on put operation $error")
    }
    val waitCallback = new FutureBasedEtcdWaitCallback()
    //when a wait call with callback is executed
    val waitCall: Future[EtcdWaitForKeyResult] = etcdcli.waitForKey(key, waitCallback)
    //then the wait call must be accepted
    val accepted: EtcdWaitForKeyResult = waitCall.futureValue
    accepted.accepted should be(true)
    inside(accepted) {
      case EtcdWaitForKeyAccepted(headers) =>
        headers.xEtcdIndex.index should be >= 0
    }
    //and the callback is not called yet
    waitCallback.isCompleted should be(false)
    //and after a set on the key
    etcdcli.setKey(key, value2).futureValue
    //the callback is called
    eventually {
      waitCallback.isCompleted should be(true)
      waitCallback.future.futureValue.body.node.value should be(value2)
    }

    val waitCallback2 = new FutureBasedEtcdWaitCallback()
    //when a wait call with a second callback and the modified index of a previous operation plus 1 is executed
    val waitCall2: Future[EtcdWaitForKeyResult] = etcdcli.waitForKey(key, waitCallback2, Some(EtcdIndex(modIndx.index + 1)))
    //then the wait call must be accepted
    val accepted2: EtcdWaitForKeyResult = waitCall2.futureValue
    accepted2.accepted should be(true)
    inside(accepted2) {
      case EtcdWaitForKeyAccepted(headers) =>
        headers.xEtcdIndex.index should be >= 0
    }
    //and the callback is called.
    eventually {
      waitCallback2.isCompleted should be(true)
      waitCallback2.future.futureValue.body.node.value should be(value2)
    }
  }
}
