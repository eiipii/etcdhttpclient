/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client.utils

import java.util.concurrent.atomic.AtomicInteger

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.{ EtcdClusterContainers, EtcdContainer, EtcdContainerWithTLS }
import com.eiipii.etcd.client.{ EtcdClient, EtcdDSL }
import com.typesafe.scalalogging.LazyLogging
import org.scalacheck.Gen
import org.scalatest._
import org.scalatest.concurrent.{ Eventually, ScalaFutures }
import org.scalatest.prop.{ GeneratorDrivenPropertyChecks, PropertyChecks }
import org.scalatest.time.{ Milliseconds, Seconds, Span }

import scala.concurrent.ExecutionContext

abstract class AbstractSecureSingleContainerEtcdClientTest extends AbstractEtcdClientTest with EtcdContainerWithTLS {
  override protected def beforeAll(): Unit = {
    startAllOrFail()
  }

  override protected def afterAll(): Unit = {
    stopAllQuietly()
  }

}

abstract class AbstractSingleContainerEtcdClientTest extends AbstractEtcdClientTest with EtcdContainer {

  override protected def beforeAll(): Unit = {
    startAllOrFail()
  }

  override protected def afterAll(): Unit = {
    stopAllQuietly()
  }
}

abstract class AbstractCommonContainerEtcdClientTest extends AbstractEtcdClientTest with EtcdContainer {

  override def etcdClientPortShift: Int = AbstractCommonContainerEtcdClientTest.commonEtcdClientPortShift
}

object AbstractCommonContainerEtcdClientTest {
  val commonEtcdClientPortShift: Int = 0
}

abstract class AbstractMultipleContainerEtcdClientTest extends AbstractEtcdClientTest with EtcdClusterContainers {

  override protected def beforeAll(): Unit = {
    startAllOrFail()
  }

  override protected def afterAll(): Unit = {
    stopAllQuietly()
  }
}

abstract class AbstractEtcdClientTest extends fixture.FlatSpec
    with Matchers
    with AppendedClues
    with Eventually
    with Inside
    with OptionValues
    with PropertyChecks
    with GeneratorDrivenPropertyChecks
    with ScalaFutures
    with LazyLogging
    with BeforeAndAfterAll
    with EtcdModelGenerators {

  implicit val executionContext = ExecutionContext.Implicits.global

  def getEtcdClientConnection: String

  //TODO make tests faster, avoid waiting
  implicit val pc = PatienceConfig(Span(3, Seconds), Span(1500, Milliseconds))

  override type FixtureParam = EtcdClient

  def withFixture(test: OneArgTest): Outcome = {
    val etcdcli = EtcdClient(EtcdDSL.config(getEtcdClientConnection))
    try {
      withFixture(test.toNoArgTest(etcdcli))
    } finally etcdcli.close()

  }

}

trait EtcdModelGenerators {

  val counter = new AtomicInteger()

  def testScopePrefix: String

  def genUniqueRandomKey: Gen[EtcdKey] = for {
    listNode <- Gen.nonEmptyListOf(Gen.identifier)
  } yield EtcdModel.key(s"$testScopePrefix/k${counter.addAndGet(1)}/" + listNode.mkString("/"))

  def genUniqueRandomKeyWithMaxDeep(deep: Int): Gen[EtcdKey] = for {
    listNode <- Gen.nonEmptyListOf(Gen.identifier)
  } yield EtcdModel.key(s"$testScopePrefix/k${counter.addAndGet(1)}/" + listNode.take(deep).mkString("/"))

  def genRandomValue: Gen[EtcdValue] = for {
    id <- Gen.identifier
  } yield EtcdModel.value(id)

  def genUniqueRandomValue: Gen[EtcdValue] = for {
    id <- Gen.identifier
  } yield EtcdModel.value(s"${counter.addAndGet(1)}_$id")

  def genUniqueRandomDirectory: Gen[EtcdDirectory] = for {
    listNode <- Gen.nonEmptyListOf(Gen.identifier)
  } yield EtcdModel.directory(s"$testScopePrefix/d${counter.addAndGet(1)}/" + listNode.mkString("/") + "/")

  def genUniqueRandomDirectoryWithMaxDeep(deep: Int): Gen[EtcdDirectory] = for {
    listNode <- Gen.nonEmptyListOf(Gen.identifier)
  } yield EtcdModel.directory(s"$testScopePrefix/d${counter.addAndGet(1)}/" + listNode.take(deep).mkString("/") + "/")

  def genUniqueRandomString: Gen[String] = for {
    id <- Gen.identifier
  } yield s"k${counter.addAndGet(1)}" + id

  def genRandomInteger: Gen[Int] = Gen.chooseNum(15, 600)
}