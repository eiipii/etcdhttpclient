/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class DeleteEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/delete"

  it should "delete a key from etcd" in { etcdcli =>

    forAll((genUniqueRandomKey, "key"), (genRandomValue, "value")) { (key: EtcdKey, value: EtcdValue) =>
      val deleteF = for {
        put <- etcdcli.setKey(key, value)
        delete <- etcdcli.deleteKey(key)
      } yield delete

      val deleteResult = deleteF.futureValue
      deleteResult.accepted should be(true)
      inside(deleteResult) {
        case EtcdDeleteKeyResponse(headers, body) =>
          body.action should be("delete")
      }
    }
  }

}
