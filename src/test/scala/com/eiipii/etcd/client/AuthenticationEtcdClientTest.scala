/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.utils.AbstractSingleContainerEtcdClientTest
import org.scalatest.CancelAfterFailure

class AuthenticationEtcdClientTest extends AbstractSingleContainerEtcdClientTest with CancelAfterFailure {

  override def testScopePrefix: String = "/authentication"

  override def etcdClientPortShift: Int = 1

  def withAuthorization(userData: EtcdUserAuthenticationData)(test: EtcdClient => Unit)(implicit baseClient: EtcdClient): Unit = {
    val authorizedClient = baseClient.withAuthorization(userData)
    try {
      test(authorizedClient)
    } finally {
      authorizedClient.close()
    }
  }

  it should "not allow authentication if there is no root user" in { implicit etcdcli =>

    val authenticationStatusResult = etcdcli.getAuthenticationStatus.futureValue
    authenticationStatusResult.accepted should be(true)
    inside(authenticationStatusResult) {
      case EtcdGetAuthenticationStatusResponse(clusterId, body) =>
        body.enabled should be(Some(false))
    }

    val stdError = etcdcli.enableAuthentication().futureValue
    stdError.accepted should be(false)
    inside(stdError) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("auth: No root user available, please create one")
    }
  }

  it should "be able to create a new user" in { implicit etcdcli =>

    val newUser: EtcdUserRequestForm = EtcdUserRequestForm("antonio", Some("password"))
    val addUpdateUserResult = etcdcli.addUpdateUser(newUser).futureValue
    addUpdateUserResult.accepted should be(true)
    inside(addUpdateUserResult) {
      case EtcdAddUpdateUserResponse(clusterId, body) =>
        body.user shouldBe newUser.user
        body.roles should be(List())
        clusterId.id shouldBe a[String]
    }

    val rootUser: EtcdUserRequestForm = EtcdUserRequestForm("root", Some("password"))
    inside(etcdcli.addUpdateUser(rootUser).futureValue) {
      case EtcdAddUpdateUserResponse(clusterId, body) =>
        body.user shouldBe rootUser.user
        body.roles should be(List("root"))
    }
  }

  it should "be able to create a new role" in { implicit etcdcli =>
    val newRole: EtcdRole = EtcdRole("usersAdmin", Some(EtcdPermission(EtcdKV(List("/users/*"), List("/users/*")))))

    val updateRoleResult = etcdcli.addUpdateRole(newRole).futureValue
    updateRoleResult.accepted should be(true)
    inside(updateRoleResult) {
      case EtcdAddUpdateRoleResponse(clusterId, body) =>
        body.role shouldBe newRole.role
        body.permissions.get.kv.read should be(List("/users/*"))
        body.permissions.get.kv.write should be(List("/users/*"))
        clusterId.id shouldBe a[String]
    }

    inside(etcdcli.addUpdateRole(newRole).futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("auth: Role usersAdmin already exists.")
    }
  }

  it should "be able to enable authentication" in { implicit etcdcli =>

    val authenticationResult = etcdcli.enableAuthentication().futureValue
    authenticationResult.accepted should be(true)
    inside(authenticationResult) {
      case EtcdConfirmationResponse(clusterID) =>
        clusterID.id shouldBe a[String]
    }
  }

  it should "be able to get user details" in { implicit etcdcli =>
    val rootUser: EtcdUserRequestForm = EtcdUserRequestForm("root", Some("password"))
    val rootAccess = EtcdUserAuthenticationData("root", "password")
    //without authorization
    inside(etcdcli.getUserDetails(rootUser).futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with authorization
    withAuthorization(rootAccess) { etcdClientRoot =>
      val userDetailsResult = etcdClientRoot.getUserDetails(rootUser).futureValue
      userDetailsResult.accepted should be(true)
      inside(userDetailsResult) {
        case EtcdGetUserDetailsResponse(clusterId, body) =>
          body.user shouldBe rootUser.user
          body.roles should be(List(EtcdRole("root", Some(EtcdPermission(EtcdKV(List("/*"), List("/*")))), None, None)))
      }
    }
  }

  it should "be able to get role details" in { implicit etcdcli =>
    val newRole: EtcdRole = EtcdRole("usersAdmin", Some(EtcdPermission(EtcdKV(List("/users/*"), List("/users/*")))))
    val rootAccess = EtcdUserAuthenticationData("root", "password")
    //without authorization
    inside(etcdcli.getRoleDetails(newRole).futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with authorization
    withAuthorization(rootAccess) { etcdClientRoot =>
      val roleDetailsResult = etcdClientRoot.getRoleDetails(newRole).futureValue
      roleDetailsResult.accepted should be(true)
      inside(roleDetailsResult) {
        case EtcdGetRoleDetailsResponse(clusterId, body) =>
          body.role shouldBe newRole.role
          body.permissions.get.kv.read.head should be("/users/*")
      }
    }
  }

  it should "be able to list existing users" in { implicit etcdcli =>
    val rootAccess = EtcdUserAuthenticationData("root", "password")

    //without authorization
    inside(etcdcli.getUsers().futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with authorization
    withAuthorization(rootAccess) { etcdClientRoot =>
      val usersResult = etcdClientRoot.getUsers().futureValue
      usersResult.accepted should be(true)
      inside(usersResult) {
        case EtcdGetUsersResponse(clusterId, body) =>
          body.users.head.user should be("antonio")
          body.users.tail.head.user should be("root")
          body.users.tail.head.roles.head.role should be("root")
          body.users.tail.head.roles.head.permissions.get.kv.read.head should be("/*")
      }
    }
  }

  it should "be able to list existing roles" in { implicit etcdcli =>
    val rootAccess = EtcdUserAuthenticationData("root", "password")

    //without authorization
    inside(etcdcli.getRoles().futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with authorization
    withAuthorization(rootAccess) { etcdClientRoot =>
      val getRolesResult = etcdClientRoot.getRoles().futureValue
      getRolesResult.accepted should be(true)
      inside(getRolesResult) {
        case EtcdGetRolesResponse(clusterId, body) =>
          body.roles.head.role should be("guest")
          body.roles.tail.head.role should be("root")
          body.roles.tail.head.permissions.get.kv.read.head should be("/*")
      }
    }
  }

  it should "be able to update a user with a new role" in { implicit etcdcli =>
    val rootAccess = EtcdUserAuthenticationData("root", "password")
    val newUser: EtcdUserRequestForm = EtcdUserRequestForm("antonio", None, List(), List("usersAdmin"))

    //without authorization
    inside(etcdcli.addUpdateUser(newUser).futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with authorization
    withAuthorization(rootAccess) { etcdClientRoot =>
      inside(etcdClientRoot.addUpdateUser(newUser).futureValue) {
        case EtcdAddUpdateUserResponse(clusterId, body) =>
          body.user shouldBe newUser.user
          body.roles.head should be("usersAdmin")
      }
    }
  }

  it should "be able to update a role with a new permission" in { implicit etcdcli =>
    val rootAccess = EtcdUserAuthenticationData("root", "password")
    val updateRole: EtcdRole = EtcdRole("usersAdmin", None, Some(EtcdPermission(EtcdKV(List("/roles/*"), List("/roles/*")))))

    //without authorization
    inside(etcdcli.addUpdateRole(updateRole).futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with authorization
    withAuthorization(rootAccess) { etcdClientRoot =>
      inside(etcdClientRoot.addUpdateRole(updateRole).futureValue) {
        case EtcdAddUpdateRoleResponse(clusterId, body) =>
          body.role shouldBe updateRole.role
          body.permissions.get.kv.read.head should be("/roles/*")
      }
    }
  }

  it should "be able to update a role (revoke read/write rights to guest role)" in { implicit etcdcli =>
    val newRole: EtcdRole = EtcdRole("guest", None, None, Some(EtcdPermission(EtcdKV(List("/*"), List("/*")))))
    val rootAccess = EtcdUserAuthenticationData("root", "password")

    //without authorization
    inside(etcdcli.addUpdateRole(newRole).futureValue) {
      //with authorization
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    withAuthorization(rootAccess) { etcdClientRoot =>
      inside(etcdClientRoot.addUpdateRole(newRole).futureValue) {
        case EtcdAddUpdateRoleResponse(clusterId, body) =>
          body.role shouldBe newRole.role
          body.permissions.get.kv.read should be(List())
          body.permissions.get.kv.write should be(List())
          clusterId.id shouldBe a[String]
      }
    }
  }

  it should "block any put operation, once authentication is enabled and write permission is revoked for guests" in { implicit etcdcli =>
    val rootAccess = EtcdUserAuthenticationData("root", "password")
    val newKey = genUniqueRandomKey.sample.get
    val newValue = genRandomValue.sample.get

    // without the proper credentials
    inside(etcdcli.setKey(newKey, newValue).futureValue) {
      case error @ EtcdRequestError(status, headers, body) =>
        inside(body) {
          case EtcdError(cause, errorCode, index, message) =>
            errorCode should be(110)
        }
    }

    //with root access
    withAuthorization(rootAccess) { etcdClientRoot =>
      inside(etcdClientRoot.setKey(newKey, newValue, None, None).futureValue) {
        case EtcdSetKeyResponse(headers, body) =>
          body.action should be("set")
          body.node.value should be(newValue)
      }
    }
  }

  it should "be able to delete a user" in { implicit etcdcli =>
    val rootAccess = EtcdUserAuthenticationData("root", "password")
    val newUser: EtcdUserRequestForm = EtcdUserRequestForm("antonio", Some("password"))

    // without the proper credentials
    inside(etcdcli.deleteUser(newUser).futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with root access
    withAuthorization(rootAccess) { etcdClientRoot =>
      inside(etcdClientRoot.deleteUser(newUser).futureValue) {
        case EtcdConfirmationResponse(clusterID) =>
          clusterID.id shouldBe a[String]
      }
    }
  }

  it should "be able to delete a role" in { implicit etcdcli =>
    val rootAccess = EtcdUserAuthenticationData("root", "password")
    val newRole: EtcdRole = EtcdRole("usersAdmin", Some(EtcdPermission(EtcdKV(List("/users/*"), List("/users/*")))))

    // without the proper credentials
    inside(etcdcli.deleteRole(newRole).futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with root access
    withAuthorization(rootAccess) { etcdClientRoot =>
      inside(etcdClientRoot.deleteRole(newRole).futureValue) {
        case EtcdConfirmationResponse(clusterID) =>
          clusterID.id shouldBe a[String]
      }
    }
  }

  it should "be able to disable authentication" in { implicit etcdcli =>
    val rootAccess = EtcdUserAuthenticationData("root", "password")
    // without the proper credentials
    inside(etcdcli.disableAuthentication().futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        message.message should be("Insufficient credentials")
    }

    //with root access
    withAuthorization(rootAccess) { etcdClientRoot =>
      inside(etcdClientRoot.disableAuthentication().futureValue) {
        case EtcdConfirmationResponse(clusterID) =>
          clusterID.id shouldBe a[String]
      }
    }
  }

}
