/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class RecursiveDeleteEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/list"

  it should "delete a non-empty directory in etcd" in { etcdcli =>
    //given a etcdclient
    //and a directory that exists
    val dir: EtcdDirectory = genUniqueRandomDirectory.sample.get
    etcdcli.createDir(dir).futureValue

    val key1: String = genUniqueRandomString.sample.get
    val key2: String = genUniqueRandomString.sample.get
    val value1: EtcdValue = genRandomValue.sample.get
    val value2: EtcdValue = genRandomValue.sample.get

    //and the directory is not empty
    val newKey1 = EtcdModel.key(dir.dir + key1)
    val newKey2 = EtcdModel.key(dir.dir + key2)

    val deleteDirF = for {
      put1 <- etcdcli.setKey(newKey1, value1)
      put2 <- etcdcli.setKey(newKey2, value2)
      deleteDir <- etcdcli.deleteDir(dir, true)
    } yield deleteDir
    //when the directory is deleted
    val deleteResult = deleteDirF.futureValue
    deleteResult.accepted should be(true)
    inside(deleteResult) {
      case EtcdCreateDirResponse(headers, body) =>
        //the operation success
        body.action should be("delete")
        body.node.dir should be(true)
    }

    //but the keys are deleted
    val getAfterDelete = etcdcli.getKey(newKey1).futureValue
    getAfterDelete.accepted should be(false)
    inside(getAfterDelete) {
      case EtcdRequestError(statusCode, headers, error) =>
        error.message should be("Key not found")
    }
  }

}

