/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest
import org.scalacheck.Gen

class RecursiveWaitEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/recursivewait"

  it should "wait on a GET query and watch for changes in subnodes" in { etcdcli =>
    //given three different values
    val value1: EtcdValue = genUniqueRandomValue.sample.get
    val value2: EtcdValue = genUniqueRandomValue.sample.get
    val value3: EtcdValue = genUniqueRandomValue.sample.get
    //and a existing directory
    val dir: EtcdDirectory = genUniqueRandomDirectoryWithMaxDeep(3).sample.get
    etcdcli.createDir(dir).futureValue
    //and the key of the directory name
    val dirKey = EtcdModel.keyOfDirectory(dir)
    //and a key on the directory
    val keyInDir: EtcdKey = EtcdModel.key(dir.dir + Gen.identifier.sample.get)
    //when set and wait operations are realized in order
    etcdcli.setKey(keyInDir, value1).futureValue
    val waitOneCallback = new FutureBasedEtcdWaitCallback()
    etcdcli.waitForKey(dirKey, waitOneCallback, None, true)
    etcdcli.setKey(keyInDir, value2).futureValue
    etcdcli.setKey(keyInDir, value3).futureValue

    //then the wait detects the nested change on value 2 update
    eventually {
      waitOneCallback.isCompleted should be(true)
      val waitResponse: EtcdGetKeyResponse = waitOneCallback.future.futureValue
      waitResponse.body.node.value should be(value2)
    }

  }

}

