/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.utils.AbstractMultipleContainerEtcdClientTest
import org.scalatest.Ignore

import scala.concurrent.Future

@Ignore
class DocumentationExamples extends AbstractMultipleContainerEtcdClientTest {

  override def etcdClientPortShift: Int = 6

  override def clusterSize: Int = 3

  override def testScopePrefix: String = "/docsExamples"

  it should "provide examples that compile for the documentation" in { etcdcli =>

    // example for setKey
    val key = EtcdModel.key("/foo")
    val value = EtcdModel.value("bar")

    val keySet: Future[EtcdSetKeyResult] = etcdcli.setKey(key, value)

    keySet onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be "set"
        body.action
        // should be value
        body.node.value
    }

    inside(keySet.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be "set"
        body.action should be("set")
        // should be value
        body.node.value should be(value)
    }

    // example for setKey with TTL
    val ttl = EtcdTTL(1200)
    val setTTLResult: Future[EtcdSetKeyResult] = for {
      set <- keySet
      ttl <- etcdcli.setKey(key, value, Some(ttl))
    } yield ttl

    setTTLResult onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be Some(ttl)
        body.node.ttl
    }

    inside(setTTLResult.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be Some(ttl)
        body.node.ttl should be(Some(ttl))
    }

    // Example for unsetting ttl
    val unsetTTLResult: Future[EtcdSetKeyResult] = for {
      ttl <- setTTLResult
      unsetTTL <- etcdcli.setKey(key, value, None, Some(KeyMustExist(true)))
    } yield unsetTTL

    unsetTTLResult onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be None
        body.node.ttl
    }

    inside(unsetTTLResult.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be None
        body.node.ttl should be(None)
    }

    // examples for compare and swap
    val randomKey =
      EtcdModel.key("/docsExamples/k1/smxx84i5l6geviqwgkfjil8qR1anm/xgsisaevcObfu6j/bsjcmr")
    val randomValue = EtcdModel.value("0000000001")
    val newValue = EtcdModel.value("0000000002")
    val thirdValue = EtcdModel.value("0000000003")
    val fourthValue = EtcdModel.value("0000000004")

    // given a non existing key with condition KeyMustExist set to false
    val newKeySet: Future[EtcdSetKeyResult] =
      etcdcli.setKey(randomKey, randomValue, None, Some(KeyMustExist(false)))

    newKeySet onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be randomValue
        body.node.value
    }

    inside(newKeySet.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be randomValue
        body.node.value should be(randomValue)
    }

    val keyUpdated: Future[EtcdSetKeyResult] = for {
      newSet <- newKeySet
      // given an existing key with condition KeyMustExist set to true
      keySet <- etcdcli.setKey(randomKey, newValue, None, Some(KeyMustExist(true)))
    } yield keySet

    keyUpdated onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be newValue
        val storedValue = body.node.value
    }

    inside(keyUpdated.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be newValue
        val storedValue = body.node.value should be(newValue)
    }

    val conditionalSetModIndex: Future[EtcdSetKeyResult] = for {
      EtcdSetKeyResponse(headers, body) <- keyUpdated
      // given an existing key with condition MustHaveIndex()
      // and the modified index of the previous put request
      keySet <- etcdcli.setKey(randomKey, thirdValue, None, Some(KeyMustHaveIndex(body.node.modifiedIndex)))
    } yield keySet

    conditionalSetModIndex onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be thirdValue
        body.node.value
    }

    inside(conditionalSetModIndex.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be thirdValue
        body.node.value should be(thirdValue)
    }

    val mustHaveValSetKey: Future[EtcdSetKeyResult] = for {
      indexSet <- conditionalSetModIndex
      // given an existing key with condition MustHaveValue() and the current value of the key
      mustHave <- etcdcli.setKey(randomKey, fourthValue, None, Some(KeyMustHaveValue(thirdValue)))
    } yield mustHave

    mustHaveValSetKey onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be fourthValue
        body.node.value
    }

    inside(mustHaveValSetKey.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be fourthValue
        body.node.value should be(fourthValue)
    }

    val conditionalSetFailure: Future[EtcdSetKeyResult] = for {
      mustHaveSet <- mustHaveValSetKey
      // given a request with an already existing key and condition KeyMustExist set to false
      failure <- etcdcli.setKey(randomKey, randomValue, None, Some(KeyMustExist(false)))
    } yield failure

    conditionalSetFailure onSuccess {
      case EtcdRequestError(statusCode, headers, body) =>
        // should be EtcdResponseCode(412)
        statusCode
        // should be
        // should be "Key already exists"
        body.message
    }

    inside(conditionalSetFailure.futureValue) {
      case EtcdRequestError(statusCode, headers, body) =>
        // should be EtcdResponseCode(412)
        statusCode should be(EtcdResponseCode(412))
        // should be "Key already exists"
        body.message should be("Key already exists")
    }

    // wait examples
    val waitCallback = new FutureBasedEtcdWaitCallback()
    val waitCall: Future[EtcdWaitForKeyResult] = for {
      unset <- unsetTTLResult
      wait <- etcdcli.waitForKey(key, waitCallback)
    } yield wait

    waitCall onSuccess {
      case EtcdWaitForKeyAccepted(headers: EtcdHeaders) =>
        // should be a String
        headers.xEtcdClusterId.id
        // should be true
        EtcdWaitForKeyAccepted(headers: EtcdHeaders).accepted
        // should be false
        waitCallback.isCompleted
    }

    inside(waitCall.futureValue) {
      case EtcdWaitForKeyAccepted(headers: EtcdHeaders) =>
        // should be a String
        headers.xEtcdClusterId.id
        // should be true
        EtcdWaitForKeyAccepted(headers: EtcdHeaders).accepted should be(true)
        // should be false
        waitCallback.isCompleted should be(false)
    }

    val updatedValue = EtcdModel.value("phoo")

    val callbackAccepted: Future[EtcdGetKeyBody] = for {
      // an update to key, so as to provoke a call to the wait
      update <- etcdcli.setKey(key, updatedValue)
      EtcdGetKeyResponse(headers: EtcdHeaders, body: EtcdGetKeyBody) <- waitCallback.future
    } yield body

    callbackAccepted onSuccess {
      case EtcdGetKeyBody(action, node) =>
        // should be true
        waitCallback.isCompleted
    }

    inside(callbackAccepted.futureValue) {
      case EtcdGetKeyBody(action, node) =>
        waitCallback.isCompleted should be(true)
    }

    // wait with modified index
    val waitCallback2 = new FutureBasedEtcdWaitCallback()
    // when a wait call with a second callback and the modified index
    // of a previous operation plus 1 is executed
    val waitCall2: Future[EtcdWaitForKeyResult] = for {
      EtcdSetKeyResponse(headers, body) <- keyUpdated
      mustHaveCondition <- mustHaveValSetKey
      accepted <- etcdcli.
        waitForKey(randomKey, waitCallback2, Some(EtcdIndex(body.node.modifiedIndex.index + 1)))
    } yield accepted

    // then the wait call must be accepted
    waitCall2 onSuccess {
      case EtcdWaitForKeyAccepted(headers) =>
        // should be true,
        // since a change in the value of the key has already been registered
        EtcdWaitForKeyAccepted(headers).accepted
    }

    inside(waitCall2.futureValue) {
      case EtcdWaitForKeyAccepted(headers) =>
        // should be true
        EtcdWaitForKeyAccepted(headers).accepted should be(true)
    }

    waitCallback2.future onSuccess {
      case EtcdGetKeyResponse(headers, body) =>
        // should be thirdValue
        // since this was the update made to this key
        // immediately after the keyUpdated
        body.node.value
    }

    inside(waitCallback2.future.futureValue) {
      case EtcdGetKeyResponse(headers, body) =>
        // should be value2
        body.node.value should be(thirdValue)
    }

  }

  it should "provide a second round of examples that compile for the documentation" in { etcdcli =>

    //recursive wait examples
    val value1: EtcdValue = EtcdModel.value("firstValue")
    val value2: EtcdValue = EtcdModel.value("secondValue")
    val value3: EtcdValue = EtcdModel.value("thirdValue")

    val listOfNodes: List[String] = List("path", "leading", "to", "node")
    // and a key of a node being used as a directory
    val dirKey: EtcdKey = EtcdModel.fromPath(listOfNodes)
    // returns EtcdKey(/path/leading/to/node/subnode)
    val keyInDir: EtcdKey = EtcdModel.fromPath(listOfNodes ::: List("subnode"))
    // and a callback
    val waitOneCallback = new FutureBasedEtcdWaitCallback()
    // when set and wait operations are performed in order
    val callReturned = for {
      set <- etcdcli.setKey(keyInDir, value1)
      setWatch <- etcdcli.waitForKey(dirKey, waitOneCallback, None, true)
      modifyKey1 <- etcdcli.setKey(keyInDir, value2)
      modifyKey2 <- etcdcli.setKey(keyInDir, value3)
      returned <- waitOneCallback.future
    } yield returned

    // then the wait detects the nested change on value2 update
    callReturned onSuccess {
      case EtcdGetKeyResponse(headers, body) =>
        // should be true
        waitOneCallback.isCompleted
        //should be value2
        body.node.value
    }

    inside(callReturned.futureValue) {
      case EtcdGetKeyResponse(headers, body) =>
        // should be true
        waitOneCallback.isCompleted should be(true)
        //should be value2
        body.node.value should be(value2)
    }

    //get examples
    val key = EtcdModel.key("/foo")
    val value = EtcdModel.value("bar")

    // setting a key and then a get request
    val result = for {
      keySet <- etcdcli.setKey(key, value)
      getRequest <- etcdcli.getKey(key)
    } yield getRequest

    // Get ok
    result onSuccess {
      case EtcdGetKeyResponse(headers, body) =>
        // should be value
        body.node.value
    }

    inside(result.futureValue) {
      case EtcdGetKeyResponse(headers, body) =>
        // should be value
        body.node.value should be(value)
    }

    // Delete example
    val deleteResult = for {
      result <- result
      delete <- etcdcli.deleteKey(key)
    } yield delete

    deleteResult onSuccess {
      case EtcdDeleteKeyResponse(headers, body) =>
        // should be "delete"
        body.action
    }

    inside(deleteResult.futureValue) {
      case EtcdDeleteKeyResponse(headers, body) =>
        // should be "delete"
        body.action should be("delete")
    }

    // Get 404 not found
    val failedGet: Future[Int] = deleteResult flatMap { delete =>
      etcdcli.getKey(key).map {
        case EtcdRequestError(statusCode, headers, error) =>
          //should be 404
          statusCode.code
      }
    }

    failedGet.futureValue should be(404)

    // conditionally delete examples
    val someKey = EtcdModel.key("/foo")
    val someValue = EtcdModel.value("bar")

    //given an existing key
    val deletedF = for {
      created <- etcdcli.setKey(someKey, someValue)
      deleted <- etcdcli.deleteKey(key, Some(KeyMustHaveValue(someValue)))
    } yield deleted

    deletedF onSuccess {
      case EtcdDeleteKeyResponse(headers, body) =>
        // should be "compareAndDelete"
        body.action
    }

    inside(deletedF.futureValue) {
      case EtcdDeleteKeyResponse(headers, body) =>
        // should be "compareAndDelete"
        body.action
    }

    val mustHaveIdxDelete: Future[EtcdDeleteKeyResult] = for {
      deleted <- deletedF
      // key set again
      EtcdSetKeyResponse(headers, body) <- etcdcli.setKey(someKey, someValue)
      deletedAgain <- etcdcli.deleteKey(someKey, Some(KeyMustHaveIndex(body.node.modifiedIndex)))
    } yield deletedAgain

    mustHaveIdxDelete onSuccess {
      case EtcdDeleteKeyResponse(headers, body) =>
        // should be "compareAndDelete"
        body.action
    }

    inside(mustHaveIdxDelete.futureValue) {
      case EtcdDeleteKeyResponse(headers, body) =>
        // should be "compareAndDelete"
        body.action should be("compareAndDelete")
    }
  }

  it should "provide a third round of examples that compile for the documentation" in { etcdcli =>

    // createDir examples
    val dir = EtcdModel.directory("/path/leading/to/some/node/")

    val dirCreated: Future[EtcdCreateDirResult] = etcdcli.createDir(dir)

    dirCreated onSuccess {
      case EtcdCreateDirResponse(headers, body) =>
        // should be "set"
        body.action
        // should be true
        body.node.dir
    }

    inside(dirCreated.futureValue) {
      case EtcdCreateDirResponse(headers, body) =>
        // should be "set"
        body.action should be("set")
        // should be true
        body.node.dir should be(true)
    }

    val dirTTL = EtcdModel.directory("/path/leading/to/other/node/")
    // and a time to leave
    val ttl = EtcdTTL(2400)

    val dirTTLCreated: Future[EtcdCreateDirResult] = etcdcli.createDir(dirTTL, Some(ttl))

    dirTTLCreated onSuccess {
      case EtcdCreateDirResponse(headers, body) =>
        // should be "set"
        body.action
        //  should be true
        body.node.dir
        // should be <= ttl.ttl
        body.node.ttl.get.ttl
    }

    inside(dirTTLCreated.futureValue) {
      case EtcdCreateDirResponse(headers, body) =>
        // should be "set"
        body.action should be("set")
        // should be true
        body.node.dir should be(true)
        // should be <= ttl.ttl
        body.node.ttl.get.ttl should be <= ttl.ttl
    }

    // refreshDirTTL examples
    // and a different time to live
    val newTTL = EtcdTTL(6000)

    val ttlRefreshed: Future[EtcdCreateDirResult] = for {
      dirTTLC <- dirTTLCreated
      refreshed <- etcdcli.refreshDirTTL(dirTTL, Some(newTTL))
    } yield refreshed

    ttlRefreshed onSuccess {
      case EtcdCreateDirResponse(headers, body) =>
        // should be "update"
        body.action
        // should be true
        body.node.dir
        // should should be <= newTTL.ttl
        body.node.ttl.get.ttl
    }

    inside(ttlRefreshed.futureValue) {
      case EtcdCreateDirResponse(headers, body) =>
        // should be "update"
        body.action should be("update")
        // should be true
        body.node.dir should be(true)
        // should should be <= newTTL.ttl
        body.node.ttl.get.ttl should be <= newTTL.ttl
    }

    val ttlUnset: Future[EtcdCreateDirResult] = for {
      refreshed <- ttlRefreshed
      unset <- etcdcli.refreshDirTTL(dirTTL, None)
    } yield unset

    ttlUnset onSuccess {
      case EtcdCreateDirResponse(headers, body) =>
        // should be None
        body.node.ttl
    }

    inside(ttlUnset.futureValue) {
      case EtcdCreateDirResponse(headers, body) =>
        // should be None
        body.node.ttl should be(None)
    }

    // deleteDir example
    val dirDeleted: Future[EtcdCreateDirResult] = for {
      created <- dirCreated
      deleted <- etcdcli.deleteDir(dir)
    } yield deleted

    dirDeleted onSuccess {
      case EtcdCreateDirResponse(headers, body) =>
        // should be "delete"
        body.action
    }

    inside(dirDeleted.futureValue) {
      case EtcdCreateDirResponse(headers, body) =>
        // should be "delete"
        body.action should be("delete")
    }
  }

  it should "provide a fourth round of examples that compile for the documentation" in { etcdcli =>
    // refreshTTL example
    val key = EtcdModel.key("/Hello!")
    val value = EtcdModel.value("¡Hola!")
    val ttl = EtcdTTL(1200)
    val newTtl = EtcdTTL(2400)
    val callback = new FutureBasedEtcdWaitCallback()

    val refreshTTLRequest = for {
      keySet <- etcdcli.setKey(key, value, Some(ttl))
      watchReady <- etcdcli.waitForKey(key, callback)
      ttlRefreshed <- etcdcli.refreshTTL(key, newTtl)
    } yield ttlRefreshed

    refreshTTLRequest onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be "set"
        body.action
        // should be Some(newTtl)
        body.node.ttl
        // should be false
        // because the watch was never notified of an update operation
        callback.isCompleted
    }

    inside(refreshTTLRequest.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be "set"
        body.action
        // should be Some(newTtl)
        body.node.ttl
        // should be false
        callback.isCompleted should be(false)
    }

    // createKeyFromCurrentEtcdIndex examples
    val value1: EtcdValue = EtcdModel.value("firstValue")
    val value2: EtcdValue = EtcdModel.value("secondValue")
    // and a existing directory
    val dir: EtcdDirectory = EtcdModel.directory("/path/to/node/")

    // createDir and createKeyFromCurrentEtcdIndex operations performed successively
    val createUniqueKeyReq = for {
      newDirReq <- etcdcli.createDir(dir)
      newKeyFromIndex <- etcdcli.createKeyFromCurrentEtcdIndex(dir, value1)
      secondKeyFromIndex <- etcdcli.createKeyFromCurrentEtcdIndex(dir, value2)
    } yield List(newKeyFromIndex, secondKeyFromIndex)

    createUniqueKeyReq onSuccess {
      case list =>
        val firstResponseBody = list.head match {
          case EtcdSetKeyResponse(headers, body) =>
            body
        }
        val secondResponseBody = list.tail.head match {
          case EtcdSetKeyResponse(headers, body) =>
            body
        }
        // sample key: EtcdKey(/path/to/node/00000000000000000118)
        val key1 = firstResponseBody.node.key
        // sample index: EtcdIndex(118)
        val modIdx1 = firstResponseBody.node.modifiedIndex
        //The created key name is 000000${modIdx1.index.toString}, thus
        // this example's integer value is 118
        val lastNodeName1 = EtcdModel.toPath(key1).takeRight(1).head.toInt
        //  sample key: EtcdKey(/path/to/node/00000000000000000119)
        val key2 = secondResponseBody.node.key
        // sample index: EtcdIndex(119)
        val modIdx2 = secondResponseBody.node.modifiedIndex
        //The created key name is 000000${modIdx1.index.toString}, thus
        // this example's integer value is 119
        val lastNodeName2 = EtcdModel.toPath(key2).takeRight(1).head.toInt
        // should be true
        modIdx1.index < modIdx2.index
        // should be true, i.e. the names of the last nodes are ordered
        lastNodeName1 < lastNodeName2
    }

    val list = createUniqueKeyReq.futureValue
    val firstResponseBody = list.head match {
      case EtcdSetKeyResponse(headers, body) =>
        body
    }
    val secondResponseBody = list.tail.head match {
      case EtcdSetKeyResponse(headers, body) =>
        body
    }
    val key1 = firstResponseBody.node.key
    val modIdx1 = firstResponseBody.node.modifiedIndex
    val lastNodeName1 = EtcdModel.toPath(key1).takeRight(1).head.toInt
    val key2 = secondResponseBody.node.key
    val modIdx2 = secondResponseBody.node.modifiedIndex
    val lastNodeName2 = EtcdModel.toPath(key2).takeRight(1).head.toInt

    modIdx1.index should be < modIdx2.index
    lastNodeName1 should be < lastNodeName2
  }

  it should "provide a fifth round of examples that compile for the statistics documentation" in { etcdcli =>

    etcdcli.getVersion onSuccess {
      case EtcdGetVersionResponse(etcdserver, etcdcluster) =>
        // should be a string starting with "2.3"
        etcdcluster
    }

    etcdcli.getHealth onSuccess {
      case EtcdGetHealthResponse(health) =>
        // should be "true"
        health
    }

    etcdcli.getSelfStats onSuccess {
      case EtcdGetSelfStatsResponse(id: String,
        leaderInfo: EtcdLeaderInfo,
        name: String,
        recvAppendRequestCnt: Int,
        recvBandwidthRate: Option[Double],
        recvPkgRate: Option[Double],
        sendAppendRequestCnt: Int,
        startTime: String,
        state: String) =>
        // sample id string: 77fb14b13d7590f7
        id
    }

    etcdcli.getStoreStats onSuccess {
      case EtcdGetStoreStatsResponse(compareAndSwapFail: Int,
        compareAndSwapSuccess: Int,
        createFail: Int,
        createSuccess: Int,
        deleteFail: Int,
        deleteSuccess: Int,
        expireCount: Int,
        getsFail: Int,
        getsSuccess: Int,
        setsFail: Int,
        setsSuccess: Int,
        updateFail: Int,
        updateSuccess: Int,
        watchers: Int) =>
        // should de an integer.
        watchers
    }
  }

  it should "provide a sixth round of examples that compile for the authentication documentation" in { implicit etcdcli =>

    val statusQuery: Future[EtcdGetAuthenticationStatusResult] = etcdcli.getAuthenticationStatus

    statusQuery onSuccess {
      case EtcdGetAuthenticationStatusResponse(clusterId, body) =>
        // should be Some(false) if authentication is not enabled
        // Some(true) otherwise.
        body.enabled
    }

    inside(statusQuery.futureValue) {
      case EtcdGetAuthenticationStatusResponse(clusterId, body) =>
        // should be Some(false) if authentication is not enabled
        // Some(true) otherwise.
        body.enabled should be(Some(false))
    }

    val enableFail: Future[EtcdConfirmationResult] = for {
      status <- statusQuery
      enable <- etcdcli.enableAuthentication()
    } yield enable

    enableFail onSuccess {
      case error @ EtcdStandardError(status, clusterId, message) =>
        // when there is no root user
        // should be "auth: No root user available, please create one"
        message.message
    }

    inside(enableFail.futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        // when there is no root user
        // should be "auth: No root user available, please create one"
        message.message should be("auth: No root user available, please create one")
    }

    val rootUser: EtcdUserRequestForm = EtcdUserRequestForm("root", Some("password"))

    val rootUserCreated: Future[EtcdAddUpdateUserResult] = for {
      enable <- enableFail
      added <- etcdcli.addUpdateUser(rootUser)
    } yield added

    rootUserCreated onSuccess {
      case EtcdAddUpdateUserResponse(clusterId, body) =>
        // should be rootUser.user
        body.user
        // should be List("root")
        body.roles
    }

    inside(rootUserCreated.futureValue) {
      case EtcdAddUpdateUserResponse(clusterId, body) =>
        // should be rootUser.user
        body.user should be(rootUser.user)
        // should be List("root")
        body.roles should be(List("root"))
    }

    val newUser: EtcdUserRequestForm = EtcdUserRequestForm("antonio", Some("password"))

    val newUserCreated: Future[EtcdAddUpdateUserResult] = for {
      rootAdded <- rootUserCreated
      added <- etcdcli.addUpdateUser(newUser)
    } yield added

    newUserCreated onSuccess {
      case EtcdAddUpdateUserResponse(clusterId, body) =>
        // should be newUser.user
        body.user
        // should be List()
        body.roles
    }

    inside(newUserCreated.futureValue) {
      case EtcdAddUpdateUserResponse(clusterId, body) =>
        // should be newUser.user
        body.user should be(newUser.user)
        // should be List()
        body.roles should be(List())
    }

    val newRole: EtcdRole =
      EtcdRole("usersAdmin", Some(EtcdPermission(EtcdKV(List("/users/*"), List("/users/*")))))

    val newRoleCreated: Future[EtcdAddUpdateRoleResult] = for {
      newUserAdded <- newUserCreated
      added <- etcdcli.addUpdateRole(newRole)
    } yield added

    newRoleCreated onSuccess {
      case EtcdAddUpdateRoleResponse(clusterId, body) =>
        //  should be newRole.role
        body.role
        // should be List("/users/*")
        body.permissions.get.kv.read
        // should be List("/users/*")
        body.permissions.get.kv.write
    }

    inside(newRoleCreated.futureValue) {
      case EtcdAddUpdateRoleResponse(clusterId, body) =>
        //  should be newRole.role
        body.role should be(newRole.role)
        // should be List("/users/*")
        body.permissions.get.kv.read should be(List("/users/*"))
        // should be List("/users/*")
        body.permissions.get.kv.write should be(List("/users/*"))
    }

    val authEnabled: Future[EtcdConfirmationResult] = for {
      added <- newRoleCreated
      enabled <- etcdcli.enableAuthentication()
    } yield enabled

    authEnabled onSuccess {
      // a response without a body which only contains an id found in the headers
      case EtcdConfirmationResponse(clusterID) =>
        // should be a string with an id of the member of the cluster
        clusterID.id
    }

    inside(authEnabled.futureValue) {
      // a response without a body which only contains an id found in the headers
      case EtcdConfirmationResponse(clusterID) =>
        // should be a string with an id of the member of the cluster
        clusterID.id shouldBe a[String]
    }

    val getUserFailed: Future[EtcdGetUserDetailsResult] = for {
      enabled <- authEnabled
      // without authorization
      // rootUser was defined in an example above
      details <- etcdcli.getUserDetails(rootUser)
    } yield details

    getUserFailed onSuccess {
      case error @ EtcdStandardError(status, clusterId, message) =>
        // should be "Insufficient credentials"
        message.message
    }

    inside(getUserFailed.futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        // should be "Insufficient credentials"
        message.message should be("Insufficient credentials")
    }

    val rootAccess = EtcdUserAuthenticationData("root", "password")

    val etcdClientRoot: EtcdClient = etcdcli.withAuthorization(rootAccess)

    val getUserSuccess: Future[EtcdGetUserDetailsResult] = for {
      failed <- getUserFailed
      //with authorization
      success <- etcdClientRoot.getUserDetails(rootUser)
    } yield success

    getUserSuccess onSuccess {
      case EtcdGetUserDetailsResponse(clusterId, body) =>
        // should be rootUser.user
        body.user
        // should be List(EtcdRole("root", Some(EtcdPermission(EtcdKV(List("/*"), List("/*")))), None, None))
        body.roles
    }

    inside(getUserSuccess.futureValue) {
      case EtcdGetUserDetailsResponse(clusterId, body) =>
        // should be rootUser.user
        body.user should be(rootUser.user)
        // should be List(EtcdRole("root", Some(EtcdPermission(EtcdKV(List("/*"), List("/*")))), None, None))
        body.roles should be(List(EtcdRole("root", Some(EtcdPermission(EtcdKV(List("/*"), List("/*")))), None, None)))
    }

    val getRole: Future[EtcdGetRoleDetailsResult] = for {
      user <- getUserSuccess
      // newRole was defined in an example above
      //with authorization
      role <- etcdClientRoot.getRoleDetails(newRole)
    } yield role

    getRole onSuccess {
      case EtcdGetRoleDetailsResponse(clusterId, body) =>
        // should be newRole.role
        body.role
        // should be "/users/*"
        body.permissions.get.kv.read.head
    }

    inside(getRole.futureValue) {
      case EtcdGetRoleDetailsResponse(clusterId, body) =>
        // should be newRole.role
        body.role should be(newRole.role)
        // should be "/users/*"
        body.permissions.get.kv.read.head should be("/users/*")
    }

    val listOfUsers: Future[EtcdGetUsersResult] = for {
      role <- getRole
      list <- etcdClientRoot.getUsers()
    } yield list

    listOfUsers onSuccess {
      case EtcdGetUsersResponse(clusterId, body) =>
        // should be "antonio"
        body.users.head.user
        // should be "root"
        body.users.tail.head.user
        // should be "root"
        body.users.tail.head.roles.head.role
        // should be "/*"
        body.users.tail.head.roles.head.permissions.get.kv.read.head
    }

    inside(listOfUsers.futureValue) {
      case EtcdGetUsersResponse(clusterId, body) =>
        // should be "antonio"
        body.users.head.user should be("antonio")
        // should be "root"
        body.users.tail.head.user should be("root")
        // should be "root"
        body.users.tail.head.roles.head.role should be("root")
        // should be "/*"
        body.users.tail.head.roles.head.permissions.get.kv.read.head should be("/*")
    }

    val listOfRoles: Future[EtcdGetRolesResult] = for {
      users <- listOfUsers
      //with authorization
      list <- etcdClientRoot.getRoles()
    } yield list

    listOfRoles onSuccess {
      case EtcdGetRolesResponse(clusterId, body) =>
        // should be "guest"
        body.roles.head.role
        //  should be "root"
        body.roles.tail.head.role
        // should be "/*"
        body.roles.tail.head.permissions.get.kv.read.head
    }

    inside(listOfRoles.futureValue) {
      case EtcdGetRolesResponse(clusterId, body) =>
        // should be "guest"
        body.roles.head.role should be("guest")
        //  should be "root"
        body.roles.tail.head.role should be("root")
        // should be "/*"
        body.roles.tail.head.permissions.get.kv.read.head should be("/*")
    }

    // a new role is granted placing in the granted field
    // a list of strings with the name of the roles added
    val newUserUpdate: EtcdUserRequestForm =
      EtcdUserRequestForm("antonio", None, List(), List("usersAdmin"))

    val newUserUpdated: Future[EtcdAddUpdateUserResult] = for {
      list <- listOfRoles
      updated <- etcdClientRoot.addUpdateUser(newUserUpdate)
    } yield updated

    newUserUpdated onSuccess {
      case EtcdAddUpdateUserResponse(clusterId, body) =>
        // should be newUser.user
        body.user
        // should be "usersAdmin"
        body.roles.head
    }

    inside(newUserUpdated.futureValue) {
      case EtcdAddUpdateUserResponse(clusterId, body) =>
        // should be newUser.user
        body.user should be(newUser.user)
        // should be "usersAdmin"
        body.roles.head should be("usersAdmin")
    }

    // role with a permissions to read and write in the key space
    // placed in the revoke field, i.e. a request to revoke read
    // and write permissions to the guest role
    val guestRole: EtcdRole =
      EtcdRole("guest", None, None, Some(EtcdPermission(EtcdKV(List("/*"), List("/*")))))

    val guestRoleUpdated: Future[EtcdAddUpdateRoleResult] = for {
      newUserUpdated <- newUserUpdated
      updated <- etcdClientRoot.addUpdateRole(guestRole)
    } yield updated

    guestRoleUpdated onSuccess {
      case EtcdAddUpdateRoleResponse(clusterId, body) =>
        // should be guestRole.role
        body.role
        // should be List()
        body.permissions.get.kv.read
        // should be List()
        body.permissions.get.kv.write
    }

    inside(guestRoleUpdated.futureValue) {
      case EtcdAddUpdateRoleResponse(clusterId, body) =>
        // should be guestRole.role
        body.role should be(guestRole.role)
        // should be(List())
        body.permissions.get.kv.read should be(List())
        // should be(List())
        body.permissions.get.kv.write should be(List())
    }

    val newKey = EtcdModel.key("/foo")
    val newValue = EtcdModel.value("bar")

    val setFailure: Future[EtcdSetKeyResult] = for {
      updated <- guestRoleUpdated
      // without the proper credentials
      failure <- etcdcli.setKey(newKey, newValue)
    } yield failure

    setFailure onSuccess {
      case error @ EtcdRequestError(status, headers, body) =>
        body match {
          case EtcdError(cause, errorCode, index, message) =>
            // should be 110
            errorCode
            // should be "The request requires user authentication"
            message
        }
    }

    inside(setFailure.futureValue) {
      case error @ EtcdRequestError(status, headers, body) =>
        body match {
          case EtcdError(cause, errorCode, index, message) =>
            // should be 110
            errorCode should be(110)
            // should be "The request requires user authentication"
            message should be("The request requires user authentication")
        }
    }

    val setSuccess: Future[EtcdSetKeyResult] = for {
      failure <- setFailure
      //with root access
      success <- etcdClientRoot.setKey(newKey, newValue, None, None)
    } yield success

    setSuccess onSuccess {
      case EtcdSetKeyResponse(headers, body) =>
        // should be "set"
        body.action
        // should be newValue
        body.node.value
    }

    inside(setSuccess.futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        // should be "set"
        body.action should be("set")
        // should be newValue
        body.node.value should be(newValue)
    }

    val newUserDeleted: Future[EtcdConfirmationResult] = for {
      success <- setSuccess
      // with root access
      deleted <- etcdClientRoot.deleteUser(newUser)
    } yield deleted

    newUserDeleted onSuccess {
      case EtcdConfirmationResponse(clusterID) =>
        clusterID.id
    }

    inside(newUserDeleted.futureValue) {
      case EtcdConfirmationResponse(clusterID) =>
        clusterID.id shouldBe a[String]
    }

    val newRoleDeleted: Future[EtcdConfirmationResult] = for {
      userDeleted <- newUserDeleted
      // with root access
      deleted <- etcdClientRoot.deleteRole(newRole)
    } yield deleted

    newRoleDeleted onSuccess {
      case EtcdConfirmationResponse(clusterID) =>
        // should be a string
        clusterID.id
    }

    inside(newRoleDeleted.futureValue) {
      case EtcdConfirmationResponse(clusterID) =>
        // should be a string
        clusterID.id shouldBe a[String]
    }

    val disableAuthFailure: Future[EtcdConfirmationResult] = for {
      deleted <- newRoleDeleted
      // without the proper credentials
      failure <- etcdcli.disableAuthentication()
    } yield failure

    disableAuthFailure onSuccess {
      case error @ EtcdStandardError(status, clusterId, message) =>
        // should be "Insufficient credentials"
        message.message
    }

    inside(disableAuthFailure.futureValue) {
      case error @ EtcdStandardError(status, clusterId, message) =>
        // should be "Insufficient credentials"
        message.message should be("Insufficient credentials")
    }

    val disableAuthSuccess: Future[EtcdConfirmationResult] = for {
      failure <- disableAuthFailure
      //with root access
      success <- etcdClientRoot.disableAuthentication()
    } yield success

    disableAuthSuccess onSuccess {
      case EtcdConfirmationResponse(clusterID) =>
        clusterID.id
        etcdClientRoot.close()
    }

    inside(disableAuthSuccess.futureValue) {
      case EtcdConfirmationResponse(clusterID) =>
        clusterID.id shouldBe a[String]
    }

  }

  it should "provide a seventh round of examples that compile for the members documentation" in { etcdcli =>

    val getMemberResult =
      etcdcli.getMembers onSuccess {
        case EtcdGetMembersResponse(headers, body) =>
          body.members.head.id
          // should have size 3 for a cluster with 3 members
          body.members
      }

    val newMemberForm: EtcdMemberForm = EtcdMemberForm(List("http://10.0.0.10:2380", "http://10.0.0.11:2380"))
    val member: Future[String] = for {
      EtcdAddMemberResponse(headers, body) <- etcdcli.addMember(newMemberForm)
    } yield body.id

    val getMembersReq: Future[EtcdGetMembersResult] = for {
      memberID <- member
      membersResp <- etcdcli.getMembers
    } yield membersResp

    val memberAdded: Future[Boolean] = member flatMap { member =>
      getMembersReq.map {
        case EtcdGetMembersResponse(clusterId, body) =>
          // should be true
          body.members.contains(EtcdMember(member, "", List("http://10.0.0.10:2380", "http://10.0.0.11:2380"), List()))
      }
    }

    memberAdded.futureValue should be(true)

    val changeMemberForm: EtcdMemberForm = EtcdMemberForm(List("http://10.0.0.12:4002"))
    val changePeerURLReq: Future[EtcdGetMembersResult] = for {
      member <- member
      confirmation <- memberAdded
      changeUrls <- etcdcli.changePeerURLs(member, changeMemberForm)
      membersResp <- etcdcli.getMembers
    } yield membersResp

    val memberPeerURLsChanged: Future[Boolean] = member flatMap { member =>
      changePeerURLReq.map {
        case EtcdGetMembersResponse(clusterId, body) =>
          // should be true
          body.members.contains(EtcdMember(member, "", List("http://10.0.0.12:4002"), List()))
      }
    }

    memberPeerURLsChanged.futureValue should be(true)

    val deleteMemberReq: Future[EtcdConfirmationResult] = for {
      member <- member
      deleteMember <- etcdcli.deleteMember(member)
    } yield deleteMember

    deleteMemberReq onSuccess {
      case EtcdConfirmationResponse(clusterID) =>
        //shouldBe a EtcdClusterId
        clusterID shouldBe a[EtcdClusterId]
    }

    inside(deleteMemberReq.futureValue) {
      case EtcdConfirmationResponse(clusterID) =>
        clusterID shouldBe a[EtcdClusterId]
    }

  }
}

