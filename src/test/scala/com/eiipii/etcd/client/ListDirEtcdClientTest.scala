/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class ListDirEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/list"

  it should "list a directory in etcd" in { etcdcli =>
    //given a etcdclient
    //and a directory that exists
    val dir: EtcdDirectory = genUniqueRandomDirectoryWithMaxDeep(2).sample.get
    etcdcli.createDir(dir).futureValue.accepted should be(true)
    //when the directory is listed
    val listDirRequest: EtcdListDirResult = etcdcli.listDir(dir).futureValue
    listDirRequest.accepted should be(true)
    inside(listDirRequest) {
      case EtcdListDirResponse(headers, body) =>
        body.action should be("get")
        body.node.dir should be(true)
        body.node.nodes shouldBe List()
    }

    val key1: String = genUniqueRandomString.sample.get
    val key2: String = genUniqueRandomString.sample.get
    val key3: EtcdKey = genUniqueRandomKeyWithMaxDeep(2).sample.get
    val value1: EtcdValue = genRandomValue.sample.get
    val value2: EtcdValue = genRandomValue.sample.get
    val value3: EtcdValue = genRandomValue.sample.get

    val newKey1 = EtcdModel.key(dir, key1)
    val newKey2 = EtcdModel.key(dir, key2)
    val newKey3 = EtcdModel.fromPath((EtcdModel.toPath(EtcdModel.keyOfDirectory(dir)) ::: EtcdModel.toPath(key3)))

    val listDirF = for {
      put1 <- etcdcli.setKey(newKey1, value1)
      put2 <- etcdcli.setKey(newKey2, value2)
      put3 <- etcdcli.setKey(newKey3, value3)
      listDir <- etcdcli.listDir(dir)
    } yield listDir

    inside(listDirF.futureValue) {
      case EtcdListDirResponse(headers, body) =>
        body.action should be("get")
        body.node.dir should be(true)
        body.node.nodes.map(_.key).toSet should contain allOf (newKey1, newKey2, EtcdModel.key(dir, "list"))
    }

    val listDirRecF = etcdcli.listDir(dir, true)

    def flatValues(dir: EtcdListDirNode): List[EtcdListValue] = {
      dir match {
        case EtcdListDirectory(dir, key, nodes, modifiedIndex, createdIndex) => nodes.flatMap(flatValues _)
        case v @ EtcdListValue(dir, key, value, modifiedIndex, createdIndex) => List(v)
      }
    }

    inside(listDirRecF.futureValue) {
      case EtcdListDirResponse(headers, body) =>
        body.action should be("get")
        flatValues(body.node).map(_.key).toSet should contain allOf (newKey1, newKey2, newKey3)
    }

  }

}
