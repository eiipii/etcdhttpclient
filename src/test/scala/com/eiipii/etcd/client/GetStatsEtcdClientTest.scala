/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.utils.AbstractMultipleContainerEtcdClientTest

class GetStatsEtcdClientTest extends AbstractMultipleContainerEtcdClientTest {

  override def etcdClientPortShift: Int = 9

  override def clusterSize: Int = 3

  override def testScopePrefix: String = "/selfStats"

  it should "get self stats for a node of an etcd cluster" in { etcdcli =>
    val selfStats = etcdcli.getSelfStats.futureValue
    selfStats shouldBe an[EtcdGetSelfStatsResponse]
    selfStats.accepted shouldBe (true)
  }

  it should "get store stats from a node of an etcd cluster" in { etcdcli =>
    val storeStats = etcdcli.getStoreStats.futureValue
    storeStats shouldBe an[EtcdGetStoreStatsResponse]
    storeStats.accepted shouldBe (true)
  }

  it should "get leader stats of an etcd cluster" in { etcdcli =>
    //Given
    val leaderData = findLeaderData(etcdcli)
    //And the leader client URL can be used to create a etcdclient connection
    val leaderClient = EtcdClient(EtcdDSL.config(leaderData.clientURLs.head))

    //When the leader statistics are extracted
    val leaderStats = leaderClient.getLeaderStats.futureValue

    //then
    leaderStats.accepted should be(true)
    inside(leaderStats) {
      case EtcdGetLeaderStatsResponse(leader, followers) =>
        leader shouldBe a[String]
        followers should have size clusterSize - 1
    }
    //and remember to close to avoid memory leaks
    leaderClient.close()
  }

  it should "failure to get leader stats from a node that is not the leader of the cluster" in { etcdcli =>
    //Given
    val NoleaderData = findNoLeaderData(etcdcli)
    //And the leader client URL can be used to create a etcdclient connection
    val memberClient = EtcdClient(EtcdDSL.config(NoleaderData.clientURLs.head))

    //When the leader statistics are extracted
    val leaderStats = memberClient.getLeaderStats.futureValue

    //then
    leaderStats.accepted should be(false)
    inside(leaderStats) {
      case EtcdStandardError(statusCode, xEtcdClusterId, error) =>
        statusCode should be(EtcdResponseCode(403))
    }
    //and remember to close to avoid memory leaks
    memberClient.close()
  }

  def findLeaderData(etcdcli: EtcdClient): EtcdMember = {
    val leaderId = findLeaderId(etcdcli)
    findMember(etcdcli, m => m.id == leaderId)
  }

  def findNoLeaderData(etcdcli: EtcdClient): EtcdMember = {
    val leaderId = findLeaderId(etcdcli)
    findMember(etcdcli, m => m.id != leaderId)
  }

  def findMember(etcdcli: EtcdClient, memberCriteria: EtcdMember => Boolean): EtcdMember = {
    val members = etcdcli.getMembers.futureValue
    members match {
      case EtcdGetMembersResponse(xEtcdClusterId, body) =>
        body.members.find(memberCriteria).value
      case EtcdStandardError(statusCode, xEtcdClusterId, error) =>
        fail("Error loading members data")
    }
  }

  def findLeaderId(etcdcli: EtcdClient): String = {
    //Given any node on the cluster
    val memberData = etcdcli.getSelfStats.futureValue
    memberData match {
      case EtcdGetSelfStatsResponse(id, leaderInfo, name, recvAppendRequestCnt, recvBandwidthRate, recvPkgRate, sendAppendRequestCnt, startTime, state) =>
        //The leaderID can be extracted from the selfStats
        leaderInfo.leader
      case EtcdStandardError(statusCode, xEtcdClusterId, error) =>
        fail("Can not find leader id")
    }
  }
}

