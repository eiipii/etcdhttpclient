/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.utils.AbstractMultipleContainerEtcdClientTest

class MembersEtcdClientTest extends AbstractMultipleContainerEtcdClientTest {

  override def etcdClientPortShift: Int = 0

  override def clusterSize: Int = 3

  override def testScopePrefix: String = "/members"

  //Designed with a cluster with 3 initial members
  it should "add, delete, list and change peer urls of etcd cluster members" in { etcdcli =>

    val getMemberResult = etcdcli.getMembers.futureValue
    getMemberResult.accepted should be(true)
    inside(getMemberResult) {
      case EtcdGetMembersResponse(headers, body) =>
        body.members.head.id shouldBe a[String]
        body.members should have size 3
    }

    val newMemberForm: EtcdMemberForm = EtcdMemberForm(List("http://10.0.0.10:2380", "http://10.0.0.11:2380"))
    val result = etcdcli.addMember(newMemberForm).futureValue
    result.accepted should be(true)
    inside(result) {
      case EtcdAddMemberResponse(headers, body) =>
        body.id shouldBe a[String]
        val member = body.id

        inside(etcdcli.getMembers.futureValue) {
          case EtcdGetMembersResponse(headers, body) =>
            body.members.contains(EtcdMember(member, "", List("http://10.0.0.10:2380", "http://10.0.0.11:2380"), List())) should be(true)
        }

        val changeMemberForm: EtcdMemberForm = EtcdMemberForm(List("http://10.0.0.12:4002"))
        inside(etcdcli.changePeerURLs(member, changeMemberForm).futureValue) {
          case EtcdConfirmationResponse(clusterID) =>
            clusterID shouldBe a[EtcdClusterId]
        }

        inside(etcdcli.getMembers.futureValue) {
          case EtcdGetMembersResponse(headers, body) =>
            body.members.contains(EtcdMember(member, "", List("http://10.0.0.12:4002"), List())) should be(true)
        }

        inside(etcdcli.deleteMember(member).futureValue) {
          case EtcdConfirmationResponse(clusterID) =>
            clusterID shouldBe a[EtcdClusterId]
        }

    }

  }

}