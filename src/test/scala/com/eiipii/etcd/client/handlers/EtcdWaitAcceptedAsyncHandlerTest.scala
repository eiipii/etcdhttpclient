/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client.handlers

import com.eiipii.etcd.client.model.{ EtcdWaitForKeyResult, FutureBasedEtcdWaitCallback }
import io.netty.handler.codec.http.HttpHeaders
import org.asynchttpclient.{ HttpResponseHeaders, HttpResponseStatus }
import org.mockito.Mockito
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ FunSuite, Matchers }

import scala.concurrent.Promise

class EtcdWaitAcceptedAsyncHandlerTest extends FunSuite with ScalaFutures with Matchers {

  test("testOnCompleted") {
    //Given
    val promise: Promise[EtcdWaitForKeyResult] = Promise[EtcdWaitForKeyResult]()
    val callback: FutureBasedEtcdWaitCallback = new FutureBasedEtcdWaitCallback()
    val handler: EtcdWaitAcceptedAsyncHandler = new EtcdWaitAcceptedAsyncHandler(promise, callback)
    //When
    handler.onCompleted()
    //Then
    val thrown = the[Exception] thrownBy callback.future.futureValue
    thrown.getMessage should be("The future returned an exception of type: com.eiipii.etcd.client.model.IncorrectEtcdRequestFormatException.")
    val thrownP = the[Exception] thrownBy promise.future.futureValue
    thrownP.getMessage should be("The future returned an exception of type: com.eiipii.etcd.client.model.IncorrectEtcdRequestFormatException.")

  }

  test("testFailPromise") {

  }

  test("testOnStatusReceived") {
    //Given
    val promise: Promise[EtcdWaitForKeyResult] = Promise[EtcdWaitForKeyResult]()
    val callback: FutureBasedEtcdWaitCallback = new FutureBasedEtcdWaitCallback()
    val handler: EtcdWaitAcceptedAsyncHandler = new EtcdWaitAcceptedAsyncHandler(promise, callback)
    val status: HttpResponseStatus = Mockito.mock(classOf[HttpResponseStatus])
    Mockito.when(status.getStatusCode()).thenReturn(404)
    //When
    handler.onStatusReceived(status)
    //Then
    val thrown = the[Exception] thrownBy callback.future.futureValue
    thrown.getMessage should be("The future returned an exception of type: com.eiipii.etcd.client.model.IncorrectEtcdRequestFormatException.")
    val thrownP = the[Exception] thrownBy promise.future.futureValue
    thrownP.getMessage should be("The future returned an exception of type: com.eiipii.etcd.client.model.IncorrectEtcdRequestFormatException.")
  }

  test("testOnHeadersReceived") {

    //Given
    val promise: Promise[EtcdWaitForKeyResult] = Promise[EtcdWaitForKeyResult]()
    val callback: FutureBasedEtcdWaitCallback = new FutureBasedEtcdWaitCallback()
    val handler: EtcdWaitAcceptedAsyncHandler = new EtcdWaitAcceptedAsyncHandler(promise, callback)
    val headers: HttpResponseHeaders = Mockito.mock(classOf[HttpResponseHeaders])
    val internal: HttpHeaders = Mockito.mock(classOf[HttpHeaders])
    Mockito.when(headers.getHeaders()).thenReturn(internal)
    //When
    handler.onHeadersReceived(headers)
    //Then
    val thrown = the[Exception] thrownBy callback.future.futureValue
    thrown.getMessage should be("The future returned an exception of type: com.eiipii.etcd.client.model.IncorrectEtcdRequestFormatException.")
    val thrownP = the[Exception] thrownBy promise.future.futureValue
    thrownP.getMessage should be("The future returned an exception of type: com.eiipii.etcd.client.model.IncorrectEtcdRequestFormatException.")
  }

}
