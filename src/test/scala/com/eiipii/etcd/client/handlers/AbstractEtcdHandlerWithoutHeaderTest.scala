/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client.handlers

import com.eiipii.etcd.client.model.{ EtcdGetStoreStatsResult, IncorrectEtcdRequestFormatException }
import io.netty.handler.codec.http.HttpHeaders
import org.asynchttpclient.{ HttpResponseBodyPart, HttpResponseHeaders, HttpResponseStatus }
import org.mockito.Mockito
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ FlatSpec, Matchers }

import scala.concurrent.Promise

class AbstractEtcdHandlerWithoutHeaderTest extends FlatSpec with Matchers with ScalaFutures {

  it should "provide complete error message - EtcdStoreStatsResponseAsyncHandler example" in {
    //Given
    val promise = Promise[EtcdGetStoreStatsResult]()
    val handler = new EtcdStoreStatsResponseAsyncHandler(promise)
    val headers = Mockito.mock(classOf[HttpResponseHeaders])
    val header = Mockito.mock(classOf[HttpHeaders])
    Mockito.when(headers.getHeaders).thenReturn(header)
    val bodyMock: HttpResponseBodyPart = Mockito.mock(classOf[HttpResponseBodyPart])
    Mockito.when(bodyMock.getBodyPartBytes()).thenReturn("{\"invalidJsonFormat\":2}".getBytes)

    val statusMock = Mockito.mock(classOf[HttpResponseStatus])
    Mockito.when(statusMock.getStatusCode).thenReturn(200)

    //when
    handler.onStatusReceived(statusMock)
    handler.onHeadersReceived(headers)
    handler.onBodyPartReceived(bodyMock)
    handler.onCompleted()
    //then

    promise.future.failed.futureValue.toString should be(IncorrectEtcdRequestFormatException(
      "EtcdStoreStatsResponseAsyncHandler",
      Some(200),
      Some("{\"invalidJsonFormat\":2}"),
      None
    ).toString)
  }
}
