/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client.handlers

import org.asynchttpclient.AsyncHandler.State
import org.asynchttpclient.{ HttpResponseHeaders, HttpResponseStatus }
import org.mockito.Mockito
import org.scalatest.{ FlatSpec, Matchers }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Promise
import scala.util._

class AbstractEtcdHandlerTest extends FlatSpec with Matchers {

  it should "Notify the promise in case of failure" in {
    //Given
    val promise = Promise[String]()
    val handler = new TestEtcdHandler(promise)
    //when
    val transportException: Exception = new scala.Exception("any exception")
    handler.onThrowable(transportException)
    //then
    promise.isCompleted should be(true)
    promise.future.onComplete {
      case Success(passed) => fail("promise should be failed")
      case Failure(t) => t should equal(transportException)
    }
  }

  it should "Call failPromise when body can not be parsed" in {
    //Given response body is not provided
    val promise = Promise[String]()
    val bodyResponse = None
    val handler = new TestEtcdHandler(promise, bodyResponse)
    val statusMock: HttpResponseStatus = Mockito.mock(classOf[HttpResponseStatus])
    //and a 200 status is provided
    Mockito.when(statusMock.getStatusCode).thenReturn(200)
    handler.onStatusReceived(statusMock)
    //when
    handler.onCompleted()
    //then
    handler.failPromiseCalled should be(true)
  }

  it should "Call failPromise when error can not be created" in {
    //Given
    val promise = Promise[String]()
    val bodyResponse = Some("should not be used")
    val errorResponse = None // Error not created correctly
    val handler = new TestEtcdHandler(promise, bodyResponse, errorResponse)
    val statusMock: HttpResponseStatus = Mockito.mock(classOf[HttpResponseStatus])
    //and a 404 status is provided
    Mockito.when(statusMock.getStatusCode).thenReturn(404)
    handler.onStatusReceived(statusMock)
    //when
    handler.onCompleted()
    //then
    handler.failPromiseCalled should be(true)

  }
  it should "Call failPromise when no status is provided" in {
    //Given
    val promise = Promise[String]()
    val bodyResponse = Some("should not be used")
    val errorResponse = Some("error will be provided")
    val handler = new TestEtcdHandler(promise, bodyResponse, errorResponse)
    //and no status call was done
    //when
    handler.onCompleted()
    //then
    handler.failPromiseCalled should be(true)
  }

}

class TestEtcdHandler(
    val promise: Promise[String],
    val responseToProvide: Option[String] = None,
    val errorToProvide: Option[String] = None
) extends AbstractEtcdHandler[String] {
  var failPromiseCalled = false
  var headerCalled = false

  override def failPromise(): Unit = failPromiseCalled = true

  override def handlerName: String = "testHandler"

  override protected def buildResponse(): Option[String] = responseToProvide

  override def buildError(): Option[String] = errorToProvide

  override def onHeadersReceived(headers: HttpResponseHeaders): State = {
    headerCalled = true
    State.CONTINUE
  }
}