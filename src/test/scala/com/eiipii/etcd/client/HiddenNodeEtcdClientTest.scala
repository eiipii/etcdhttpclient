/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class HiddenNodeEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/hiddenNode"

  it should "be able to PUT a value in a hidden node" in { etcdcli =>
    val valueForNodeName = genUniqueRandomValue.sample.get
    val value = genUniqueRandomValue.sample.get
    val dir = genUniqueRandomDirectoryWithMaxDeep(4).sample.get
    val hiddenNode = EtcdModel.hiddenNode(dir.dir, valueForNodeName.value)

    inside(etcdcli.setKey(hiddenNode, value).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        body.action should be("set")
        body.node.value should be(value)
    }

    inside(etcdcli.listDir(dir).futureValue) {
      case EtcdListDirResponse(headers, body) =>
        //the hidden node is not listed
        body.node.nodes should be(List())
    }

    //but its value can be retrieved
    val getResult = etcdcli.getKey(hiddenNode).futureValue
    getResult.accepted should be(true)
    inside(getResult) {
      case EtcdGetKeyResponse(headers, body) =>
        body.node.value should be(value)
    }
  }
}

