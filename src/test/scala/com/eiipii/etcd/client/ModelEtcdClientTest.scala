/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class ModelEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/model"

  it should "accept only proper keys and directories" in { etcdcli =>
    val newKey = genUniqueRandomKey.sample.get
    val newDirectory = genUniqueRandomDirectory.sample.get
    val str = genUniqueRandomString.sample.get

    val key = EtcdModel.key(newKey.key)
    key should be(newKey)
    //substring methods eliminate "/" in order to cause errors.
    an[IllegalArgumentException] should be thrownBy EtcdModel.key(null)
    an[IllegalArgumentException] should be thrownBy EtcdModel.key("")
    an[IllegalArgumentException] should be thrownBy EtcdModel.key(newDirectory.dir)
    an[IllegalArgumentException] should be thrownBy EtcdModel.key(newKey.key.substring(1, newKey.key.length))

    val dir = EtcdModel.directory(newDirectory.dir)

    dir should be(newDirectory)
    an[IllegalArgumentException] should be thrownBy EtcdModel.directory(null)
    an[IllegalArgumentException] should be thrownBy EtcdModel.directory("")
    an[IllegalArgumentException] should be thrownBy EtcdModel.directory(newKey.key)
    an[IllegalArgumentException] should be thrownBy EtcdModel.directory(newKey.key.substring(1, newKey.key.length))

    val hiddenNode = EtcdModel.hiddenNode(newDirectory.dir, newKey.key.substring(1, newKey.key.length))
    hiddenNode.key should be(newDirectory.dir + "_" + newKey.key.substring(1, newKey.key.length))

    an[IllegalArgumentException] should be thrownBy EtcdModel.hiddenNode(newDirectory.dir, "")
    an[IllegalArgumentException] should be thrownBy EtcdModel.hiddenNode(newDirectory.dir, null)
    an[IllegalArgumentException] should be thrownBy EtcdModel.hiddenNode("", newKey.key)
    an[IllegalArgumentException] should be thrownBy EtcdModel.hiddenNode(null, newKey.key)
    an[IllegalArgumentException] should be thrownBy EtcdModel.hiddenNode(newDirectory.dir, newKey.key)
    an[IllegalArgumentException] should be thrownBy EtcdModel.hiddenNode(newKey.key, newKey.key.substring(1, newKey.key.length))
    an[IllegalArgumentException] should be thrownBy EtcdModel.hiddenNode(newKey.key.substring(1, newKey.key.length), newKey.key.substring(1, newKey.key.length))

    val dir2 = EtcdModel.directoryOfKey(newKey)
    EtcdModel.keyOfDirectory(dir2) should be(newKey)

    val value = EtcdModel.value(str)
    value.value should be(str)

    val dirAndKey = EtcdModel.key(dir, str)
    dirAndKey.key should endWith(str)
  }

  it should "create a path (represented as a list of strings) from a key, and a key from a path" in { etcdcli =>
    val newKey = genUniqueRandomKey.sample.get
    val newKey2 = genUniqueRandomKey.sample.get

    val path: List[String] = EtcdModel.toPath(newKey)
    var nodeIndex: Int = 0
    var nodeCounter: Int = 0
    while (nodeIndex < newKey.key.indexOf("/", nodeIndex + 1)) {
      // The substring containing a path name. The first + 1 is added in order to drop the "/" from the node name.
      newKey.key.substring(newKey.key.indexOf("/", nodeIndex) + 1, newKey.key.indexOf("/", nodeIndex + 1)) should be(path.drop(nodeCounter).head)
      nodeIndex = newKey.key.indexOf("/", nodeIndex + 1)
      nodeCounter = nodeCounter + 1
    }
    newKey.key.substring(newKey.key.indexOf("/", nodeIndex) + 1, newKey.key.length) should be(path.drop(nodeCounter).head)

    val key = EtcdModel.fromPath(path)
    key should be(newKey)

    // path with strings containing "/".
    val path2 = newKey2.key :: path

    an[IllegalArgumentException] should be thrownBy EtcdModel.fromPath(path2)
  }
}