/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class UnsettingTTLEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/unsettingTTL"

  it should "be able to PUT a value in etcd with TTL and then unset it" in { etcdcli =>
    val key = genUniqueRandomKey.sample.get
    val value = genRandomValue.sample.get
    val ttl = EtcdTTL(genRandomInteger.sample.get)

    val setResult = etcdcli.setKey(key, value, Some(ttl)).futureValue
    setResult.accepted should be(true)
    inside(setResult) {
      case EtcdSetKeyResponse(headers, body) =>
        body.action should be("set")
        body.node.ttl should be(Some(ttl))
    }

    inside(etcdcli.setKey(key, value, None, Some(KeyMustExist(true))).futureValue) {
      case EtcdSetKeyResponse(headers, body) =>
        body.node.ttl should be(None)
    }
  }
}

