/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client.testutils

import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest
import org.scalatest.BeforeAndAfterAll

object EtcdSharedInstanceSingleton extends EtcdContainer {

  override def etcdClientPortShift: Int = AbstractCommonContainerEtcdClientTest.commonEtcdClientPortShift

  val lock = new Object

  @volatile
  var running: Boolean = false

  def registerTest(): Unit = {
    lock.synchronized {
      if (!running) {
        running = true
        //first test will block until the container is started
        startAllOrFail()
      }
    }
  }

}

trait EtcdSharedInstanceRegistration {
  self: BeforeAndAfterAll =>

  override protected def beforeAll(): Unit = {
    EtcdSharedInstanceSingleton.registerTest()
  }

  override protected def afterAll(): Unit = {
    //shutdown of container will be on jvm stop
  }
}
