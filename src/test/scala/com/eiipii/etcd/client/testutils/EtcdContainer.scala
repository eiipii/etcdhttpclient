/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.testutils

import com.whisk.docker.{ DockerContainer, DockerReadyChecker }

import scala.concurrent.duration._

trait EtcdContainer extends DockerKitConfigWithSpotifyClient {

  def etcdClientPortShift: Int

  private val etcdClientPortInternal = 4001 + etcdClientPortShift
  private val etcdClientPortExternal = 4001 + etcdClientPortShift
  private val etcdPeerPortInternal = 2380 + etcdClientPortShift
  private val etcdPeerPortExternal = 2380 + etcdClientPortShift

  def getEtcdClientConnection: String = s"http://localhost:$etcdClientPortExternal"

  private lazy val nodeName = s"etcd$etcdClientPortShift"

  val etcddbContainer: DockerContainer = DockerContainer("quay.io/coreos/etcd:v2.3.2")
    .withPorts(
      etcdClientPortInternal -> Some(etcdClientPortExternal),
      etcdPeerPortInternal -> Some(etcdPeerPortExternal)
    )
    .withReadyChecker(
      DockerReadyChecker.LogLineContains("etcdserver: set the initial cluster version")
        and
        DockerReadyChecker.HttpResponseCode(etcdClientPortExternal, "/health", Some("localhost")).looped(50, 500 milliseconds)
    )
    .withCommand(
      "-name", nodeName,
      s"-advertise-client-urls", s"http://localhost:$etcdClientPortExternal",
      s"-listen-client-urls", s"http://0.0.0.0:$etcdClientPortInternal",
      s"-initial-advertise-peer-urls", s"http://localhost:$etcdPeerPortExternal",
      s"-listen-peer-urls", s"http://0.0.0.0:$etcdPeerPortInternal",
      s"-initial-cluster-token", s"etcd-cluster-$etcdClientPortShift",
      s"-initial-cluster", s"$nodeName=http://localhost:$etcdPeerPortExternal",
      "-initial-cluster-state", "new"
    )

  abstract override def dockerContainers: List[DockerContainer] = etcddbContainer :: super.dockerContainers
}