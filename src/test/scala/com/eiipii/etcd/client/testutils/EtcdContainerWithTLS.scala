/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.testutils

import com.whisk.docker.{ DockerContainer, DockerReadyChecker, VolumeMapping }

trait EtcdContainerWithTLS extends DockerKitConfigWithSpotifyClient {

  def etcdClientPortShift: Int

  private val etcdClientPortInternal = 24001 + etcdClientPortShift
  private val etcdClientPortExternal = 24001 + etcdClientPortShift
  private val etcdPeerPortInternal = 22380 + etcdClientPortShift
  private val etcdPeerPortExternal = 22380 + etcdClientPortShift

  def getEtcdClientConnection: String = s"https://localhost:$etcdClientPortExternal"

  private lazy val nodeName = s"etcd$etcdClientPortShift"

  val PATH: String = getClass.getResource("/ssl").getPath

  val etcddbContainer: DockerContainer = DockerContainer("quay.io/coreos/etcd:v2.3.2")
    .withPorts(
      etcdClientPortInternal -> Some(etcdClientPortExternal),
      etcdPeerPortInternal -> Some(etcdPeerPortExternal)
    )
    .withVolumes(Seq(
      VolumeMapping(host = PATH, container = "/testCa/", rw = false)
    ))
    .withCommand(
      "-name", nodeName,
      "-trusted-ca-file", "/testCa/dev-ca.crt",
      "-cert-file", "/testCa/localhost.crt",
      "-key-file", "/testCa/localhost_nopass.key",
      "-advertise-client-urls", s"https://localhost:$etcdClientPortExternal",
      "-listen-client-urls", s"https://0.0.0.0:$etcdClientPortInternal",
      "-initial-advertise-peer-urls", s"http://localhost:$etcdPeerPortExternal",
      "-listen-peer-urls", s"http://0.0.0.0:$etcdPeerPortInternal",
      "-initial-cluster-token", s"etcd-cluster-$etcdClientPortShift",
      "-initial-cluster", s"$nodeName=http://localhost:$etcdPeerPortExternal",
      "-initial-cluster-state", "new",
      "-client-cert-auth"
    ).withReadyChecker(
        DockerReadyChecker.LogLineContains("etcdserver: set the initial cluster version")
      )

  abstract override def dockerContainers: List[DockerContainer] = etcddbContainer :: super.dockerContainers

}
