/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiipii.etcd.client.testutils

import java.util.concurrent.Executors
import com.whisk.docker._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

trait EtcdClusterContainers extends DockerKitConfigWithSpotifyClient {

  def etcdClientPortShift: Int

  private def etcdClientPortInternal(instance: Int): Int = 14001 + 10 * etcdClientPortShift + instance

  private def etcdClientPortExternal(instance: Int): Int = 14001 + 10 * etcdClientPortShift + instance

  private def etcdPeerPortInternal(instance: Int): Int = 12380 + 10 * etcdClientPortShift + instance

  private def etcdPeerPortExternal(instance: Int): Int = 12380 + 10 * etcdClientPortShift + instance

  val ipAddressToUse: String = {
    import java.net._

    import collection.JavaConverters._
    val enumeration = NetworkInterface.getNetworkInterfaces.asScala.toSeq
    val ipAddresses = enumeration.flatMap(p =>
      p.getInetAddresses.asScala.toSeq)
    val address = ipAddresses.find { address =>
      val host = address.getHostAddress
      host.contains(".") && !address.isLoopbackAddress
    }.getOrElse(throw new IllegalStateException("Only loopback interface found"))
    address.getHostAddress
  }

  def getEtcdClientConnection: String = s"http://$ipAddressToUse:${etcdClientPortExternal(1)}"

  def clusterSize: Int
  //put explicit executionContext to avoid initialization loop.
  override implicit lazy val dockerExecutionContext: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(clusterSize * 2))

  val buildClusterConfiguration: List[DockerContainer] =
    (1 to clusterSize).map { instance =>
      definitionAndPorts(instance)
    }.toList

  private def instanceName(instance: Int): String = s"cluster${etcdClientPortShift}Instance${instance}Size$clusterSize"

  private def definitionAndPorts(instance: Int): DockerContainer = {
    //2380 etcdPeerPortInternal
    //2379 etcdClientPortExternal
    val initialAdvertisePeerUrls: String = s"http://$ipAddressToUse:${etcdPeerPortInternal(instance)}"
    val listenPeerUrls: String = s"http://0.0.0.0:${etcdPeerPortInternal(instance)}"
    val listenClientUrls: String = s"http://0.0.0.0:${etcdClientPortExternal(instance)}"
    val advertiseClientUrls: String = s"http://$ipAddressToUse:${etcdClientPortExternal(instance)}"
    val initialCluster: String = (1 to clusterSize).map { i => s"${instanceName(i)}=http://$ipAddressToUse:${etcdPeerPortInternal(i)}" }.mkString(",")
    DockerContainer("quay.io/coreos/etcd:v2.3.2")
      .withPorts(
        etcdClientPortInternal(instance) -> Some(etcdClientPortExternal(instance)),
        etcdPeerPortInternal(instance) -> Some(etcdPeerPortExternal(instance))
      )
      .withReadyChecker(
        DockerReadyChecker.LogLineContains("etcdserver: set the initial cluster version")
          and
          DockerReadyChecker.HttpResponseCode(etcdClientPortExternal(instance), "/health", Some("localhost")).looped(50, 500 milliseconds)
      ).withCommand(
          "-name", instanceName(instance),
          s"-initial-advertise-peer-urls", initialAdvertisePeerUrls,
          s"-listen-peer-urls", listenPeerUrls,
          s"-listen-client-urls", listenClientUrls,
          s"-advertise-client-urls", advertiseClientUrls,
          s"-initial-cluster-token", instanceName(0),
          s"-initial-cluster", initialCluster,
          "-initial-cluster-state", "new"
        )
  }

  abstract override def dockerContainers: List[DockerContainer] = buildClusterConfiguration ++ super.dockerContainers
}

