/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class GetEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/get"

  it should "be able to GET values from etcd" in { etcdcli =>

    forAll((genUniqueRandomKey, "key"), (genRandomValue, "value")) { (key: EtcdKey, value: EtcdValue) =>
      // Set
      val result: EtcdSetKeyResult = etcdcli.setKey(key, value).futureValue
      result shouldBe a[EtcdSetKeyResponse]

      // Get ok
      inside(etcdcli.getKey(key).futureValue) {
        case EtcdGetKeyResponse(headers, body) =>
          body.node.value should be(value)
      }

      // Delete
      val deleteResult: EtcdDeleteKeyResult = etcdcli.deleteKey(key).futureValue
      deleteResult shouldBe a[EtcdDeleteKeyResponse]

      // Get 404 not found
      inside(etcdcli.getKey(key).futureValue) {
        case EtcdRequestError(statusCode, headers, error) =>
          statusCode.code should be(404)
      }
    }
  }

}
