/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiipii.etcd.client

import com.eiipii.etcd.client.model._
import com.eiipii.etcd.client.testutils.EtcdSharedInstanceRegistration
import com.eiipii.etcd.client.utils.AbstractCommonContainerEtcdClientTest

class CreateAndDeleteDirEtcdClientTest extends AbstractCommonContainerEtcdClientTest with EtcdSharedInstanceRegistration {

  override def testScopePrefix: String = "/createDir"

  it should "create a directory in etcd and delete it" in { etcdcli =>

    forAll((genUniqueRandomDirectory, "dir")) { (dir: EtcdDirectory) =>
      inside(etcdcli.createDir(dir).futureValue) {
        case EtcdCreateDirResponse(headers, body) =>
          body.action should be("set")
          body.node.dir should be(true)
      }

      inside(etcdcli.deleteDir(dir).futureValue) {
        case EtcdCreateDirResponse(headers, body) =>
          body.action should be("delete")
      }
    }
  }
}
