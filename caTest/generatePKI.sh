#!/bin/bash

source caEnvironment.sh

export KEYS_PASSWORD="testPassword"

echo "Prepare CA directories"
mkdir $CA_HOME
cd $CA_HOME
mkdir certs db private generated
chmod 700 private
touch db/index
openssl rand -hex 16 > db/serial
echo 1001 > db/crlnumber
cd ..

echo "Prepare Sub CA directories"
mkdir $SUB_CA_HOME
cd $SUB_CA_HOME
mkdir certs db private generated
chmod 700 private
touch db/index
openssl rand -hex 16 > db/serial
echo 1001 > db/crlnumber
cd ..

echo "Generate CA root certificate"
openssl req -new -config $CA_CONF -out $CA_REQ -keyout $CA_KEY -passout env:KEYS_PASSWORD
openssl ca -selfsign -config $CA_CONF -in $CA_REQ -out $CA_CERT -extensions ca_ext -passin env:KEYS_PASSWORD
openssl ca -gencrl -config $CA_CONF -cert $CA_CERT -out $CA_CRL -passin env:KEYS_PASSWORD

echo "Generate OCSP key"
openssl req -new -newkey rsa:4056 -subj "/C=NT/O=PMSOFT/OU=Dev/CN=OCSP Root Responder" -keyout $CA_OCSP_KEY -out $CA_OCSP_REQ -passout env:KEYS_PASSWORD
openssl ca -config $CA_CONF -cert $CA_CERT -in $CA_OCSP_REQ -out $CA_OCSP_CERT -extensions ocsp_ext -days 30 -passin env:KEYS_PASSWORD

echo "Generate Sub CA certificate CSR"
openssl req -new -config $SUB_CA_CONF -out $SUB_CA_REQ -keyout $SUB_CA_KEY -passout env:KEYS_PASSWORD

echo "Sign Sub CA CSR with root CA Cert"
openssl ca -cert $CA_CERT -config $CA_CONF -in $SUB_CA_REQ -out $SUB_CA_CERT -extensions sub_ca_ext -passin env:KEYS_PASSWORD

echo "Generate CA+SubCA pem"
cat $CA_CERT > $PKI_BUNDLE
cat $SUB_CA_CERT >> $PKI_BUNDLE
