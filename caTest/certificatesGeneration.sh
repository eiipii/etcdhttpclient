#!/bin/bash

source caEnvironment.sh

export KEYS_PASSWORD="testPassword"

echo "Prepare CA directories"
mkdir devServers

STAR_DEV_KEY=devServers/start_dev_pmsoft_eu.key
STAR_DEV_KEY_NOPASS=devServers/start_dev_pmsoft_eu_nopass.key
STAR_DEV_REQ=devServers/start_dev_pmsoft_eu.csr
STAR_DEV_CERT=devServers/start_dev_pmsoft_eu.crt
STAR_DEV_CONF=start_dev_pmsoft_eu.conf

echo "Generate server certificate for *.dev.pmsoft.eu"
openssl genrsa -aes256 -out $STAR_DEV_KEY -passout env:KEYS_PASSWORD 2048
openssl req -new -config $STAR_DEV_CONF -key $STAR_DEV_KEY -out $STAR_DEV_REQ -passin env:KEYS_PASSWORD

echo "Sign certificate with Sub CA"
openssl ca -cert $SUB_CA_CERT -config $SUB_CA_CONF -in $STAR_DEV_REQ -out $STAR_DEV_CERT -extensions server_ext -passin env:KEYS_PASSWORD

openssl rsa -in $STAR_DEV_KEY -out $STAR_DEV_KEY_NOPASS -passin env:KEYS_PASSWORD


LOCALHOST_DEV_KEY=devServers/localhost_dev_pmsoft_eu.key
LOCALHOST_DEV_KEY_NOPASS=devServers/localhost_dev_pmsoft_eu_nopass.key
LOCALHOST_DEV_REQ=devServers/localhost_dev_pmsoft_eu.csr
LOCALHOST_DEV_CERT=devServers/localhost_dev_pmsoft_eu.crt
LOCALHOST_DEV_CONF=localhost_dev_pmsoft_eu.conf


echo "Generate server certificate for localhost.dev.pmsoft.eu"
openssl genrsa -aes256 -out $LOCALHOST_DEV_KEY -passout env:KEYS_PASSWORD 2048
openssl req -new -config $LOCALHOST_DEV_CONF -key $LOCALHOST_DEV_KEY -out $LOCALHOST_DEV_REQ -passin env:KEYS_PASSWORD

echo "Sign certificate with Sub CA"
openssl ca -cert $SUB_CA_CERT -config $SUB_CA_CONF -in $LOCALHOST_DEV_REQ -out $LOCALHOST_DEV_CERT -extensions server_ext -passin env:KEYS_PASSWORD

openssl rsa -in $LOCALHOST_DEV_KEY -out $LOCALHOST_DEV_KEY_NOPASS -passin env:KEYS_PASSWORD


CLIENT_DEV_KEY=devServers/client_dev_pmsoft_eu.key
CLIENT_DEV_KEY_NOPASS=devServers/client_dev_pmsoft_eu_nopass.key
CLIENT_DEV_REQ=devServers/client_dev_pmsoft_eu.csr
CLIENT_DEV_CERT=devServers/client_dev_pmsoft_eu.crt
CLIENT_DEV_CONF=client_dev_pmsoft_eu.conf


echo "Generate test client certificate"
openssl genrsa -aes256 -out $CLIENT_DEV_KEY -passout env:KEYS_PASSWORD 2048
openssl req -new -config $CLIENT_DEV_CONF -key $CLIENT_DEV_KEY -out $CLIENT_DEV_REQ -passin env:KEYS_PASSWORD

echo "Sign certificate with Sub CA"
openssl ca -cert $SUB_CA_CERT -config $SUB_CA_CONF -in $CLIENT_DEV_REQ -out $CLIENT_DEV_CERT -extensions client_ext -passin env:KEYS_PASSWORD

openssl rsa -in $CLIENT_DEV_KEY -out $CLIENT_DEV_KEY_NOPASS -passin env:KEYS_PASSWORD
