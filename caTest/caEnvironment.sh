#!/bin/bash


CA_HOME=devCa
SUB_CA_HOME=devSubCa

CA_CONF=dev_ca.conf
CA_REQ=$CA_HOME/generated/dev-ca.csr
CA_KEY=$CA_HOME/private/dev-root-ca.key
CA_CERT=$CA_HOME/generated/dev-ca.crt
CA_CRL=$CA_HOME/generated/dev-ca.crl
CA_OCSP_REQ=$CA_HOME/generated/dev-ocsp.csr
CA_OCSP_KEY=$CA_HOME/private/dev-ocsp.key
CA_OCSP_CERT=$CA_HOME/generated/dev-ca-ocsp.crt

SUB_CA_CONF=dev_sub_ca.conf
SUB_CA_REQ=$SUB_CA_HOME/generated/dev-sub-ca.csr
SUB_CA_KEY=$SUB_CA_HOME/private/dev-sub-ca.key
SUB_CA_CERT=$SUB_CA_HOME/generated/dev-sub-ca.crt

PKI_BUNDLE=$SUB_CA_HOME/generated/bundle.pem