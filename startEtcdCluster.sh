#!/bin/bash

export HostIP="$(ip route get 1 | awk '{print $NF;exit}')"

#docker ps -aq | xargs docker stop
#docker ps -aq | xargs docker rm



docker run -d -v /usr/share/ca-certificates/:/etc/ssl/certs -p 4001:4001 -p 2380:2380 -p 12379:12379 \
  --name infra0 quay.io/coreos/etcd:v2.3.2 \
  --name infra0 \
  --initial-advertise-peer-urls http://${HostIP}:2380 \
  --listen-peer-urls http://0.0.0.0:2380 \
  --listen-client-urls http://0.0.0.0:12379 \
  --advertise-client-urls http://${HostIP}:12379 \
  --initial-cluster-token etcd-cluster-1 \
  --initial-cluster infra0=http://${HostIP}:2380,infra1=http://${HostIP}:2381,infra2=http://${HostIP}:2382 \
  --initial-cluster-state new

docker run -d -v /usr/share/ca-certificates/:/etc/ssl/certs -p 4011:4011 -p 2381:2381 -p 12378:12378 \
  --name infra1 quay.io/coreos/etcd:v2.3.2 \
  --name infra1 \
  --initial-advertise-peer-urls http://${HostIP}:2381 \
  --listen-peer-urls http://0.0.0.0:2381 \
  --listen-client-urls http://0.0.0.0:12378 \
  --advertise-client-urls http://${HostIP}:12378 \
  --initial-cluster-token etcd-cluster-1 \
  --initial-cluster infra0=http://${HostIP}:2380,infra1=http://${HostIP}:2381,infra2=http://${HostIP}:2382 \
  --initial-cluster-state new

docker run -d -v /usr/share/ca-certificates/:/etc/ssl/certs -p 4021:4021 -p 2382:2382 -p 12377:12377 \
  --name infra2 quay.io/coreos/etcd:v2.3.2 \
  --name infra2 \
  --initial-advertise-peer-urls http://${HostIP}:2382 \
  --listen-peer-urls http://0.0.0.0:2382 \
  --listen-client-urls http://0.0.0.0:12377 \
  --advertise-client-urls http://${HostIP}:12377 \
  --initial-cluster-token etcd-cluster-1 \
  --initial-cluster infra0=http://${HostIP}:2380,infra1=http://${HostIP}:2381,infra2=http://${HostIP}:2382 \
  --initial-cluster-state new

