#!/bin/bash
sbt -Droot-level=INFO clean coverage test
sbt coverageReport
sbt coverageAggregate

docker ps -qa | xargs docker stop
docker ps -qa | xargs docker rm

