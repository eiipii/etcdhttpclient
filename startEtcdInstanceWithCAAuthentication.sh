#!/bin/bash

export HostIP="localhost"

docker stop etcd
docker rm etcd

docker run -d -v /usr/share/ca-certificates/:/etc/ssl/certs -v ${PWD}/testCa:/testCa -p 4001:4001 -p 2380:2380 -p 2379:2379 \
 --name etcd quay.io/coreos/etcd:v2.3.2 \
 -name etcd0 \
 -trusted-ca-file /testCa/devCa/generated/dev-ca.crt \
 -cert-file /testCa/certificates/localhost.crt \
 -key-file /testCa/certificates/localhost_nopass.key \
 --client-cert-auth \
 -advertise-client-urls https://${HostIP}:2379,https://${HostIP}:4001 \
 -listen-client-urls https://0.0.0.0:2379,https://0.0.0.0:4001 \
 -initial-advertise-peer-urls https://${HostIP}:2380 \
 -initial-cluster-token etcd-cluster-1 \
 -initial-cluster etcd0=https://${HostIP}:2380 \
 -initial-cluster-state new

sleep 2

docker logs etcd

curl -L -S --cacert testCa/devCa/generated/dev-ca.crt --cert testCa/certificates/client.crt --key testCa/certificates/client_nopass.key https://localhost:4001/v2/keys/foo -XPUT -d value=bar -v

curl -L -S --cacert testCa/devCa/generated/dev-ca.crt --cert testCa/certificates/client.crt --key testCa/certificates/client_nopass.key https://localhost:2379/v2/keys/foo -XPUT -d value=bar -v

curl -L -S --cacert testCa/devCa/generated/dev-ca.crt --cert testCa/certificates/client.crt --key testCa/certificates/client_nopass.key https://localhost:2379/v2/keys/foo -XPUT -d value=bar -v


