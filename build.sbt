/*
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Dependencies._

lazy val commonSettings = Seq(
  organization := "com.eiipii",
  version := "0.1.1",
  scalaVersion := "2.11.11",
  crossScalaVersions := Seq("2.11.11", "2.12.2"),
  parallelExecution in Test := true
)

lazy val etcdhttpclient = (project in file(".")).
  settings(commonSettings: _*).
  settings(PublishSettings.sonatypeSettings: _*).
  settings(
    name := "etcdHttpClient",
    libraryDependencies ++= async ++ Seq(json4s) ++ loggingDependencies ++ testDependencies ++ dockerTest,
    resolvers += Classpaths.typesafeReleases,
    resolvers += Resolver.defaultUserFileRepository("cache"),
    resolvers += Resolver.file("Local repo", file(System.getProperty("user.home") + "/.ivy2/local"))(Resolver.ivyStylePatterns),
    resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
  )