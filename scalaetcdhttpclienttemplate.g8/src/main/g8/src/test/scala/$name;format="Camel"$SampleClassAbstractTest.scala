package $package$.sampleClass

import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import com.whisk.docker.config.DockerKitConfig
import com.whisk.docker.{DockerContainer, DockerReadyChecker}
import org.scalatest.time.{Milliseconds, Seconds, Span}

import scala.concurrent.ExecutionContext

abstract class $name;format="Camel"$SampleClassAbstractTest extends fixture.FlatSpec with Matchers with ScalaFutures with BeforeAndAfterAll with EtcdContainer {

override protected def beforeAll(): Unit = {
startAllOrFail()
}

override protected def afterAll(): Unit = {
stopAllQuietly()
}

implicit val executionContext = ExecutionContext.Implicits.global

implicit val pc = PatienceConfig(Span(3, Seconds), Span(1500, Milliseconds))

override type FixtureParam = $name;format="Camel"$SampleClass

def withFixture(test: OneArgTest): Outcome = {
val test$name;format="Camel"$ = new $name;format="Camel"$SampleClass("http://127.0.0.1:4001")
try {
withFixture(test.toNoArgTest(test$name;format="Camel"$))
} finally test$name;format="Camel"$.closeEtcdClient()

}

}

import scala.concurrent.duration._

trait EtcdContainer extends DockerKitConfig {

  private val etcdClientPortInternal = "4001"
  private val etcdClientPortExternal = "4001"
  private val etcdPeerPortInternal = "2380"
  private val etcdPeerPortExternal = "2380"

  def getEtcdClientConnection: String = "http://localhost:" + etcdClientPortExternal

  private lazy val nodeName = "etcd"

  val etcddbContainer: DockerContainer = DockerContainer("quay.io/coreos/etcd:v2.3.2")
    .withPorts(
      etcdClientPortInternal.toInt -> Some(etcdClientPortExternal.toInt),
      etcdPeerPortInternal.toInt -> Some(etcdPeerPortExternal.toInt)
    )
    .withReadyChecker(
      DockerReadyChecker.LogLineContains("etcdserver: set the initial cluster version")
        and
        DockerReadyChecker.HttpResponseCode(etcdClientPortExternal.toInt, "/health", Some("localhost")).looped(50, 500 milliseconds)
    )
    .withCommand(
      "-name", nodeName,
      s"-advertise-client-urls", "http://localhost:" + etcdClientPortExternal,
      s"-listen-client-urls", "http://0.0.0.0:" + etcdClientPortInternal,
      s"-initial-advertise-peer-urls", "http://localhost:" + etcdPeerPortExternal,
      s"-listen-peer-urls", "http://0.0.0.0:" + etcdPeerPortInternal,
      s"-initial-cluster-token", "etcd-cluster",
      s"-initial-cluster", "etcd=http://localhost:" + etcdPeerPortExternal,
      "-initial-cluster-state", "new"
    )

  abstract override def dockerContainers: List[DockerContainer] = etcddbContainer :: super.dockerContainers
}
