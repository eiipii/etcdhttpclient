package $package$.sampleClass

  import com.eiipii.etcd.client.model.{EtcdGetVersionResponse, EtcdGetVersionResult, EtcdStandardError}

  import scala.concurrent.Future

class $name;format="Camel"$SampleClassTest extends $name;format="Camel"$SampleClassAbstractTest {

  it should "be able to get the version of the etcd API" in { test$name;format="Camel"$ =>

  val versionFut: Future[EtcdGetVersionResult] = test$name;format="Camel"$.getEtcdVersion

  versionFut.futureValue match {
  case EtcdGetVersionResponse(etcdserver, etcdcluster) => {
  etcdserver shouldBe "2.3.2"
  etcdcluster shouldBe "2.3.0"
}
  case EtcdStandardError(statusCode, xEtcdClusterId, error) => fail(error.message)
}
}
}
